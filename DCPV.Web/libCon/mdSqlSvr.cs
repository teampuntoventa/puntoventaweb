﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace libCon
{
    public class mdSqlSvr : GDatos
    {
        static readonly System.Collections.Hashtable ColComandos = new System.Collections.Hashtable();

        public override sealed string CadenaConexion
        {
            get
            {
                if (MCadenaConexion.Length == 0)
                {
                    if (MBase.Length != 0 && MServidor.Length != 0)
                    {
                        System.Data.OleDb.OleDbConnectionStringBuilder b = new System.Data.OleDb.OleDbConnectionStringBuilder();
                        b.Provider = "Microsoft.ACE.OLEDB.12.0";
                        b.DataSource = System.IO.Path.Combine(Servidor, Base); // "contacts.accdb";
                        return b.ToString();
                    }
                    throw new Exception("No se puede establecer la cadena de conexión en la clase MySQL Server");
                }
                return MCadenaConexion = CadenaConexion;
            }// end get
            set
            { MCadenaConexion = value; } // end set
        }

        protected override void CargarParametros(System.Data.IDbCommand com, Object[] args)
        {
            for (int i = 1; i < com.Parameters.Count; i++)
            {
                var p = (System.Data.OleDb.OleDbParameter)com.Parameters[i];
                p.Value = i <= args.Length ? args[i - 1] ?? DBNull.Value : null;
            } // end for
        } // end CargarParametros

        protected override System.Data.IDbCommand Comando(string procedimientoAlmacenado)
        {
            System.Data.OleDb.OleDbCommand com;
            if (ColComandos.Contains(procedimientoAlmacenado))
                com = (System.Data.OleDb.OleDbCommand)ColComandos[procedimientoAlmacenado];
            else
            {
                var con2 = new System.Data.OleDb.OleDbConnection(CadenaConexion);
                con2.Open();
                com = new System.Data.OleDb.OleDbCommand(procedimientoAlmacenado, con2) { CommandType = System.Data.CommandType.StoredProcedure };
                System.Data.OleDb.OleDbCommandBuilder.DeriveParameters(com);
                con2.Close();
                con2.Dispose();
                ColComandos.Add(procedimientoAlmacenado, com);
            }//end else
            com.Connection = (System.Data.OleDb.OleDbConnection)Conexion;
            com.Transaction = (System.Data.OleDb.OleDbTransaction)MTransaccion;
            return com;
        }// end Comando

        protected override System.Data.IDbCommand ComandoSql(string comandoSql)
        {
            var com = new System.Data.OleDb.OleDbCommand(comandoSql, (System.Data.OleDb.OleDbConnection)Conexion, (System.Data.OleDb.OleDbTransaction)MTransaccion);
            return com;
        }// end Comando

        protected override System.Data.IDbConnection CrearConexion(string cadenaConexion)
        { return new System.Data.OleDb.OleDbConnection(cadenaConexion); }

        protected override System.Data.IDataAdapter CrearDataAdapter(string procedimientoAlmacenado, params Object[] args)
        {
            var da = new System.Data.OleDb.OleDbDataAdapter((System.Data.OleDb.OleDbCommand)Comando(procedimientoAlmacenado));
            if (args.Length != 0)
                CargarParametros(da.SelectCommand, args);
            return da;
        } // end CrearDataAdapter

        protected override System.Data.IDataAdapter CrearDataAdapterSql(string comandoSql)
        {
            var da = new System.Data.OleDb.OleDbDataAdapter((System.Data.OleDb.OleDbCommand)ComandoSql(comandoSql));
            return da;
        } // end CrearDataAdapterSql


        protected override void ImportarData(System.Data.OleDb.OleDbDataReader drData, string pTabla = "tbl")
        {
            throw new NotImplementedException();
        }

        public mdSqlSvr()
        {
            Base = "";
            Servidor = "";
            Usuario = "";
            Password = "";
        }// end DatosSQLServer

        public mdSqlSvr(string cadenaConexion)
        { CadenaConexion = cadenaConexion; }// end DataAccess

        public mdSqlSvr(string servidor, string @base)
        {
            Base = @base;
            Servidor = servidor;
        }// end DataAccess

        public mdSqlSvr(string servidor, string @base, string usuario, string password)
        {
            Base = @base;
            Servidor = servidor;
            Usuario = usuario;
            Password = password;
        }// end DataAccess
    }
}
