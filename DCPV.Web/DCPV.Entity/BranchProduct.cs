﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{

    public class BranchProduct
    {
        #region Variables
        private Int32 _BrnPrdId;
        private Int32 _BsnId;
        private Int32 _PrdId;
        private Int32 _BrnId;
        private decimal _BrnPrdQty;
        private decimal _BrnPrdQtyMin;
        private Int32 _BrnPrdClosed;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 BrnPrdId
        {
            get
            {
                return this._BrnPrdId;
            }
            set
            {
                if ((this._BrnPrdId != value))
                {
                    this._BrnPrdId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 PrdId
        {
            get
            {
                return this._PrdId;
            }
            set
            {
                if ((this._PrdId != value))
                {
                    this._PrdId = value;
                }
            }
        }

        public Int32 BrnId
        {
            get
            {
                return this._BrnId;
            }
            set
            {
                if ((this._BrnId != value))
                {
                    this._BrnId = value;
                }
            }
        }

        public decimal BrnPrdQty
        {
            get
            {
                return this._BrnPrdQty;
            }
            set
            {
                if ((this._BrnPrdQty != value))
                {
                    this._BrnPrdQty = value;
                }
            }
        }

        public decimal BrnPrdQtyMin
        {
            get
            {
                return this._BrnPrdQtyMin;
            }
            set
            {
                if ((this._BrnPrdQtyMin != value))
                {
                    this._BrnPrdQtyMin = value;
                }
            }
        }

        public Int32 BrnPrdClosed
        {
            get
            {
                return this._BrnPrdClosed;
            }
            set
            {
                if ((this._BrnPrdClosed != value))
                {
                    this._BrnPrdClosed = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public BranchProduct()
        {
            this.BrnPrdId = 0;
            this.BsnId = 0;
            this.PrdId = 0;
            this.BrnId = 0;
            this.BrnPrdQty = 0;
            this.BrnPrdQtyMin = 0;
            this.BrnPrdClosed = 0;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
