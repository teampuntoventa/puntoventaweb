﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{

    public class Movement
    {
        #region Variables
        private Int32 _MvnId;
        private Int32 _BsnId;
        private Int32 _ExrId;
        private Int32 _PerdId;
        private Int32 _BsnPrtId;
        private string _MvmDocnum;
        private string _MvnDocStatus;
        private Int32 _DcmObjId;
        private string _MvnDocType;
        private string _MvnDocCur;
        private decimal _MvnDocRate;
        private string _MvnComments;
        private string _MvnJrnlDiario;
        private Int32 _EplId;
        private DateTime _MvnDocDate;
        private string _MvnTipoDoc;
        private string _MvnSerieDoc;
        private string _MvnCorrelativo;
        private string _MvnBaseTipoDoc;
        private string _MvnBaseSerieDoc;
        private string _MvnBaseCorrelativo;
        private decimal _MvnSubTotal;
        private decimal _MvnIGV;
        private decimal _MvnOtros;
        private decimal _MvnTotal;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 MvnId
        {
            get
            {
                return this._MvnId;
            }
            set
            {
                if ((this._MvnId != value))
                {
                    this._MvnId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 ExrId
        {
            get
            {
                return this._ExrId;
            }
            set
            {
                if ((this._ExrId != value))
                {
                    this._ExrId = value;
                }
            }
        }

        public Int32 PerdId
        {
            get
            {
                return this._PerdId;
            }
            set
            {
                if ((this._PerdId != value))
                {
                    this._PerdId = value;
                }
            }
        }

        public Int32 BsnPrtId
        {
            get
            {
                return this._BsnPrtId;
            }
            set
            {
                if ((this._BsnPrtId != value))
                {
                    this._BsnPrtId = value;
                }
            }
        }

        public string MvmDocnum
        {
            get
            {
                return this._MvmDocnum;
            }
            set
            {
                if ((this._MvmDocnum != value))
                {
                    this._MvmDocnum = value;
                }
            }
        }

        public string MvnDocStatus
        {
            get
            {
                return this._MvnDocStatus;
            }
            set
            {
                if ((this._MvnDocStatus != value))
                {
                    this._MvnDocStatus = value;
                }
            }
        }

        public Int32 DcmObjId
        {
            get
            {
                return this._DcmObjId;
            }
            set
            {
                if ((this._DcmObjId != value))
                {
                    this._DcmObjId = value;
                }
            }
        }

        public string MvnDocType
        {
            get
            {
                return this._MvnDocType;
            }
            set
            {
                if ((this._MvnDocType != value))
                {
                    this._MvnDocType = value;
                }
            }
        }

        public string MvnDocCur
        {
            get
            {
                return this._MvnDocCur;
            }
            set
            {
                if ((this._MvnDocCur != value))
                {
                    this._MvnDocCur = value;
                }
            }
        }

        public decimal MvnDocRate
        {
            get
            {
                return this._MvnDocRate;
            }
            set
            {
                if ((this._MvnDocRate != value))
                {
                    this._MvnDocRate = value;
                }
            }
        }

        public string MvnComments
        {
            get
            {
                return this._MvnComments;
            }
            set
            {
                if ((this._MvnComments != value))
                {
                    this._MvnComments = value;
                }
            }
        }

        public string MvnJrnlDiario
        {
            get
            {
                return this._MvnJrnlDiario;
            }
            set
            {
                if ((this._MvnJrnlDiario != value))
                {
                    this._MvnJrnlDiario = value;
                }
            }
        }

        public Int32 EplId
        {
            get
            {
                return this._EplId;
            }
            set
            {
                if ((this._EplId != value))
                {
                    this._EplId = value;
                }
            }
        }

        public DateTime MvnDocDate
        {
            get
            {
                return this._MvnDocDate;
            }
            set
            {
                if ((this._MvnDocDate != value))
                {
                    this._MvnDocDate = value;
                }
            }
        }

        public string MvnTipoDoc
        {
            get
            {
                return this._MvnTipoDoc;
            }
            set
            {
                if ((this._MvnTipoDoc != value))
                {
                    this._MvnTipoDoc = value;
                }
            }
        }

        public string MvnSerieDoc
        {
            get
            {
                return this._MvnSerieDoc;
            }
            set
            {
                if ((this._MvnSerieDoc != value))
                {
                    this._MvnSerieDoc = value;
                }
            }
        }

        public string MvnCorrelativo
        {
            get
            {
                return this._MvnCorrelativo;
            }
            set
            {
                if ((this._MvnCorrelativo != value))
                {
                    this._MvnCorrelativo = value;
                }
            }
        }

        public string MvnBaseTipoDoc
        {
            get
            {
                return this._MvnBaseTipoDoc;
            }
            set
            {
                if ((this._MvnBaseTipoDoc != value))
                {
                    this._MvnBaseTipoDoc = value;
                }
            }
        }

        public string MvnBaseSerieDoc
        {
            get
            {
                return this._MvnBaseSerieDoc;
            }
            set
            {
                if ((this._MvnBaseSerieDoc != value))
                {
                    this._MvnBaseSerieDoc = value;
                }
            }
        }

        public string MvnBaseCorrelativo
        {
            get
            {
                return this._MvnBaseCorrelativo;
            }
            set
            {
                if ((this._MvnBaseCorrelativo != value))
                {
                    this._MvnBaseCorrelativo = value;
                }
            }
        }

        public decimal MvnSubTotal
        {
            get
            {
                return this._MvnSubTotal;
            }
            set
            {
                if ((this._MvnSubTotal != value))
                {
                    this._MvnSubTotal = value;
                }
            }
        }

        public decimal MvnIGV
        {
            get
            {
                return this._MvnIGV;
            }
            set
            {
                if ((this._MvnIGV != value))
                {
                    this._MvnIGV = value;
                }
            }
        }

        public decimal MvnOtros
        {
            get
            {
                return this._MvnOtros;
            }
            set
            {
                if ((this._MvnOtros != value))
                {
                    this._MvnOtros = value;
                }
            }
        }

        public decimal MvnTotal
        {
            get
            {
                return this._MvnTotal;
            }
            set
            {
                if ((this._MvnTotal != value))
                {
                    this._MvnTotal = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public Movement()
        {
            this.MvnId = 0;
            this.BsnId = 0;
            this.ExrId = 0;
            this.PerdId = 0;
            this.BsnPrtId = 0;
            this.MvmDocnum = string.Empty;
            this.MvnDocStatus = string.Empty;
            this.DcmObjId = 0;
            this.MvnDocType = string.Empty;
            this.MvnDocCur = string.Empty;
            this.MvnDocRate = 0;
            this.MvnComments = string.Empty;
            this.MvnJrnlDiario = string.Empty;
            this.EplId = 0;
            this.MvnDocDate = DateTime.Parse("01/01/1900");
            this.MvnTipoDoc = string.Empty;
            this.MvnSerieDoc = string.Empty;
            this.MvnCorrelativo = string.Empty;
            this.MvnBaseTipoDoc = string.Empty;
            this.MvnBaseSerieDoc = string.Empty;
            this.MvnBaseCorrelativo = string.Empty;
            this.MvnSubTotal = 0;
            this.MvnIGV = 0;
            this.MvnOtros = 0;
            this.MvnTotal = 0;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
