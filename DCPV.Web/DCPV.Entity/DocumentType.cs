﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{

    public class DocumentType
    {
        #region Variables
        private Int32 _DcmTpId;
        private Int32 _BsnId;
        private string _DcmTpDescription;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 DcmTpId
        {
            get
            {
                return this._DcmTpId;
            }
            set
            {
                if ((this._DcmTpId != value))
                {
                    this._DcmTpId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public string DcmTpDescription
        {
            get
            {
                return this._DcmTpDescription;
            }
            set
            {
                if ((this._DcmTpDescription != value))
                {
                    this._DcmTpDescription = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public DocumentType()
        {
            this.DcmTpId = 0;
            this.BsnId = 0;
            this.DcmTpDescription = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
