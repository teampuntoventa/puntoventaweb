﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class Product
    {
        #region Variables
        private Int32 _PrdId;
        private Int32 _BsnId;
        private Int32 _PrdTpid;
        private Int32 _PrdLnId;
        private Int32 _SbPrdLnId;
        private Int32 _UmId;
        private string _CodeBar;
        private string _PrdDescription;
        private string _PrdImage;
        private decimal _PrdCst;
        private decimal _PrdVnt;
        private string _PrdObser;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 PrdId
        {
            get
            {
                return this._PrdId;
            }
            set
            {
                if ((this._PrdId != value))
                {
                    this._PrdId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 PrdTpid
        {
            get
            {
                return this._PrdTpid;
            }
            set
            {
                if ((this._PrdTpid != value))
                {
                    this._PrdTpid = value;
                }
            }
        }

        public Int32 PrdLnId
        {
            get
            {
                return this._PrdLnId;
            }
            set
            {
                if ((this._PrdLnId != value))
                {
                    this._PrdLnId = value;
                }
            }
        }

        public Int32 SbPrdLnId
        {
            get
            {
                return this._SbPrdLnId;
            }
            set
            {
                if ((this._SbPrdLnId != value))
                {
                    this._SbPrdLnId = value;
                }
            }
        }

        public Int32 UmId
        {
            get
            {
                return this._UmId;
            }
            set
            {
                if ((this._UmId != value))
                {
                    this._UmId = value;
                }
            }
        }

        public string CodeBar
        {
            get
            {
                return this._CodeBar;
            }
            set
            {
                if ((this._CodeBar != value))
                {
                    this._CodeBar = value;
                }
            }
        }

        public string PrdDescription
        {
            get
            {
                return this._PrdDescription;
            }
            set
            {
                if ((this._PrdDescription != value))
                {
                    this._PrdDescription = value;
                }
            }
        }

        public string PrdImage
        {
            get
            {
                return this._PrdImage;
            }
            set
            {
                if ((this._PrdImage != value))
                {
                    this._PrdImage = value;
                }
            }
        }

        public decimal PrdCst
        {
            get
            {
                return this._PrdCst;
            }
            set
            {
                if ((this._PrdCst != value))
                {
                    this._PrdCst = value;
                }
            }
        }

        public decimal PrdVnt
        {
            get
            {
                return this._PrdVnt;
            }
            set
            {
                if ((this._PrdVnt != value))
                {
                    this._PrdVnt = value;
                }
            }
        }

        public string PrdObser
        {
            get
            {
                return this._PrdObser;
            }
            set
            {
                if ((this._PrdObser != value))
                {
                    this._PrdObser = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public Product()
        {
            this.PrdId = 0;
            this.BsnId = 0;
            this.PrdTpid = 0;
            this.PrdLnId = 0;
            this.SbPrdLnId = 0;
            this.UmId = 0;
            this.CodeBar = string.Empty;
            this.PrdDescription = string.Empty;
            this.PrdImage = string.Empty;
            this.PrdCst = 0;
            this.PrdVnt = 0;
            this.PrdObser = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
