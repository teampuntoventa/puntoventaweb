﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{

    public class Branch
    {
        #region Variables
        private Int32 _BrnId;
        private Int32 _BsnId;
        private string _BrnDescription;
        private string _BrnType;
        private Int32 _BrnRefId;
        private Int32 _BrnVen;
        private Int32 _BrnCom;
        private Int32 _BrnIng;
        private Int32 _BrnSal;
        private Int32 _BrnTrns;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 BrnId
        {
            get
            {
                return this._BrnId;
            }
            set
            {
                if ((this._BrnId != value))
                {
                    this._BrnId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public string BrnDescription
        {
            get
            {
                return this._BrnDescription;
            }
            set
            {
                if ((this._BrnDescription != value))
                {
                    this._BrnDescription = value;
                }
            }
        }

        public string BrnType
        {
            get
            {
                return this._BrnType;
            }
            set
            {
                if ((this._BrnType != value))
                {
                    this._BrnType = value;
                }
            }
        }

        public Int32 BrnRefId
        {
            get
            {
                return this._BrnRefId;
            }
            set
            {
                if ((this._BrnRefId != value))
                {
                    this._BrnRefId = value;
                }
            }
        }

        public Int32 BrnVen
        {
            get
            {
                return this._BrnVen;
            }
            set
            {
                if ((this._BrnVen != value))
                {
                    this._BrnVen = value;
                }
            }
        }

        public Int32 BrnCom
        {
            get
            {
                return this._BrnCom;
            }
            set
            {
                if ((this._BrnCom != value))
                {
                    this._BrnCom = value;
                }
            }
        }

        public Int32 BrnIng
        {
            get
            {
                return this._BrnIng;
            }
            set
            {
                if ((this._BrnIng != value))
                {
                    this._BrnIng = value;
                }
            }
        }

        public Int32 BrnSal
        {
            get
            {
                return this._BrnSal;
            }
            set
            {
                if ((this._BrnSal != value))
                {
                    this._BrnSal = value;
                }
            }
        }

        public Int32 BrnTrns
        {
            get
            {
                return this._BrnTrns;
            }
            set
            {
                if ((this._BrnTrns != value))
                {
                    this._BrnTrns = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public Branch()
        {
            this.BrnId = 0;
            this.BsnId = 0;
            this.BrnDescription = string.Empty;
            this.BrnType = string.Empty;
            this.BrnRefId = 0;
            this.BrnVen = 0;
            this.BrnCom = 0;
            this.BrnIng = 0;
            this.BrnSal = 0;
            this.BrnTrns = 0;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
