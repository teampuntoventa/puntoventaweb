﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class ProductImage
    {
        #region Variables
        private Int32 _PrdImgId;
        private Int32 _PrdId;
        private Int32 _BsnId;
        private string _PrdImgRuta;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 PrdImgId
        {
            get
            {
                return this._PrdImgId;
            }
            set
            {
                if ((this._PrdImgId != value))
                {
                    this._PrdImgId = value;
                }
            }
        }

        public Int32 PrdId
        {
            get
            {
                return this._PrdId;
            }
            set
            {
                if ((this._PrdId != value))
                {
                    this._PrdId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public string PrdImgRuta
        {
            get
            {
                return this._PrdImgRuta;
            }
            set
            {
                if ((this._PrdImgRuta != value))
                {
                    this._PrdImgRuta = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public ProductImage()
        {
            this.PrdImgId = 0;
            this.PrdId = 0;
            this.BsnId = 0;
            this.PrdImgRuta = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
