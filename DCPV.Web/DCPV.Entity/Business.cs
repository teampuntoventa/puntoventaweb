﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{

    public class Business
    {
        #region Variables
        private Int32 _BsnId;
        private string _BsnRUC;
        private string _BsnReason;
        private string _BsnAddress;
        private string _BsnDescription;
        private string _BsnLogo;
        private string _BsnNameContact;
        private string _BsnEmail;
        private string _BsnTel;
        private string _BsnInfAdd;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public string BsnRUC
        {
            get
            {
                return this._BsnRUC;
            }
            set
            {
                if ((this._BsnRUC != value))
                {
                    this._BsnRUC = value;
                }
            }
        }

        public string BsnReason
        {
            get
            {
                return this._BsnReason;
            }
            set
            {
                if ((this._BsnReason != value))
                {
                    this._BsnReason = value;
                }
            }
        }

        public string BsnAddress
        {
            get
            {
                return this._BsnAddress;
            }
            set
            {
                if ((this._BsnAddress != value))
                {
                    this._BsnAddress = value;
                }
            }
        }

        public string BsnDescription
        {
            get
            {
                return this._BsnDescription;
            }
            set
            {
                if ((this._BsnDescription != value))
                {
                    this._BsnDescription = value;
                }
            }
        }

        public string BsnLogo
        {
            get
            {
                return this._BsnLogo;
            }
            set
            {
                if ((this._BsnLogo != value))
                {
                    this._BsnLogo = value;
                }
            }
        }

        public string BsnNameContact
        {
            get
            {
                return this._BsnNameContact;
            }
            set
            {
                if ((this._BsnNameContact != value))
                {
                    this._BsnNameContact = value;
                }
            }
        }

        public string BsnEmail
        {
            get
            {
                return this._BsnEmail;
            }
            set
            {
                if ((this._BsnEmail != value))
                {
                    this._BsnEmail = value;
                }
            }
        }

        public string BsnTel
        {
            get
            {
                return this._BsnTel;
            }
            set
            {
                if ((this._BsnTel != value))
                {
                    this._BsnTel = value;
                }
            }
        }

        public string BsnInfAdd
        {
            get
            {
                return this._BsnInfAdd;
            }
            set
            {
                if ((this._BsnInfAdd != value))
                {
                    this._BsnInfAdd = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public Business()
        {
            this.BsnId = 0;
            this.BsnRUC = string.Empty;
            this.BsnReason = string.Empty;
            this.BsnAddress = string.Empty;
            this.BsnDescription = string.Empty;
            this.BsnLogo = string.Empty;
            this.BsnNameContact = string.Empty;
            this.BsnEmail = string.Empty;
            this.BsnTel = string.Empty;
            this.BsnInfAdd = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
