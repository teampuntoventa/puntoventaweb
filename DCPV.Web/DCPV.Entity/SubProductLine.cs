﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class SubProductLine
    {
        #region Variables
        private Int32 _SbPrdLnId;
        private Int32 _BsnId;
        private Int32 _PrdTpid;
        private Int32 _PrdLnId;
        private string _SbPrdLnDescription;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 SbPrdLnId
        {
            get
            {
                return this._SbPrdLnId;
            }
            set
            {
                if ((this._SbPrdLnId != value))
                {
                    this._SbPrdLnId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 PrdTpid
        {
            get
            {
                return this._PrdTpid;
            }
            set
            {
                if ((this._PrdTpid != value))
                {
                    this._PrdTpid = value;
                }
            }
        }

        public Int32 PrdLnId
        {
            get
            {
                return this._PrdLnId;
            }
            set
            {
                if ((this._PrdLnId != value))
                {
                    this._PrdLnId = value;
                }
            }
        }

        public string SbPrdLnDescription
        {
            get
            {
                return this._SbPrdLnDescription;
            }
            set
            {
                if ((this._SbPrdLnDescription != value))
                {
                    this._SbPrdLnDescription = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public SubProductLine()
        {
            this.SbPrdLnId = 0;
            this.BsnId = 0;
            this.PrdTpid = 0;
            this.PrdLnId = 0;
            this.SbPrdLnDescription = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
