﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class MovementDetail
    {
        #region Variables
        private Int32 _MvnDtlId;
        private Int32 _MvnId;
        private Int32 _BrnId;
        private Int32 _PrdId;
        private string _PrdDesc;
        private string _MvnDtlStatus;
        private Int32 _DcmObjBaseId;
        private Int32 _MvnBaseId;
        private string _MvnDtlCur;
        private decimal _MvnDtlRate;
        private string _MvnDtlAcctCode;
        private string _MvnDtlTaxCode;
        private string _MvnDtlCostingCode;
        private decimal _MvnDtlQty;
        private decimal _MvnDtlVnt;
        private decimal _MvnDtlDsct;
        private decimal _MvnDtlMnt;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 MvnDtlId
        {
            get
            {
                return this._MvnDtlId;
            }
            set
            {
                if ((this._MvnDtlId != value))
                {
                    this._MvnDtlId = value;
                }
            }
        }

        public Int32 MvnId
        {
            get
            {
                return this._MvnId;
            }
            set
            {
                if ((this._MvnId != value))
                {
                    this._MvnId = value;
                }
            }
        }

        public Int32 BrnId
        {
            get
            {
                return this._BrnId;
            }
            set
            {
                if ((this._BrnId != value))
                {
                    this._BrnId = value;
                }
            }
        }

        public Int32 PrdId
        {
            get
            {
                return this._PrdId;
            }
            set
            {
                if ((this._PrdId != value))
                {
                    this._PrdId = value;
                }
            }
        }

        public string PrdDesc
        {
            get
            {
                return this._PrdDesc;
            }
            set
            {
                if ((this._PrdDesc != value))
                {
                    this._PrdDesc = value;
                }
            }
        }

        public string MvnDtlStatus
        {
            get
            {
                return this._MvnDtlStatus;
            }
            set
            {
                if ((this._MvnDtlStatus != value))
                {
                    this._MvnDtlStatus = value;
                }
            }
        }

        public Int32 DcmObjBaseId
        {
            get
            {
                return this._DcmObjBaseId;
            }
            set
            {
                if ((this._DcmObjBaseId != value))
                {
                    this._DcmObjBaseId = value;
                }
            }
        }

        public Int32 MvnBaseId
        {
            get
            {
                return this._MvnBaseId;
            }
            set
            {
                if ((this._MvnBaseId != value))
                {
                    this._MvnBaseId = value;
                }
            }
        }

        public string MvnDtlCur
        {
            get
            {
                return this._MvnDtlCur;
            }
            set
            {
                if ((this._MvnDtlCur != value))
                {
                    this._MvnDtlCur = value;
                }
            }
        }

        public decimal MvnDtlRate
        {
            get
            {
                return this._MvnDtlRate;
            }
            set
            {
                if ((this._MvnDtlRate != value))
                {
                    this._MvnDtlRate = value;
                }
            }
        }

        public string MvnDtlAcctCode
        {
            get
            {
                return this._MvnDtlAcctCode;
            }
            set
            {
                if ((this._MvnDtlAcctCode != value))
                {
                    this._MvnDtlAcctCode = value;
                }
            }
        }

        public string MvnDtlTaxCode
        {
            get
            {
                return this._MvnDtlTaxCode;
            }
            set
            {
                if ((this._MvnDtlTaxCode != value))
                {
                    this._MvnDtlTaxCode = value;
                }
            }
        }

        public string MvnDtlCostingCode
        {
            get
            {
                return this._MvnDtlCostingCode;
            }
            set
            {
                if ((this._MvnDtlCostingCode != value))
                {
                    this._MvnDtlCostingCode = value;
                }
            }
        }

        public decimal MvnDtlQty
        {
            get
            {
                return this._MvnDtlQty;
            }
            set
            {
                if ((this._MvnDtlQty != value))
                {
                    this._MvnDtlQty = value;
                }
            }
        }

        public decimal MvnDtlVnt
        {
            get
            {
                return this._MvnDtlVnt;
            }
            set
            {
                if ((this._MvnDtlVnt != value))
                {
                    this._MvnDtlVnt = value;
                }
            }
        }

        public decimal MvnDtlDsct
        {
            get
            {
                return this._MvnDtlDsct;
            }
            set
            {
                if ((this._MvnDtlDsct != value))
                {
                    this._MvnDtlDsct = value;
                }
            }
        }

        public decimal MvnDtlMnt
        {
            get
            {
                return this._MvnDtlMnt;
            }
            set
            {
                if ((this._MvnDtlMnt != value))
                {
                    this._MvnDtlMnt = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public MovementDetail()
        {
            this.MvnDtlId = 0;
            this.MvnId = 0;
            this.BrnId = 0;
            this.PrdId = 0;
            this.PrdDesc = string.Empty;
            this.MvnDtlStatus = string.Empty;
            this.DcmObjBaseId = 0;
            this.MvnBaseId = 0;
            this.MvnDtlCur = string.Empty;
            this.MvnDtlRate = 0;
            this.MvnDtlAcctCode = string.Empty;
            this.MvnDtlTaxCode = string.Empty;
            this.MvnDtlCostingCode = string.Empty;
            this.MvnDtlQty = 0;
            this.MvnDtlVnt = 0;
            this.MvnDtlDsct = 0;
            this.MvnDtlMnt = 0;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
