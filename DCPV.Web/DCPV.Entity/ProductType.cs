﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class ProductType
    {
        #region Variables
        private Int32 _PrdTpId;
        private Int32 _BsnId;
        private string _PrdTpDescription;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 PrdTpId
        {
            get
            {
                return this._PrdTpId;
            }
            set
            {
                if ((this._PrdTpId != value))
                {
                    this._PrdTpId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public string PrdTpDescription
        {
            get
            {
                return this._PrdTpDescription;
            }
            set
            {
                if ((this._PrdTpDescription != value))
                {
                    this._PrdTpDescription = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public ProductType()
        {
            this.PrdTpId = 0;
            this.BsnId = 0;
            this.PrdTpDescription = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
