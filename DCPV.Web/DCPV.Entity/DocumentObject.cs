﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{

    public class DocumentObject
    {
        #region Variables
        private Int32 _DcmObjId;
        private Int32 _BsnId;
        private Int32 _MvnTpId;
        private string _DcmObjDesc;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 DcmObjId
        {
            get
            {
                return this._DcmObjId;
            }
            set
            {
                if ((this._DcmObjId != value))
                {
                    this._DcmObjId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 MvnTpId
        {
            get
            {
                return this._MvnTpId;
            }
            set
            {
                if ((this._MvnTpId != value))
                {
                    this._MvnTpId = value;
                }
            }
        }

        public string DcmObjDesc
        {
            get
            {
                return this._DcmObjDesc;
            }
            set
            {
                if ((this._DcmObjDesc != value))
                {
                    this._DcmObjDesc = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public DocumentObject()
        {
            this.DcmObjId = 0;
            this.BsnId = 0;
            this.MvnTpId = 0;
            this.DcmObjDesc = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
