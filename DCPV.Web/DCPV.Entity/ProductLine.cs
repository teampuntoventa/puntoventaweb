﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class ProductLine
    {
        #region Variables
        private Int32 _PrdLnId;
        private Int32 _BsnId;
        private Int32 _PrdTpid;
        private string _PrdLnDescription;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 PrdLnId
        {
            get
            {
                return this._PrdLnId;
            }
            set
            {
                if ((this._PrdLnId != value))
                {
                    this._PrdLnId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 PrdTpid
        {
            get
            {
                return this._PrdTpid;
            }
            set
            {
                if ((this._PrdTpid != value))
                {
                    this._PrdTpid = value;
                }
            }
        }

        public string PrdLnDescription
        {
            get
            {
                return this._PrdLnDescription;
            }
            set
            {
                if ((this._PrdLnDescription != value))
                {
                    this._PrdLnDescription = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public ProductLine()
        {
            this.PrdLnId = 0;
            this.BsnId = 0;
            this.PrdTpid = 0;
            this.PrdLnDescription = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
