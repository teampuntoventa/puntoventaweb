﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class Exercise
    {
        #region Variables
        private Int32 _ExrId;
        private Int32 _BsnId;
        private string _ExrDescription;
        private string _ExrYear;
        private Int32 _ExrClosed;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 ExrId
        {
            get
            {
                return this._ExrId;
            }
            set
            {
                if ((this._ExrId != value))
                {
                    this._ExrId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public string ExrDescription
        {
            get
            {
                return this._ExrDescription;
            }
            set
            {
                if ((this._ExrDescription != value))
                {
                    this._ExrDescription = value;
                }
            }
        }

        public string ExrYear
        {
            get
            {
                return this._ExrYear;
            }
            set
            {
                if ((this._ExrYear != value))
                {
                    this._ExrYear = value;
                }
            }
        }

        public Int32 ExrClosed
        {
            get
            {
                return this._ExrClosed;
            }
            set
            {
                if ((this._ExrClosed != value))
                {
                    this._ExrClosed = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public Exercise()
        {
            this.ExrId = 0;
            this.BsnId = 0;
            this.ExrDescription = string.Empty;
            this.ExrYear = string.Empty;
            this.ExrClosed = 0;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
