﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{

    public class Config
    {
        #region Variables
        private Int32 _CnfId;
        private string _CnfIdApp;
        private string _CnfKey;
        private Int32 _BsnId;
        private Int32 _FrmImpId;
        private Int32 _CnfLauncherCam;
        private Int32 _CnfInfCustomer;
        private string _CnfMon;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 CnfId
        {
            get
            {
                return this._CnfId;
            }
            set
            {
                if ((this._CnfId != value))
                {
                    this._CnfId = value;
                }
            }
        }

        public string CnfIdApp
        {
            get
            {
                return this._CnfIdApp;
            }
            set
            {
                if ((this._CnfIdApp != value))
                {
                    this._CnfIdApp = value;
                }
            }
        }

        public string CnfKey
        {
            get
            {
                return this._CnfKey;
            }
            set
            {
                if ((this._CnfKey != value))
                {
                    this._CnfKey = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 FrmImpId
        {
            get
            {
                return this._FrmImpId;
            }
            set
            {
                if ((this._FrmImpId != value))
                {
                    this._FrmImpId = value;
                }
            }
        }

        public Int32 CnfLauncherCam
        {
            get
            {
                return this._CnfLauncherCam;
            }
            set
            {
                if ((this._CnfLauncherCam != value))
                {
                    this._CnfLauncherCam = value;
                }
            }
        }

        public Int32 CnfInfCustomer
        {
            get
            {
                return this._CnfInfCustomer;
            }
            set
            {
                if ((this._CnfInfCustomer != value))
                {
                    this._CnfInfCustomer = value;
                }
            }
        }

        public string CnfMon
        {
            get
            {
                return this._CnfMon;
            }
            set
            {
                if ((this._CnfMon != value))
                {
                    this._CnfMon = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public Config()
        {
            this.CnfId = 0;
            this.CnfIdApp = string.Empty;
            this.CnfKey = string.Empty;
            this.BsnId = 0;
            this.FrmImpId = 0;
            this.CnfLauncherCam = 0;
            this.CnfInfCustomer = 0;
            this.CnfMon = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
