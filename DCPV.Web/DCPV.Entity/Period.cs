﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class Period
    {
        #region Variables
        private Int32 _PerdId;
        private Int32 _BsnId;
        private Int32 _ExrId;
        private DateTime _PerdFecIni;
        private DateTime _PerdFecFin;
        private string _PerdDescription;
        private Int32 _PerdClosed;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 PerdId
        {
            get
            {
                return this._PerdId;
            }
            set
            {
                if ((this._PerdId != value))
                {
                    this._PerdId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 ExrId
        {
            get
            {
                return this._ExrId;
            }
            set
            {
                if ((this._ExrId != value))
                {
                    this._ExrId = value;
                }
            }
        }

        public DateTime PerdFecIni
        {
            get
            {
                return this._PerdFecIni;
            }
            set
            {
                if ((this._PerdFecIni != value))
                {
                    this._PerdFecIni = value;
                }
            }
        }

        public DateTime PerdFecFin
        {
            get
            {
                return this._PerdFecFin;
            }
            set
            {
                if ((this._PerdFecFin != value))
                {
                    this._PerdFecFin = value;
                }
            }
        }

        public string PerdDescription
        {
            get
            {
                return this._PerdDescription;
            }
            set
            {
                if ((this._PerdDescription != value))
                {
                    this._PerdDescription = value;
                }
            }
        }

        public Int32 PerdClosed
        {
            get
            {
                return this._PerdClosed;
            }
            set
            {
                if ((this._PerdClosed != value))
                {
                    this._PerdClosed = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public Period()
        {
            this.PerdId = 0;
            this.BsnId = 0;
            this.ExrId = 0;
            this.PerdFecIni = DateTime.Parse("01/01/1900");
            this.PerdFecFin = DateTime.Parse("01/01/1900");
            this.PerdDescription = string.Empty;
            this.PerdClosed = 0;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
