﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{

    public class MovementType
    {
        #region Variables
        private Int32 _MvnTpId;
        private Int32 _BsnId;
        private Int32 _ExrId;
        private string _MvnTpDesc;
        private string _MvnTpType;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 MvnTpId
        {
            get
            {
                return this._MvnTpId;
            }
            set
            {
                if ((this._MvnTpId != value))
                {
                    this._MvnTpId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 ExrId
        {
            get
            {
                return this._ExrId;
            }
            set
            {
                if ((this._ExrId != value))
                {
                    this._ExrId = value;
                }
            }
        }

        public string MvnTpDesc
        {
            get
            {
                return this._MvnTpDesc;
            }
            set
            {
                if ((this._MvnTpDesc != value))
                {
                    this._MvnTpDesc = value;
                }
            }
        }

        public string MvnTpType
        {
            get
            {
                return this._MvnTpType;
            }
            set
            {
                if ((this._MvnTpType != value))
                {
                    this._MvnTpType = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public MovementType()
        {
            this.MvnTpId = 0;
            this.BsnId = 0;
            this.ExrId = 0;
            this.MvnTpDesc = string.Empty;
            this.MvnTpType = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
