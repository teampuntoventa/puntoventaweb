﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class UM
    {
        #region Variables
        private Int32 _UmId;
        private Int32 _BsnId;
        private string _UmDescription;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 UmId
        {
            get
            {
                return this._UmId;
            }
            set
            {
                if ((this._UmId != value))
                {
                    this._UmId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public string UmDescription
        {
            get
            {
                return this._UmDescription;
            }
            set
            {
                if ((this._UmDescription != value))
                {
                    this._UmDescription = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public UM()
        {
            this.UmId = 0;
            this.BsnId = 0;
            this.UmDescription = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
