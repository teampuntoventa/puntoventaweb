﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class ListPriceSale
    {
        #region Variables
        private Int32 _LstPrcSlId;
        private Int32 _BsnId;
        private Int32 _PrdId;
        private Int32 _LstPrcSlNum;
        private decimal _LstPrcSlVnt;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 LstPrcSlId
        {
            get
            {
                return this._LstPrcSlId;
            }
            set
            {
                if ((this._LstPrcSlId != value))
                {
                    this._LstPrcSlId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 PrdId
        {
            get
            {
                return this._PrdId;
            }
            set
            {
                if ((this._PrdId != value))
                {
                    this._PrdId = value;
                }
            }
        }

        public Int32 LstPrcSlNum
        {
            get
            {
                return this._LstPrcSlNum;
            }
            set
            {
                if ((this._LstPrcSlNum != value))
                {
                    this._LstPrcSlNum = value;
                }
            }
        }

        public decimal LstPrcSlVnt
        {
            get
            {
                return this._LstPrcSlVnt;
            }
            set
            {
                if ((this._LstPrcSlVnt != value))
                {
                    this._LstPrcSlVnt = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public ListPriceSale()
        {
            this.LstPrcSlId = 0;
            this.BsnId = 0;
            this.PrdId = 0;
            this.LstPrcSlNum = 0;
            this.LstPrcSlVnt = 0;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
