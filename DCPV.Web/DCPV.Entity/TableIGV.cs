﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class TableIGV
    {
        #region Variables
        private string _Code;
        private string _Name;
        private string _Description;
        private decimal _Rate;
        private string _ValidForAR;
        private string _ValidForAP;
        private string _SalesTax;
        private string _PurchTax;
        private decimal _MinAmount;
        private decimal _MaxAmount;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public string Code
        {
            get
            {
                return this._Code;
            }
            set
            {
                if ((this._Code != value))
                {
                    this._Code = value;
                }
            }
        }

        public string Name
        {
            get
            {
                return this._Name;
            }
            set
            {
                if ((this._Name != value))
                {
                    this._Name = value;
                }
            }
        }

        public string Description
        {
            get
            {
                return this._Description;
            }
            set
            {
                if ((this._Description != value))
                {
                    this._Description = value;
                }
            }
        }

        public decimal Rate
        {
            get
            {
                return this._Rate;
            }
            set
            {
                if ((this._Rate != value))
                {
                    this._Rate = value;
                }
            }
        }

        public string ValidForAR
        {
            get
            {
                return this._ValidForAR;
            }
            set
            {
                if ((this._ValidForAR != value))
                {
                    this._ValidForAR = value;
                }
            }
        }

        public string ValidForAP
        {
            get
            {
                return this._ValidForAP;
            }
            set
            {
                if ((this._ValidForAP != value))
                {
                    this._ValidForAP = value;
                }
            }
        }

        public string SalesTax
        {
            get
            {
                return this._SalesTax;
            }
            set
            {
                if ((this._SalesTax != value))
                {
                    this._SalesTax = value;
                }
            }
        }

        public string PurchTax
        {
            get
            {
                return this._PurchTax;
            }
            set
            {
                if ((this._PurchTax != value))
                {
                    this._PurchTax = value;
                }
            }
        }

        public decimal MinAmount
        {
            get
            {
                return this._MinAmount;
            }
            set
            {
                if ((this._MinAmount != value))
                {
                    this._MinAmount = value;
                }
            }
        }

        public decimal MaxAmount
        {
            get
            {
                return this._MaxAmount;
            }
            set
            {
                if ((this._MaxAmount != value))
                {
                    this._MaxAmount = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public TableIGV()
        {
            this.Code = string.Empty;
            this.Name = string.Empty;
            this.Description = string.Empty;
            this.Rate = 0;
            this.ValidForAR = string.Empty;
            this.ValidForAP = string.Empty;
            this.SalesTax = string.Empty;
            this.PurchTax = string.Empty;
            this.MinAmount = 0;
            this.MaxAmount = 0;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
