﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{

    public class BusinessPartner
    {
        #region Variables
        private Int32 _BsnPrtId;
        private Int32 _BsnId;
        private Int32 _DcmTpId;
        private Int32 _BsnPrtType;
        private string _BsnPrtNumDoc;
        private string _BsnPrtReason;
        private string _BsnPrtContact;
        private string _BsnPrtAlias;
        private string _BsnPrtTlf1;
        private string _BsnPrtTlf2;
        private string _BsnPrtEmail;
        private string _BsnPrtDirección;
        private string _BsnPrtMapLat;
        private string _BsnPrtMapLng;
        private string _BsnPrtObs;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 BsnPrtId
        {
            get
            {
                return this._BsnPrtId;
            }
            set
            {
                if ((this._BsnPrtId != value))
                {
                    this._BsnPrtId = value;
                }
            }
        }

        public Int32 BsnId
        {
            get
            {
                return this._BsnId;
            }
            set
            {
                if ((this._BsnId != value))
                {
                    this._BsnId = value;
                }
            }
        }

        public Int32 DcmTpId
        {
            get
            {
                return this._DcmTpId;
            }
            set
            {
                if ((this._DcmTpId != value))
                {
                    this._DcmTpId = value;
                }
            }
        }

        public Int32 BsnPrtType
        {
            get
            {
                return this._BsnPrtType;
            }
            set
            {
                if ((this._BsnPrtType != value))
                {
                    this._BsnPrtType = value;
                }
            }
        }

        public string BsnPrtNumDoc
        {
            get
            {
                return this._BsnPrtNumDoc;
            }
            set
            {
                if ((this._BsnPrtNumDoc != value))
                {
                    this._BsnPrtNumDoc = value;
                }
            }
        }

        public string BsnPrtReason
        {
            get
            {
                return this._BsnPrtReason;
            }
            set
            {
                if ((this._BsnPrtReason != value))
                {
                    this._BsnPrtReason = value;
                }
            }
        }

        public string BsnPrtContact
        {
            get
            {
                return this._BsnPrtContact;
            }
            set
            {
                if ((this._BsnPrtContact != value))
                {
                    this._BsnPrtContact = value;
                }
            }
        }

        public string BsnPrtAlias
        {
            get
            {
                return this._BsnPrtAlias;
            }
            set
            {
                if ((this._BsnPrtAlias != value))
                {
                    this._BsnPrtAlias = value;
                }
            }
        }

        public string BsnPrtTlf1
        {
            get
            {
                return this._BsnPrtTlf1;
            }
            set
            {
                if ((this._BsnPrtTlf1 != value))
                {
                    this._BsnPrtTlf1 = value;
                }
            }
        }

        public string BsnPrtTlf2
        {
            get
            {
                return this._BsnPrtTlf2;
            }
            set
            {
                if ((this._BsnPrtTlf2 != value))
                {
                    this._BsnPrtTlf2 = value;
                }
            }
        }

        public string BsnPrtEmail
        {
            get
            {
                return this._BsnPrtEmail;
            }
            set
            {
                if ((this._BsnPrtEmail != value))
                {
                    this._BsnPrtEmail = value;
                }
            }
        }

        public string BsnPrtDirección
        {
            get
            {
                return this._BsnPrtDirección;
            }
            set
            {
                if ((this._BsnPrtDirección != value))
                {
                    this._BsnPrtDirección = value;
                }
            }
        }

        public string BsnPrtMapLat
        {
            get
            {
                return this._BsnPrtMapLat;
            }
            set
            {
                if ((this._BsnPrtMapLat != value))
                {
                    this._BsnPrtMapLat = value;
                }
            }
        }

        public string BsnPrtMapLng
        {
            get
            {
                return this._BsnPrtMapLng;
            }
            set
            {
                if ((this._BsnPrtMapLng != value))
                {
                    this._BsnPrtMapLng = value;
                }
            }
        }

        public string BsnPrtObs
        {
            get
            {
                return this._BsnPrtObs;
            }
            set
            {
                if ((this._BsnPrtObs != value))
                {
                    this._BsnPrtObs = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public BusinessPartner()
        {
            this.BsnPrtId = 0;
            this.BsnId = 0;
            this.DcmTpId = 0;
            this.BsnPrtType = 0;
            this.BsnPrtNumDoc = string.Empty;
            this.BsnPrtReason = string.Empty;
            this.BsnPrtContact = string.Empty;
            this.BsnPrtAlias = string.Empty;
            this.BsnPrtTlf1 = string.Empty;
            this.BsnPrtTlf2 = string.Empty;
            this.BsnPrtEmail = string.Empty;
            this.BsnPrtDirección = string.Empty;
            this.BsnPrtMapLat = string.Empty;
            this.BsnPrtMapLng = string.Empty;
            this.BsnPrtObs = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
