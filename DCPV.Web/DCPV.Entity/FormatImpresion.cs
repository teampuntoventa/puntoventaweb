﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class FormatImpresion
    {
        #region Variables
        private Int32 _FrmImpId;
        private string _FrmImpDescripction;
        private string _FrmImpCampoLibre1;
        private string _FrmImpCampoLibre2;
        private string _FrmImpCampoLibre3;
        private string _FrmImpCampoLibre4;
        private Int32 _objType;
        private Int32 _Active;
        #endregion

        #region Propiedades

        public Int32 FrmImpId
        {
            get
            {
                return this._FrmImpId;
            }
            set
            {
                if ((this._FrmImpId != value))
                {
                    this._FrmImpId = value;
                }
            }
        }

        public string FrmImpDescripction
        {
            get
            {
                return this._FrmImpDescripction;
            }
            set
            {
                if ((this._FrmImpDescripction != value))
                {
                    this._FrmImpDescripction = value;
                }
            }
        }

        public string FrmImpCampoLibre1
        {
            get
            {
                return this._FrmImpCampoLibre1;
            }
            set
            {
                if ((this._FrmImpCampoLibre1 != value))
                {
                    this._FrmImpCampoLibre1 = value;
                }
            }
        }

        public string FrmImpCampoLibre2
        {
            get
            {
                return this._FrmImpCampoLibre2;
            }
            set
            {
                if ((this._FrmImpCampoLibre2 != value))
                {
                    this._FrmImpCampoLibre2 = value;
                }
            }
        }

        public string FrmImpCampoLibre3
        {
            get
            {
                return this._FrmImpCampoLibre3;
            }
            set
            {
                if ((this._FrmImpCampoLibre3 != value))
                {
                    this._FrmImpCampoLibre3 = value;
                }
            }
        }

        public string FrmImpCampoLibre4
        {
            get
            {
                return this._FrmImpCampoLibre4;
            }
            set
            {
                if ((this._FrmImpCampoLibre4 != value))
                {
                    this._FrmImpCampoLibre4 = value;
                }
            }
        }

        public Int32 objType
        {
            get
            {
                return this._objType;
            }
            set
            {
                if ((this._objType != value))
                {
                    this._objType = value;
                }
            }
        }

        public Int32 Active
        {
            get
            {
                return this._Active;
            }
            set
            {
                if ((this._Active != value))
                {
                    this._Active = value;
                }
            }
        }
        #endregion

        #region Constructor
        public FormatImpresion()
        {
            this.FrmImpId = 0;
            this.FrmImpDescripction = string.Empty;
            this.FrmImpCampoLibre1 = string.Empty;
            this.FrmImpCampoLibre2 = string.Empty;
            this.FrmImpCampoLibre3 = string.Empty;
            this.FrmImpCampoLibre4 = string.Empty;
            this.objType = 0;
            this.Active = 0;
        }
        #endregion
    }

}
