﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Entity
{
    public class CResult
    {
        public int Result { get; set; }
        public string Error { get; set; }
        public string DocEntry { get; set; }
        public string JrnlMemo { get; set; }
        public string CodigoExterno { get; set; }
        public int Result2 { get; set; }
        public string Error2 { get; set; }
        public string DocEntry2 { get; set; }
        public string JrnlMemo2 { get; set; }
        public string CodigoExterno2 { get; set; }
        public List<string> LstCodigoExterno { get; set; }

        public CResult()
        {
            Result = -1;
            Error = string.Empty;
            DocEntry = string.Empty;
            JrnlMemo = string.Empty;
            CodigoExterno = string.Empty;
            Result2 = 0;
            Error2 = string.Empty;
            DocEntry2 = string.Empty;
            JrnlMemo2 = string.Empty;
            CodigoExterno2 = string.Empty;
            LstCodigoExterno = new List<string>();
        }
        public CResult(int pResult, string pError, string pDocEntry, string pJrnlMemo, string pCodigoExterno)
        {
            Result = pResult;
            Error = pError;
            DocEntry = pDocEntry;
            JrnlMemo = pJrnlMemo;
            CodigoExterno = pCodigoExterno;
            Result2 = 0;
            Error2 = string.Empty;
            DocEntry2 = string.Empty;
            JrnlMemo2 = string.Empty;
            CodigoExterno2 = string.Empty;
            LstCodigoExterno = new List<string>();
        }
        public CResult(string pJrnlMemo, string pCodigoExterno)
        {
            Result = -1;
            Error = string.Empty;
            DocEntry = string.Empty;
            JrnlMemo = pJrnlMemo;
            CodigoExterno = pCodigoExterno;
            Result2 = 0;
            Error2 = string.Empty;
            DocEntry2 = string.Empty;
            JrnlMemo2 = string.Empty;
            CodigoExterno2 = string.Empty;
            LstCodigoExterno = new List<string>();
        }
        public CResult(string Error)
        {
            Result = -1;
            Error = string.Empty;
            DocEntry = string.Empty;
            JrnlMemo = string.Empty;
            CodigoExterno = string.Empty;
            Result2 = 0;
            Error2 = string.Empty;
            DocEntry2 = string.Empty;
            JrnlMemo2 = string.Empty;
            CodigoExterno2 = string.Empty;
            LstCodigoExterno = new List<string>();
        }
    }
}
