﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantBranchProductProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new BranchProduct());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(BranchProduct p)
        {
            CResult result = new CResult();
            try
            {
                iloBranchProduct ilo = new logBranchProduct();
                result = p.BrnPrdId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(BranchProduct p)
        {
            CResult result = new CResult();
            try
            {
                iloBranchProduct ilo = new logBranchProduct();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(BranchProduct p)
        {
            List<BranchProduct> oLista = new List<BranchProduct>();
            try
            {
                iloBranchProduct ilo = new logBranchProduct();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<BranchProduct>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(BranchProduct p)
        {
            BranchProduct oObj = new BranchProduct();
            try
            {
                iloBranchProduct ilo = new logBranchProduct();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new BranchProduct();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}