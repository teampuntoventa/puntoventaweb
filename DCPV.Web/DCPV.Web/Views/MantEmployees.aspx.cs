﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantEmployees : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new Employees());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(Employees p)
        {
            CResult result = new CResult();
            try
            {
                iloEmployees ilo = new logEmployees();
                result = p.EplId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(Employees p)
        {
            CResult result = new CResult();
            try
            {
                iloEmployees ilo = new logEmployees();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(Employees p)
        {
            List<Employees> oLista = new List<Employees>();
            try
            {
                iloEmployees ilo = new logEmployees();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<Employees>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(Employees p)
        {
            Employees oObj = new Employees();
            try
            {
                iloEmployees ilo = new logEmployees();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new Employees();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}