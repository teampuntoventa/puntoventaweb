﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantDocumentObject : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new DocumentObject());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(DocumentObject p)
        {
            CResult result = new CResult();
            try
            {
                iloDocumentObject ilo = new logDocumentObject();
                result = p.DcmObjId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(DocumentObject p)
        {
            CResult result = new CResult();
            try
            {
                iloDocumentObject ilo = new logDocumentObject();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(DocumentObject p)
        {
            List<DocumentObject> oLista = new List<DocumentObject>();
            try
            {
                iloDocumentObject ilo = new logDocumentObject();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<DocumentObject>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(DocumentObject p)
        {
            DocumentObject oObj = new DocumentObject();
            try
            {
                iloDocumentObject ilo = new logDocumentObject();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new DocumentObject();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}