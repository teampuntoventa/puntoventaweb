﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantConfig : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new Config());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(Config p)
        {
            CResult result = new CResult();
            try
            {
                iloConfig ilo = new logConfig();
                result = p.CnfId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(Config p)
        {
            CResult result = new CResult();
            try
            {
                iloConfig ilo = new logConfig();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(Config p)
        {
            List<Config> oLista = new List<Config>();
            try
            {
                iloConfig ilo = new logConfig();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<Config>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(Config p)
        {
            Config oObj = new Config();
            try
            {
                iloConfig ilo = new logConfig();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new Config();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}