﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantMovementType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new MovementType());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(MovementType p)
        {
            CResult result = new CResult();
            try
            {
                iloMovementType ilo = new logMovementType();
                result = p.MvnTpId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(MovementType p)
        {
            CResult result = new CResult();
            try
            {
                iloMovementType ilo = new logMovementType();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(MovementType p)
        {
            List<MovementType> oLista = new List<MovementType>();
            try
            {
                iloMovementType ilo = new logMovementType();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<MovementType>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(MovementType p)
        {
            MovementType oObj = new MovementType();
            try
            {
                iloMovementType ilo = new logMovementType();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new MovementType();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}