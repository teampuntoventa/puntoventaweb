﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantPeriod : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new Period());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(Period p)
        {
            CResult result = new CResult();
            try
            {
                iloPeriod ilo = new logPeriod();
                result = p.PerdId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(Period p)
        {
            CResult result = new CResult();
            try
            {
                iloPeriod ilo = new logPeriod();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(Period p)
        {
            List<Period> oLista = new List<Period>();
            try
            {
                iloPeriod ilo = new logPeriod();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<Period>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(Period p)
        {
            Period oObj = new Period();
            try
            {
                iloPeriod ilo = new logPeriod();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new Period();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}