﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantBranch : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new Branch());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(Branch p)
        {
            CResult result = new CResult();
            try
            {
                iloBranch ilo = new logBranch();
                result = p.BrnId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(Branch p)
        {
            CResult result = new CResult();
            try
            {
                iloBranch ilo = new logBranch();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(Branch p)
        {
            List<Branch> oLista = new List<Branch>();
            try
            {
                iloBranch ilo = new logBranch();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<Branch>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(Branch p)
        {
            Branch oObj = new Branch();
            try
            {
                iloBranch ilo = new logBranch();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new Branch();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}