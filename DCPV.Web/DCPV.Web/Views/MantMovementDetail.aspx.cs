﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantMovementDetail : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new MovementDetail());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(MovementDetail p)
        {
            CResult result = new CResult();
            try
            {
                iloMovementDetail ilo = new logMovementDetail();
                result = p.MvnDtlId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(MovementDetail p)
        {
            CResult result = new CResult();
            try
            {
                iloMovementDetail ilo = new logMovementDetail();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(MovementDetail p)
        {
            List<MovementDetail> oLista = new List<MovementDetail>();
            try
            {
                iloMovementDetail ilo = new logMovementDetail();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<MovementDetail>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(MovementDetail p)
        {
            MovementDetail oObj = new MovementDetail();
            try
            {
                iloMovementDetail ilo = new logMovementDetail();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new MovementDetail();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}