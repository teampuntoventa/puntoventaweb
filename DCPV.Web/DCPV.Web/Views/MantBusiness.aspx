﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteDCPV.Master" AutoEventWireup="true" CodeBehind="MantBusiness.aspx.cs" Inherits="DCPV.Web.Views.MantBusiness" %>
<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <script src="../Scripts/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/Views/Business.js" type="text/javascript"></script>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>LISTADO DE VENTAS</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="../index.aspx">Home</a>
                </li>
                <li>
                    <a>PUNTO DE VENTA</a>
                </li>
                <li class="active">
                    <a href="#"><strong>Busqueda de Ventas</strong></a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="ibox float-e-margins">
                    <div class="ibox-title">
                        <div class="ibox-tools">
                            <button class="btn btn-outline btn-default" type="button" onclick="javascript: jsBusiness.Listar();" data-toggle="button"><i class="fa fa-refresh"></i>&nbsp;&nbsp;Listar</button>
                            <button class="btn btn-outline btn-warning" type="button" onclick="javascript: jsBusiness.irAgregar();" data-toggle="button"><i class="fa fa-plus"></i>&nbsp;&nbsp;Agregar</button>
                            <button class="btn btn-default collapse-link" type="button" data-toggle="button"><i class="fa fa-chevron-up"></i></button>
                        </div>
                    </div>
                    <div class="ibox-content" id="ibox-content">
                        <table id="tbBusiness" class="table table-striped table-bordered table-hover dataTables-example">
                        </table>
                    </div>
                    <div class="ibox-footer"></div>

                </div>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            try {
                jsBusiness.IniFormListar();
            } catch (e) {
                toastr.error(e.message.toString(), Basetitle);
            }
        });
    </script>

</asp:Content>
