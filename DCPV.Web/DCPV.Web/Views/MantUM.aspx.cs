﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantUM : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new UM());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(UM p)
        {
            CResult result = new CResult();
            try
            {
                iloUM ilo = new logUM();
                result = p.UmId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(UM p)
        {
            CResult result = new CResult();
            try
            {
                iloUM ilo = new logUM();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(UM p)
        {
            List<UM> oLista = new List<UM>();
            try
            {
                iloUM ilo = new logUM();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<UM>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(UM p)
        {
            UM oObj = new UM();
            try
            {
                iloUM ilo = new logUM();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new UM();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}