﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantSubProductLine : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new SubProductLine());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(SubProductLine p)
        {
            CResult result = new CResult();
            try
            {
                iloSubProductLine ilo = new logSubProductLine();
                result = p.SbPrdLnId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(SubProductLine p)
        {
            CResult result = new CResult();
            try
            {
                iloSubProductLine ilo = new logSubProductLine();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(SubProductLine p)
        {
            List<SubProductLine> oLista = new List<SubProductLine>();
            try
            {
                iloSubProductLine ilo = new logSubProductLine();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<SubProductLine>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(SubProductLine p)
        {
            SubProductLine oObj = new SubProductLine();
            try
            {
                iloSubProductLine ilo = new logSubProductLine();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new SubProductLine();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}