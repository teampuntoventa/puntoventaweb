﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantBusiness : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new Business());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(Business p)
        {
            CResult result = new CResult();
            try
            {
                iloBusiness ilo = new logBusiness();
                result = p.BsnId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(Business p)
        {
            CResult result = new CResult();
            try
            {
                iloBusiness ilo = new logBusiness();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(Business p)
        {
            List<Business> oLista = new List<Business>();
            try
            {
                iloBusiness ilo = new logBusiness();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<Business>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(Business p)
        {
            Business oObj = new Business();
            try
            {
                iloBusiness ilo = new logBusiness();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new Business();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}