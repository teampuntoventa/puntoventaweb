﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantProduct : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new Product());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(Product p)
        {
            CResult result = new CResult();
            try
            {
                iloProduct ilo = new logProduct();
                result = p.PrdId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(Product p)
        {
            CResult result = new CResult();
            try
            {
                iloProduct ilo = new logProduct();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(Product p)
        {
            List<Product> oLista = new List<Product>();
            try
            {
                iloProduct ilo = new logProduct();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<Product>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(Product p)
        {
            Product oObj = new Product();
            try
            {
                iloProduct ilo = new logProduct();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new Product();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}