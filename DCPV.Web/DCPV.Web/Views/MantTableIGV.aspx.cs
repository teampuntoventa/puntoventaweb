﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantTableIGV : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new TableIGV());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(TableIGV p)
        {
            CResult result = new CResult();
            try
            {
                iloTableIGV ilo = new logTableIGV();
                result = p.Code == "" ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(TableIGV p)
        {
            CResult result = new CResult();
            try
            {
                iloTableIGV ilo = new logTableIGV();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(TableIGV p)
        {
            List<TableIGV> oLista = new List<TableIGV>();
            try
            {
                iloTableIGV ilo = new logTableIGV();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<TableIGV>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(TableIGV p)
        {
            TableIGV oObj = new TableIGV();
            try
            {
                iloTableIGV ilo = new logTableIGV();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new TableIGV();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}