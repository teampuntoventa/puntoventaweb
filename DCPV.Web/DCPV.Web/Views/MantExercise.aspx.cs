﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantExercise : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new Exercise());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(Exercise p)
        {
            CResult result = new CResult();
            try
            {
                iloExercise ilo = new logExercise();
                result = p.ExrId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(Exercise p)
        {
            CResult result = new CResult();
            try
            {
                iloExercise ilo = new logExercise();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(Exercise p)
        {
            List<Exercise> oLista = new List<Exercise>();
            try
            {
                iloExercise ilo = new logExercise();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<Exercise>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(Exercise p)
        {
            Exercise oObj = new Exercise();
            try
            {
                iloExercise ilo = new logExercise();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new Exercise();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}