﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantProductLine : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new ProductLine());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(ProductLine p)
        {
            CResult result = new CResult();
            try
            {
                iloProductLine ilo = new logProductLine();
                result = p.PrdLnId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(ProductLine p)
        {
            CResult result = new CResult();
            try
            {
                iloProductLine ilo = new logProductLine();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(ProductLine p)
        {
            List<ProductLine> oLista = new List<ProductLine>();
            try
            {
                iloProductLine ilo = new logProductLine();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<ProductLine>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(ProductLine p)
        {
            ProductLine oObj = new ProductLine();
            try
            {
                iloProductLine ilo = new logProductLine();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new ProductLine();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}