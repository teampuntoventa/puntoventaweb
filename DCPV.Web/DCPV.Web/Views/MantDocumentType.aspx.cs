﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantDocumentType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new DocumentType());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(DocumentType p)
        {
            CResult result = new CResult();
            try
            {
                iloDocumentType ilo = new logDocumentType();
                result = p.DcmTpId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(DocumentType p)
        {
            CResult result = new CResult();
            try
            {
                iloDocumentType ilo = new logDocumentType();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(DocumentType p)
        {
            List<DocumentType> oLista = new List<DocumentType>();
            try
            {
                iloDocumentType ilo = new logDocumentType();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<DocumentType>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(DocumentType p)
        {
            DocumentType oObj = new DocumentType();
            try
            {
                iloDocumentType ilo = new logDocumentType();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new DocumentType();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}