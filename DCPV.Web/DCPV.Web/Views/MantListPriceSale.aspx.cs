﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantListPriceSale : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new ListPriceSale());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(ListPriceSale p)
        {
            CResult result = new CResult();
            try
            {
                iloListPriceSale ilo = new logListPriceSale();
                result = p.LstPrcSlId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(ListPriceSale p)
        {
            CResult result = new CResult();
            try
            {
                iloListPriceSale ilo = new logListPriceSale();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(ListPriceSale p)
        {
            List<ListPriceSale> oLista = new List<ListPriceSale>();
            try
            {
                iloListPriceSale ilo = new logListPriceSale();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<ListPriceSale>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(ListPriceSale p)
        {
            ListPriceSale oObj = new ListPriceSale();
            try
            {
                iloListPriceSale ilo = new logListPriceSale();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new ListPriceSale();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}