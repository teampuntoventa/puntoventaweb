﻿<%@ Page Title="" Language="C#" MasterPageFile="~/SiteDCPV.Master" AutoEventWireup="true" CodeBehind="MantWebRegisterBusiness.aspx.cs" Inherits="DCPV.Web.Views.MantWebRegisterBusiness" %>

<asp:Content ID="Content1" ContentPlaceHolderID="body" runat="server">
    <script src="../Scripts/jquery-2.1.1.min.js" type="text/javascript"></script>
    <script src="../Scripts/Views/Business.js" type="text/javascript"></script>
    <script src="../Scripts/Views/Exercise.js" type="text/javascript"></script>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-10">
            <h2>REGISTRO DE EMPRESAS</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="../index.aspx">Inicio</a>
                </li>
                <li>
                    <a>MANTENIMIENTO</a>
                </li>
                <li class="active">
                    <a href="../Views/MantWebTicket.aspx"><strong>INGRESO DE EMPRESAS</strong></a>
                </li>
            </ol>
        </div>
        <div class="col-lg-2"></div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight ecommerce">
        <div class="row">
            <div class="col-md-12 col-sm-12 col-xs-12">
                <form id="form-Business" role="form">
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="ibox float-e-margins">
                                    <div class="ibox-title">
                                        <h5>Datos para el Registro de Empresas</h5>
                                        <div class="ibox-tools">
                                            <button class="btn btn-outline btn-success" type="button" onclick="javascript: jsBusiness.irList();" data-toggle="button"><i class="fa fa-share"></i>&nbsp;&nbsp;Ir a Bandeja</button>
                                            <button class="btn btn-default collapse-link" type="button" data-toggle="button"><i class="fa fa-chevron-up"></i></button>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <input type="hidden" id="HdBsnId" value="0" />
                                        <div class="row">
                                            <div class="col-md-3 col-sm-3 col-xs-3">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtBsnRUC">RUC&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese RUC" class="form-control" id="txtBsnRUC" name="txtBsnRUC" />
                                                </div>
                                            </div>
                                            <div class="col-md-9 col-sm-9 col-xs-9">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtBsnReason">Razon Social&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese Razon Social" class="form-control" id="txtBsnReason" name="txtBsnReason" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtBsnAddress">Dirección&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese Dirección" class="form-control" id="txtBsnAddress" name="txtBsnAddress" />
                                                </div>
                                            </div>
                                            <div class="col-md-6 col-sm-6 col-xs-6">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtBsnDescription">Descripción&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese Descripción" class="form-control" id="txtBsnDescription" name="txtBsnDescription" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtBsnLogo">Logo&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese Logo" class="form-control" id="txtBsnLogo" name="txtBsnLogo" />
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtBsnNameContact">Contacto&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese Contacto" class="form-control" id="txtBsnNameContact" name="txtBsnNameContact" />
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtBsnEmail">Email&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese Email" class="form-control" id="txtBsnEmail" name="txtBsnEmail" />
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtBsnTel">Teléfono&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese Teléfono" class="form-control" id="txtBsnTel" name="txtBsnTel" />
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtBsnInfAdd">BsnInfAdd&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese BsnInfAdd" class="form-control" id="txtBsnInfAdd" name="txtBsnInfAdd" />
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4" id="divcboApertura">
                                                <div class="form-group">
                                                    <label class="control-label" for="cboApertura">¿Aperturar Año?&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <select id="cboApertura" class="form-control" onchange="javascript:jsBusiness.ChangeApertura(this);"></select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="ibox float-e-margins hide" id="divRegEjercicios">
                                    <div class="ibox-title">
                                        <h5>Datos para el Registro de Ejercicios</h5>
                                        <div class="ibox-tools">
                                            <button class="btn btn-default collapse-link" type="button" data-toggle="button"><i class="fa fa-chevron-up"></i></button>
                                        </div>
                                    </div>
                                    <div class="ibox-content">
                                        <input type="hidden" id="hdExrId" value="0" />
                                        <div class="row">
                                            <div class="col-md-8 col-sm-8 col-xs-8">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtExrDescription">Descripción&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                                    <input type="text" placeholder="Ingrese Descripción" class="form-control" id="txtExrDescription" name="txtExrDescription" />
                                                </div>
                                            </div>
                                            <div class="col-md-4 col-sm-4 col-xs-4">
                                                <div class="form-group">
                                                    <label class="control-label" for="txtExrYear">Año de Apertura</label>
                                                    <div class="input-group date">
                                                        <span class="input-group-addon"><i class="fa fa-calendar"></i></span>
                                                        <input type="text" id="txtExrYear" name="txtExrYear" class="form-control" placeholder="" />
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="hide">
                                        </div>


                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="ibox-footer">
                        <div class="row">
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="form-group" style="text-align: center">
                                    <button class="btn btn-primary" style="align-content: center" type="submit">Guardar</button>&nbsp;
                                    <button class="btn btn-info" style="align-content: center" type="button" onclick="javascript : jsBusiness.ClearFormRegistro();">Limpiar&nbsp;</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function () {
            try {
                jsBusiness.IniForm();
            } catch (e) {
                toastr.error(e.message.toString(), Basetitle);
            }
        });
    </script>
</asp:Content>
