﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantProductType : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new ProductType());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(ProductType p)
        {
            CResult result = new CResult();
            try
            {
                iloProductType ilo = new logProductType();
                result = p.PrdTpId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(ProductType p)
        {
            CResult result = new CResult();
            try
            {
                iloProductType ilo = new logProductType();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(ProductType p)
        {
            List<ProductType> oLista = new List<ProductType>();
            try
            {
                iloProductType ilo = new logProductType();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<ProductType>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(ProductType p)
        {
            ProductType oObj = new ProductType();
            try
            {
                iloProductType ilo = new logProductType();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new ProductType();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}