﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantMovement : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new Movement());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(Movement p)
        {
            CResult result = new CResult();
            try
            {
                iloMovement ilo = new logMovement();
                result = p.MvnId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(Movement p)
        {
            CResult result = new CResult();
            try
            {
                iloMovement ilo = new logMovement();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(Movement p)
        {
            List<Movement> oLista = new List<Movement>();
            try
            {
                iloMovement ilo = new logMovement();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<Movement>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(Movement p)
        {
            Movement oObj = new Movement();
            try
            {
                iloMovement ilo = new logMovement();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new Movement();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}