﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantBusinessPartner : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new BusinessPartner());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(BusinessPartner p)
        {
            CResult result = new CResult();
            try
            {
                iloBusinessPartner ilo = new logBusinessPartner();
                result = p.BsnPrtId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(BusinessPartner p)
        {
            CResult result = new CResult();
            try
            {
                iloBusinessPartner ilo = new logBusinessPartner();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(BusinessPartner p)
        {
            List<BusinessPartner> oLista = new List<BusinessPartner>();
            try
            {
                iloBusinessPartner ilo = new logBusinessPartner();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<BusinessPartner>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(BusinessPartner p)
        {
            BusinessPartner oObj = new BusinessPartner();
            try
            {
                iloBusinessPartner ilo = new logBusinessPartner();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new BusinessPartner();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}