﻿using DCPV.Entity;
using DCPV.ILogic;
using DCPV.Logic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DCPV.Web.Views
{
    public partial class MantProductImage : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        #region CRUD
        [System.Web.Services.WebMethod]
        public static string Atributos()
        {
            return JsonConvert.SerializeObject(new ProductImage());
        }

        [System.Web.Services.WebMethod]
        public static string Transaccion(ProductImage p)
        {
            CResult result = new CResult();
            try
            {
                iloProductImage ilo = new logProductImage();
                result = p.PrdImgId == 0 ? ilo.add(p) : ilo.upd(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Delete(ProductImage p)
        {
            CResult result = new CResult();
            try
            {
                iloProductImage ilo = new logProductImage();
                result = ilo.del(p);
            }
            catch (Exception ex)
            {
                result = new CResult { Result = -11, Error = ex.Message.ToString() };
            }
            return JsonConvert.SerializeObject(result);
        }

        [System.Web.Services.WebMethod]
        public static string Listar(ProductImage p)
        {
            List<ProductImage> oLista = new List<ProductImage>();
            try
            {
                iloProductImage ilo = new logProductImage();
                oLista = ilo.lis(p).ToList();
            }
            catch (Exception)
            {
                oLista = new List<ProductImage>();
            }
            return JsonConvert.SerializeObject(oLista);
        }

        [System.Web.Services.WebMethod]
        public static string Get(ProductImage p)
        {
            ProductImage oObj = new ProductImage();
            try
            {
                iloProductImage ilo = new logProductImage();
                oObj = ilo.sel(p);
            }
            catch (Exception)
            {
                oObj = new ProductImage();
            }
            return JsonConvert.SerializeObject(oObj);
        }
        #endregion

    }
}