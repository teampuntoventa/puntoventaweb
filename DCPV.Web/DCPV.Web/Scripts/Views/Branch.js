﻿var jsEntidadBranch = new Object();
var response = null;
var sendInfo = null;
var WebMethodpath = new function () { return "" };
var TableName = "Branch";

var jsBranch = new function () {


    var path = "../Views/MantBranch.aspx";

    var ButtonDelete = "<button class='btn-outline btn-danger btn btn-xs' onclick='javascript: jsBranch.Delete([jsparameter]);' >Delete</button>";
    var ButtonEdit = "<button class='btn-outline btn-info btn btn-xs' onclick='javascript: jsBranch.irEdit([jsparameter]);' >Editar</button>";
    var ButtonView = "<button class='btn-outline btn-success btn btn-xs' onclick='javascript: jsBranch.irView([jsparameter]);' >Ver</button>";
    var ButtonSeleccionar = "<button id ='btnSeleccionar' class='btn-outline btn-info     btn btn-xs'     onclick='javascript: jsBranch.Seleccionar_Branch([jsparameter]);'      type='button' >Seleccionar</button>";

    //#region Atributos 

    this.objFillMantWebBranch = function () {
        try {
            var LstObj = [
                {
                    Id: 'BrnId', Descripcion: 'BrnId', Input: '#HdBsnId', Type: 'hidden',
                    Source: null,
                    Valid: {},
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'BsnId', Descripcion: 'BsnId', Input: '#HdBsnId', Type: 'hidden',
                    Source: null,
                    Valid: {},
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'BrnDescription', Descripcion: 'Descripcion', Input: '#txtBsnRUC', Type: 'Text',
                    Source: null,
                    Valid: { required: true, minlength: 11 },
                    Table: { Visible: true, width: 100, func: null }
                },
                {
                    Id: 'BrnType', Descripcion: 'BrnType', Input: '#txtBsnReason', Type: 'Text',
                    Source: null,
                    Valid: { required: true, minlength: 4 },
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'BrnRefId', Descripcion: 'BrnRefId', Input: '#txtBsnAddress', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'BrnVen', Descripcion: 'Venta', Input: '#txtBsnDescription', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: true, width: 10, func: function (data, type, full) { return '<div class="text-center">' + (data.BrnVen.toString() == '1' ? '<i class="fa fa-check "></i>' : '<i class="fa fa-times"></i>') + '</td>' } }
                },
                {
                    Id: 'BrnCom', Descripcion: 'Compras', Input: '#txtBsnNameContact', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: true, width: 10, func: function (data, type, full) { return '<div class="text-center">' + (data.BrnCom.toString() == '1' ? '<i class="fa fa-check "></i>' : '<i class="fa fa-times"></i>') + '</td>' } }
                },
                {
                    Id: 'BrnIng', Descripcion: 'Ingreso', Input: '#txtBsnEmail', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: true, width: 10, func: function (data, type, full) { return '<div class="text-center">' + (data.BrnIng.toString() == '1' ? '<i class="fa fa-check "></i>' : '<i class="fa fa-times"></i>') + '</td>' } }
                },
                {
                    Id: 'BrnSal', Descripcion: 'Salidas', Input: '#txtBsnTel', Type: 'Text',
                    Source: null,
                    Valid: { required: true, minlength: 7 },
                    Table: { Visible: true, width: 10, func: function (data, type, full) { return '<div class="text-center">' + (data.BrnSal.toString() == '1' ? '<i class="fa fa-check "></i>' : '<i class="fa fa-times"></i>') + '</td>' } }
                },
                {
                    Id: 'BrnTrns', Descripcion: 'Transferencias', Input: '#txtBsnInfAdd', Type: 'Text',
                    Source: null,
                    Valid: {},
                    Table: { Visible: true, width: 10, func: function (data, type, full) { return '<div class="text-center">' + (data.BrnTrns.toString() == '1' ? '<i class="fa fa-check "></i>' : '<i class="fa fa-times"></i>') + '</td>' } }
                },
                {
                    Id: '', Descripcion: 'Buttons', Input: '', Type: '',
                    Source: null,
                    Valid: {},
                    Table: {
                        Visible: true, width: 25, func: function (data, type, full) {
                            var _ButtonView = ButtonView.replace('[jsparameter]', data.BsnId);
                            var _ButtonEdit = ButtonEdit.replace('[jsparameter]', data.BsnId);
                            var _ButtonDelete = ButtonDelete.replace('[jsparameter]', data.BsnId);
                            var concat = '<td><div class="text-right">' + _ButtonView + _ButtonEdit + _ButtonDelete + '</td></div>';
                            return concat;
                        }
                    }
                }
            ];
            /*
             {
                    Id: 'BsnFecha', Input: '#txtBsnFecha', Type: 'Date',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: false, Descripcion: 'Fecha', func: null }
                },
                {
                    Id: 'TypePerson', Input: '#cboTypePerson', Type: 'select',
                    Source: { data: TypePerson, dataValue: 'dataValue', dataText: 'dataText', etiqueta: null, index: null },
                    Valid: {},
                    Table: { Visible: false, Descripcion: 'Fecha', func: null }
                },
            */
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
        return LstObj;
    }

    this.CreateAtributes = function () {
        try {
            var WebMethodpath = path.concat("/Atributos");
            var sendInfoComp = {};
            var data = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfoComp).d);
            var Obj = data;
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
        return Obj;
    }


    //#endregion

    //#region Formularios

    this.IniForm = function () {
        try {
            var pType = $.urlParam("Type");
            var pId = $.urlParam("id");
            switch (pType) {
                case 'N': jsBranch.IniFormRegistro(); break;
                case 'V': jsBranch.IniFormView(pId); break;
                case 'E': jsBranch.IniFormEdit(pId); break;
            }
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormListar = function () {
        try {
            $('#tbBranch').iniDataTableObject(jsBranch.objFillMantWebBranch(), "#ibox-content");
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.ClearFormRegistro = function () {
        try {
            ClearHtmlObject(jsBranch.objFillMantWebBranch());
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormRegistro = function () {
        try {
            jsBranch.ClearFormRegistro();
            IniHtmlObject(jsBranch.objFillMantWebBranch());
            jsBranch.Validacion();
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormView = function (id) {
        try {
            jsBranch.ClearFormRegistro();
            IniHtmlObject(jsBranch.objFillMantWebBranch(), 'V');
            FillHtmlObject(jsBranch.objFillMantWebBranch(), jsBranch.Get(id));
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormEdit = function (id) {
        try {
            jsBranch.ClearFormRegistro();
            IniHtmlObject(jsBranch.objFillMantWebBranch());
            FillHtmlObject(jsBranch.objFillMantWebBranch(), jsBranch.Get(id));
            jsBranch.Validacion();
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.Validacion = function () {
        try {
            $('#form-Branch').iniValidateObjectForm(jsBranch.objFillMantWebBranch(), jsBranch.Transaccion)
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };


    //#endregion

    //#region Funciones Varias

    this.getList = function () {
        WebMethodpath = path.concat("/Listar");
        var sendInfo = { p: jsBranch.CreateAtributes() };
        var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
        return response;
    };

    //#endregion

    //#region CRUD

    this.Get = function (Id) {
        try {
            jsEntidadBranch = jsBranch.CreateAtributes();
            jsEntidadBranch.BsnId = Id;
            var WebMethodpath = path.concat("/Get");
            var sendInfoComp = { p: jsEntidadBranch };
            var data = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfoComp).d);
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
        return data;
    }

    this.Transaccion = function () {
        try {
            jsEntBranch = FillObject(jsBranch.objFillMantWebBranch(), jsBranch.CreateAtributes());

            response = null;
            sendInfo = null;

            AlertSwalConfirm("Desea Registrar", 0, function () {
                WebMethodpath = path.concat("/Transaccion");
                sendInfo = { p: jsEntBranch };
                response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
                if (response.Result == 0) {
                    AlertSwalOptionConfirm("Se registro correctamente la información. \n Desea Registrar uno nuevo", 0, function (result) {
                        if (result)
                            jsBranch.irAgregar();
                        else
                            jsBranch.irList();
                    });
                }
                else
                    AlertSwal(response.Error, 2);
            });

        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.Listar = function () {
        try {
            $('#tbBranch').iniDataTableObject(jsBranch.objFillMantWebBranch(), "#ibox-content", true, jsBranch.getList());
            toastr.success('Registros Cargados', Basetitle);

        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.Delete = function (id) {
        try {
            jsEntidadBranch = jsBranch.CreateAtributes();
            jsEntidadBranch.BsnId = id;

            response = null;
            sendInfo = null;

            WebMethodpath = path.concat("/Delete");
            sendInfo = { p: jsEntidadBranch };

            AlertSwalConfirm("Desea Elimar", 0, function () {
                MAIN.ajaxPostDataJasonToServerPOST_Returm(WebMethodpath, sendInfo, function (res) {
                    $.unblockUI();
                    var response = JSON.parse(res.d);
                    if (response.Result == 0) {
                        AlertSwal("Eliminado", Basetitle);
                        jsBranch.Listar();
                    }
                    else
                        AlertSwal(response.Error, 2);
                });

            });
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    //#endregion

    //#region irCRUD 

    this.irAgregar = function () {
        try {
            window.location.href = "../Views/MantWebRegisterBranch.aspx?Type=N";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.irEdit = function (id) {
        try {
            window.location.href = "../Views/MantWebRegisterBranch.aspx?id=" + id + "&Type=E";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.irView = function (id) {
        try {
            window.location.href = "../Views/MantWebRegisterBranch.aspx?id=" + id + "&Type=V";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.irList = function () {
        try {
            window.location.href = "../Views/MantBranch.aspx";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    //#endregion


}
