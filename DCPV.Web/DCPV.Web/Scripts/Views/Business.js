﻿var jsEntidadBusiness = new Object();
var response = null;
var sendInfo = null;
var WebMethodpath = new function () { return "" };
var TableName = "Business";

var jsBusiness = new function () {


    var path = "../Views/MantBusiness.aspx";

    var ButtonDelete = "<button class='btn-outline btn-danger btn btn-xs' onclick='javascript: jsBusiness.Delete([jsparameter]);' >Delete</button>";
    var ButtonEdit = "<button class='btn-outline btn-info btn btn-xs' onclick='javascript: jsBusiness.irEdit([jsparameter]);' >Editar</button>";
    var ButtonView = "<button class='btn-outline btn-success btn btn-xs' onclick='javascript: jsBusiness.irView([jsparameter]);' >Ver</button>";
    var ButtonSeleccionar = "<button id ='btnSeleccionar' class='btn-outline btn-info     btn btn-xs'     onclick='javascript: jsBusiness.Seleccionar_Business([jsparameter]);'      type='button' >Seleccionar</button>";

    //#region Atributos 

    this.objFillMantWebBusiness = function () {
        try {
            var LstObj = [
                {
                    Id: 'BsnId', Descripcion: 'BsnId', Input: '#HdBsnId', Type: 'hidden',
                    Source: null,
                    Valid: {},
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'BsnRUC', Descripcion: 'RUC', Input: '#txtBsnRUC', Type: 'Text',
                    Source: null,
                    Valid: { required: true, minlength: 11 },
                    Table: { Visible: true, width: 20, func: null }
                },
                {
                    Id: 'BsnReason', Descripcion: 'Razón Social', Input: '#txtBsnReason', Type: 'Text',
                    Source: null,
                    Valid: { required: true, minlength: 4 },
                    Table: { Visible: true, width: 100, func: function (data, type, full) { return data.BsnReason.toString().toUpperCase() } }
                },
                {
                    Id: 'BsnAddress', Descripcion: 'Dirección', Input: '#txtBsnAddress', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: true, width: 200, func: null }
                },
                {
                    Id: 'BsnDescription', Descripcion: 'Descripción', Input: '#txtBsnDescription', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: true, width: 200, func: null }
                },
                {
                    Id: 'BsnLogo', Descripcion: 'Logo', Input: '#txtBsnLogo', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'BsnNameContact', Descripcion: 'Contacto', Input: '#txtBsnNameContact', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'BsnEmail', Descripcion: 'Email', Input: '#txtBsnEmail', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: true, width: null, func: null }
                },
                {
                    Id: 'BsnTel', Descripcion: 'Teléfono', Input: '#txtBsnTel', Type: 'Text',
                    Source: null,
                    Valid: { required: true, minlength: 7 },
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'BsnInfAdd', Descripcion: 'BsnInfAdd', Input: '#txtBsnInfAdd', Type: 'Text',
                    Source: null,
                    Valid: {},
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: '', Descripcion: '', Input: '#cboApertura', Type: 'select',
                    Source: { data: OptionsTypeConfirm, dataValue: 'dataValue', dataText: 'dataText', index: true },
                    Valid: {},
                    Table: {}
                },
                {
                    Id: '', Descripcion: 'Buttons', Input: '', Type: '',
                    Source: null,
                    Valid: {},
                    Table: {
                        Visible: true, width: 100, func: function (data, type, full) {
                            var _ButtonView = ButtonView.replace('[jsparameter]', data.BsnId);
                            var _ButtonEdit = ButtonEdit.replace('[jsparameter]', data.BsnId);
                            var _ButtonDelete = ButtonDelete.replace('[jsparameter]', data.BsnId);
                            var concat = '<td><div class="text-right">' + _ButtonView + _ButtonEdit + _ButtonDelete + '</td></div>';
                            return concat;
                        }
                    }
                }
            ];
            /*
             {
                    Id: 'BsnFecha', Input: '#txtBsnFecha', Type: 'Date',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: false, Descripcion: 'Fecha', func: null }
                },
                {
                    Id: 'TypePerson', Input: '#cboTypePerson', Type: 'select',
                    Source: { data: TypePerson, dataValue: 'dataValue', dataText: 'dataText', etiqueta: null, index: null },
                    Valid: {},
                    Table: { Visible: false, Descripcion: 'Fecha', func: null }
                },
            */
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
        return LstObj;
    }

    this.CreateAtributes = function () {
        try {
            var WebMethodpath = path.concat("/Atributos");
            var sendInfoComp = {};
            var data = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfoComp).d);
            var Obj = data;
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
        return Obj;
    }


    //#endregion

    //#region Formularios

    this.IniForm = function () {
        try {
            var pType = $.urlParam("Type");
            var pId = $.urlParam("id");
            switch (pType) {
                case 'N': jsBusiness.IniFormRegistro(); break;
                case 'V': jsBusiness.IniFormView(pId); break;
                case 'E': jsBusiness.IniFormEdit(pId); break;
            }
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormListar = function () {
        try {
            $('#tbBusiness').iniDataTableObject(jsBusiness.objFillMantWebBusiness(), "#ibox-content");
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.ClearFormRegistro = function () {
        try {
            ClearHtmlObject(jsBusiness.objFillMantWebBusiness());
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormRegistro = function () {
        try {
            jsBusiness.ClearFormRegistro();
            IniHtmlObject(jsBusiness.objFillMantWebBusiness());
            jsBusiness.Validacion();
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormView = function (id) {
        try {
            jsBusiness.ClearFormRegistro();
            IniHtmlObject(jsBusiness.objFillMantWebBusiness(), 'V');
            FillHtmlObject(jsBusiness.objFillMantWebBusiness(), jsBusiness.Get(id));
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormEdit = function (id) {
        try {
            jsBusiness.ClearFormRegistro();
            IniHtmlObject(jsBusiness.objFillMantWebBusiness());
            FillHtmlObject(jsBusiness.objFillMantWebBusiness(), jsBusiness.Get(id));
            jsBusiness.Validacion();
            $('#divcboApertura').addClass('hide');
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.Validacion = function () {
        try {
            $('#form-Business').iniValidateObjectForm(jsBusiness.objFillMantWebBusiness(), jsBusiness.Transaccion)
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };


    //#endregion

    //#region Funciones Varias

    this.getList = function () {
        WebMethodpath = path.concat("/Listar");
        var sendInfo = { p: jsBusiness.CreateAtributes() };
        var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
        return response;
    };

    this.ChangeApertura = function (obj) {
        try {
            var IdSelect = $(obj).val() == null ? "" : $(obj).val();
            var IdText = $(obj).find("option:selected").text();
            if (IdSelect == 'Y') {
                $('#divRegEjercicios').removeClass('hide');
                jsExercise.IniFormRegistro();
            }
            else
                $('#divRegEjercicios').addClass('hide');
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    //#endregion

    //#region CRUD

    this.Get = function (Id) {
        try {
            jsEntidadBusiness = jsBusiness.CreateAtributes();
            jsEntidadBusiness.BsnId = Id;
            var WebMethodpath = path.concat("/Get");
            var sendInfoComp = { p: jsEntidadBusiness };
            var data = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfoComp).d);
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
        return data;
    }

    this.Transaccion = function () {
        try {
            jsEntBusiness = FillObject(jsBusiness.objFillMantWebBusiness(), jsBusiness.CreateAtributes());

            response = null;
            sendInfo = null;

            AlertSwalConfirm("Desea Registrar", 0, function () {
                WebMethodpath = path.concat("/Transaccion");
                sendInfo = { p: jsEntBusiness };
                response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
                if (response.Result == 0) {

                    if ($('#cboApertura').val() == 'Y') {
                        AlertSwal('Se registro correctamente la Empresa',0);
                        var ResExercise = jsExercise.Transaccion();
                        if (ResExercise.Result == 0) {
                            AlertSwalOptionConfirm("Se aperturo correctamente el Ejercicio(año). \n Desea Registrar una nueva Empresa", 0, function (result) {
                                if (result)
                                    jsBusiness.irAgregar();
                                else
                                    jsBusiness.irList();
                            });
                        }
                     } else {
                        AlertSwalOptionConfirm("Se registro correctamente la información. \n Desea Registrar uno nuevo", 0, function (result) {
                            if (result)
                                jsBusiness.irAgregar();
                            else
                                jsBusiness.irList();
                        });
                    }
                }
                else
                    AlertSwal(response.Error, 2);
            });

        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.Listar = function () {
        try {
            $('#tbBusiness').iniDataTableObject(jsBusiness.objFillMantWebBusiness(), "#ibox-content", true, jsBusiness.getList());
            toastr.success('Registros Cargados', Basetitle);

        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.Delete = function (id) {
        try {
            jsEntidadBusiness = jsBusiness.CreateAtributes();
            jsEntidadBusiness.BsnId = id;

            response = null;
            sendInfo = null;

            WebMethodpath = path.concat("/Delete");
            sendInfo = { p: jsEntidadBusiness };

            AlertSwalConfirm("Desea Elimar", 0, function () {
                MAIN.ajaxPostDataJasonToServerPOST_Returm(WebMethodpath, sendInfo, function (res) {
                    $.unblockUI();
                    var response = JSON.parse(res.d);
                    if (response.Result == 0) {
                        AlertSwal("Eliminado", Basetitle);
                        jsBusiness.Listar();
                    }
                    else
                        AlertSwal(response.Error, 2);
                });

            });
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    //#endregion

    //#region irCRUD 

    this.irAgregar = function () {
        try {
            window.location.href = "../Views/MantWebRegisterBusiness.aspx?Type=N";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.irEdit = function (id) {
        try {
            window.location.href = "../Views/MantWebRegisterBusiness.aspx?id=" + id + "&Type=E";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.irView = function (id) {
        try {
            window.location.href = "../Views/MantWebRegisterBusiness.aspx?id=" + id + "&Type=V";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.irList = function () {
        try {
            window.location.href = "../Views/MantBusiness.aspx";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    //#endregion

    //#region Modal

    this.ModalAgregar = function () {
        try {
            var modal = jsBusiness.CreateControles("Mantenimiento Business (NUEVO)");
            if (modal == true) { } else
                toastr.error("Error al Abrir el Popup", Basetitle);
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.ModalEdit = function (id) {
        var modal = jsBusiness.CreateControles("Mantenimiento Business (EDITAR)");
        try {
            if (modal == true) {
                $('#ModalBusiness').modal('show');
                response = null;
                sendInfo = null;
                jsEntBusiness = jsBusiness.CreateAtributes();
                jsEntBusiness.id = parseInt(id);
                WebMethodpath = path.concat("/Get");
                sendInfo = { p: jsEntBusiness };
                response = JSON.parse(MAIN.ajaxPostDataJasonToServer(WebMethodpath, sendInfo).d);
                if (response != null) {
                    $("#id").val(response[0].id);
                    $("#Codigo").val(response[0].Codigo);
                    $("#Descripcion").val(response[0].Descripcion);
                    $('#Direccion').val(response[0].Direccion);
                    $('#Distrito').val(response[0].Distrito);
                    $('#Ciudad').val(response[0].Ciudad);
                } else
                    toastr.error("Error al Buscar Registro", Basetitle);
            } else
                toastr.error("Error al crear modal", Basetitle);

        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.createControlesModal = function (title, modal) {
        try {

            if (modal.search("myModal-Direccion") == 0) {
                $("#tile-myModal-Direccion").html(title);
                $('#cboTipoDireccion_myModal_Direccion').ClearSelected();
                $('#cboPais_myModal_Direccion').ClearSelected();
                $('#cboDepartamento_myModal_Direccion').ClearSelected();
                $('#cboProvincia_myModal_Direccion').ClearSelected();
                $('#cboDistrito_myModal_Direccion').ClearSelected();
                $('#txtDireccion_myModal_Direccion').val("");
                //$('#txtCalle_myModal_Direccion').val("");
                getSelectPaices($("#cboPais_myModal_Direccion"));
                $("#cboTipoDireccion_myModal_Direccion").CreateSelected(OptionsTypeDireccion, "dataValue", "dataText", undefined, true);
                $('#txtCalle_myModal_Direccion').ClearSelected();
                $("#txtCalle_myModal_Direccion").CreateSelected(OptionsTypeVia, "dataValue", "dataText", undefined, true);
                /*
                $('#cboTipovivienda_myModal_Direccion').ClearSelected();
                $("#cboTipovivienda_myModal_Direccion").CreateSelected(OptionsTypeViVienda, "dataValue", "dataText", undefined, true);
                */
            }
            else if (modal.search("myModal-Contacto") == 0) {
                $("#tile-myModal-Contacto").html(title);

                $('#txtIdPersona_myModal_Contacto').val("");
                $('#txtNombre_myModal_Contacto').val("");
                $('#txtApellidoPaterno_myModal_Contacto').val("");
                $('#txtTelefono1_myModal_Contacto').val("");
                $('#txtTelefonoCelular_myModal_Contacto').val("");
                $('#txtEmail_myModal_Contacto').val("");
                $('#txtPuesto_myModal_Contacto').val("");
                $('#cboTipoContato_myModal_Contacto').ClearSelected();

                $("#cboTipoContato_myModal_Contacto").CreateSelected(OptionsTypeContact, "dataValue", "dataText", undefined, true);
            }
            else if (modal.search("myModal-Medico") == 0) {
                $("#tile-myModal-Medico").html(title);
                $('#txtCodigo_Medico_myModal_Medico').val("");
                $('#txtNombre_Medico_myModal_Medico').val("");
            }
            else if (modal.search("myModal-Business") == 0) {

            }

            $("#modal-form .error").each(function () {
                $(this).remove();
            });
            return true;
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
            return false
        }
    };

    //#endregion


}