﻿var jsEntidadExercise = new Object();
var response = null;
var sendInfo = null;
var WebMethodpath = new function () { return "" };
var TableName = "Exercise";
var obsss = null;

var jsExercise = new function () {


    var path = "../Views/MantExercise.aspx";

    var ButtonDelete = "<button class='btn-outline btn-danger btn btn-xs' onclick='javascript: jsExercise.Delete([jsparameter]);' >Delete</button>";
    var ButtonEdit = "<button class='btn-outline btn-info btn btn-xs' onclick='javascript: jsExercise.irEdit([jsparameter]);' >Editar</button>";
    var ButtonView = "<button class='btn-outline btn-success btn btn-xs' onclick='javascript: jsExercise.irView([jsparameter]);' >Ver</button>";
    var ButtonSeleccionar = "<button id ='btnSeleccionar' class='btn-outline btn-info     btn btn-xs'     onclick='javascript: jsExercise.Seleccionar_Exercise([jsparameter]);'      type='button' >Seleccionar</button>";

    //#region Atributos 

    this.objFillMantWebExercise = function () {
        try {
            var LstObj = [
                {
                    Id: 'ExrId', Descripcion: 'ExrId', Input: '#HdExrId', Type: 'hidden',
                    Source: null,
                    Valid: {},
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'BsnId', Descripcion: 'BsnId', Input: '#HdBsnId', Type: 'hidden',
                    Source: null,
                    Valid: {},
                    Table: { Visible: false, width: null, func: null }
                },
                {
                    Id: 'ExrDescription', Descripcion: 'Descriptión', Input: '#txtExrDescription', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: true, func: null }
                },
                {
                    Id: 'ExrYear', Descripcion: 'Año', Input: '#txtExrYear', Type: 'year',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: true, func: null }
                },
                {
                    Id: 'ExrClosed', Descripcion: 'Cerrado?', Input: '#txtExrClosed', Type: 'Text',
                    Source: null,
                    Valid: { required: true },
                    Table: { Visible: true, func: null }
                },
                {
                    Id: '', Descripcion: 'Buttons', Input: '', Type: '',
                    Source: null,
                    Valid: {},
                    Table: {
                        Visible: true, width: 100, func: function (data, type, full) {
                            var _ButtonView = ButtonView.replace('[jsparameter]', data.ExrId);
                            var _ButtonEdit = ButtonEdit.replace('[jsparameter]', data.ExrId);
                            var _ButtonDelete = ButtonDelete.replace('[jsparameter]', data.ExrId);
                            var concat = '<td><div class="text-right">' + _ButtonView + _ButtonEdit + _ButtonDelete + '</td></div>';
                            return concat;
                        }
                    }
                }
            ];
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
        return LstObj;
    }

    this.CreateAtributes = function () {
        try {
            var WebMethodpath = path.concat("/Atributos");
            var sendInfoComp = {};
            var data = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfoComp).d);
            var Obj = data;
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
        return Obj;
    }


    //#endregion

    //#region Formularios

    this.IniForm = function () {
        try {
            var pType = $.urlParam("Type");
            var pId = $.urlParam("id");
            switch (pType) {
                case 'N': jsExercise.IniFormRegistro(); break;
                case 'V': jsExercise.IniFormView(pId); break;
                case 'E': jsExercise.IniFormEdit(pId); break;
            }
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormListar = function () {
        try {
            $('#tbExercise').iniDataTableObject(jsExercise.objFillMantWebExercise(), "#ibox-content");
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.ClearFormRegistro = function () {
        try {
            ClearHtmlObject(jsExercise.objFillMantWebExercise());
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormRegistro = function () {
        try {
            jsExercise.ClearFormRegistro();
            IniHtmlObject(jsExercise.objFillMantWebExercise());
            jsExercise.Validacion();
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormView = function (id) {
        try {
            jsExercise.ClearFormRegistro();
            IniHtmlObject(jsExercise.objFillMantWebExercise(), 'V');
            FillHtmlObject(jsExercise.objFillMantWebExercise(), jsExercise.Get(id));
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.IniFormEdit = function (id) {
        try {
            jsExercise.ClearFormRegistro();
            IniHtmlObject(jsExercise.objFillMantWebExercise());
            FillHtmlObject(jsExercise.objFillMantWebExercise(), jsExercise.Get(id));
            jsExercise.Validacion();
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.Validacion = function () {
        try {
            $('#form-Exercise').iniValidateObjectForm(jsExercise.objFillMantWebExercise(), jsExercise.Transaccion)
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };


    //#endregion

    //#region Funciones Varias

    this.getList = function () {
        WebMethodpath = path.concat("/Listar");
        var sendInfo = { p: jsExercise.CreateAtributes() };
        var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
        return response;
    };

    //#endregion

    //#region CRUD

    this.Get = function (Id) {
        try {
            jsEntidadExercise = jsExercise.CreateAtributes();
            jsEntidadExercise.BsnId = Id;
            var WebMethodpath = path.concat("/Get");
            var sendInfoComp = { p: jsEntidadExercise };
            var data = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfoComp).d);
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
        return data;
    }

    this.Transaccion = function () {
        try {
            jsEntExercise = FillObject(jsExercise.objFillMantWebExercise(), jsExercise.CreateAtributes());

            response = null;
            sendInfo = null;

            AlertSwalConfirm("Desea Registrar", 0, function () {
                WebMethodpath = path.concat("/Transaccion");
                sendInfo = { p: jsEntExercise };
                response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
                if (response.Result == 0) {
                    AlertSwalOptionConfirm("Se registro correctamente la información. \n Desea Registrar uno nuevo", 0, function (result) {
                        if (result)
                            jsExercise.irAgregar();
                        else
                            jsExercise.irList();
                    });
                }
                else
                    AlertSwal(response.Error, 2);
            });
            return response;
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.Listar = function () {
        try {
            $('#tbExercise').iniDataTableObject(jsExercise.objFillMantWebExercise(), "#ibox-content", true, jsExercise.getList());
            toastr.success('Registros Cargados', Basetitle);

        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.Delete = function (id) {
        try {
            jsEntidadExercise = jsExercise.CreateAtributes();
            jsEntidadExercise.BsnId = id;

            response = null;
            sendInfo = null;

            WebMethodpath = path.concat("/Delete");
            sendInfo = { p: jsEntidadExercise };

            AlertSwalConfirm("Desea Elimar", 0, function () {
                MAIN.ajaxPostDataJasonToServerPOST_Returm(WebMethodpath, sendInfo, function (res) {
                    $.unblockUI();
                    var response = JSON.parse(res.d);
                    if (response.Result == 0) {
                        AlertSwal("Eliminado", Basetitle);
                        jsExercise.Listar();
                    }
                    else
                        AlertSwal(response.Error, 2);
                });

            });
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    //#endregion

    //#region irCRUD 

    this.irAgregar = function () {
        try {
            window.location.href = "../Views/MantWebRegisterExercise.aspx?Type=N";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.irEdit = function (id) {
        try {
            window.location.href = "../Views/MantWebRegisterExercise.aspx?id=" + id + "&Type=E";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.irView = function (id) {
        try {
            window.location.href = "../Views/MantWebRegisterExercise.aspx?id=" + id + "&Type=V";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.irList = function () {
        try {
            window.location.href = "../Views/MantExercise.aspx";
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    //#endregion

    //#region Modal

    this.ModalAgregar = function () {
        try {
            var modal = jsExercise.CreateControles("Mantenimiento Exercise (NUEVO)");
            if (modal == true) { } else
                toastr.error("Error al Abrir el Popup", Basetitle);
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.ModalEdit = function (id) {
        var modal = jsExercise.CreateControles("Mantenimiento Exercise (EDITAR)");
        try {
            if (modal == true) {
                $('#ModalExercise').modal('show');
                response = null;
                sendInfo = null;
                jsEntExercise = jsExercise.CreateAtributes();
                jsEntExercise.id = parseInt(id);
                WebMethodpath = path.concat("/Get");
                sendInfo = { p: jsEntExercise };
                response = JSON.parse(MAIN.ajaxPostDataJasonToServer(WebMethodpath, sendInfo).d);
                if (response != null) {
                    $("#id").val(response[0].id);
                    $("#Codigo").val(response[0].Codigo);
                    $("#Descripcion").val(response[0].Descripcion);
                    $('#Direccion').val(response[0].Direccion);
                    $('#Distrito').val(response[0].Distrito);
                    $('#Ciudad').val(response[0].Ciudad);
                } else
                    toastr.error("Error al Buscar Registro", Basetitle);
            } else
                toastr.error("Error al crear modal", Basetitle);

        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

    this.createControlesModal = function (title, modal) {
        try {

            if (modal.search("myModal-Direccion") == 0) {
                $("#tile-myModal-Direccion").html(title);
                $('#cboTipoDireccion_myModal_Direccion').ClearSelected();
                $('#cboPais_myModal_Direccion').ClearSelected();
                $('#cboDepartamento_myModal_Direccion').ClearSelected();
                $('#cboProvincia_myModal_Direccion').ClearSelected();
                $('#cboDistrito_myModal_Direccion').ClearSelected();
                $('#txtDireccion_myModal_Direccion').val("");
                //$('#txtCalle_myModal_Direccion').val("");
                getSelectPaices($("#cboPais_myModal_Direccion"));
                $("#cboTipoDireccion_myModal_Direccion").CreateSelected(OptionsTypeDireccion, "dataValue", "dataText", undefined, true);
                $('#txtCalle_myModal_Direccion').ClearSelected();
                $("#txtCalle_myModal_Direccion").CreateSelected(OptionsTypeVia, "dataValue", "dataText", undefined, true);
                /*
                $('#cboTipovivienda_myModal_Direccion').ClearSelected();
                $("#cboTipovivienda_myModal_Direccion").CreateSelected(OptionsTypeViVienda, "dataValue", "dataText", undefined, true);
                */
            }
            else if (modal.search("myModal-Contacto") == 0) {
                $("#tile-myModal-Contacto").html(title);

                $('#txtIdPersona_myModal_Contacto').val("");
                $('#txtNombre_myModal_Contacto').val("");
                $('#txtApellidoPaterno_myModal_Contacto').val("");
                $('#txtTelefono1_myModal_Contacto').val("");
                $('#txtTelefonoCelular_myModal_Contacto').val("");
                $('#txtEmail_myModal_Contacto').val("");
                $('#txtPuesto_myModal_Contacto').val("");
                $('#cboTipoContato_myModal_Contacto').ClearSelected();

                $("#cboTipoContato_myModal_Contacto").CreateSelected(OptionsTypeContact, "dataValue", "dataText", undefined, true);
            }
            else if (modal.search("myModal-Medico") == 0) {
                $("#tile-myModal-Medico").html(title);
                $('#txtCodigo_Medico_myModal_Medico').val("");
                $('#txtNombre_Medico_myModal_Medico').val("");
            }
            else if (modal.search("myModal-Exercise") == 0) {

            }

            $("#modal-form .error").each(function () {
                $(this).remove();
            });
            return true;
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
            return false
        }
    };

    //#endregion


}