﻿'use strict';

var MAIN = new function () {

    var action = "";                                // marca la acción actual en un proceso
    var request = null;                             // objeto que indicará uso ajax
    var tempdata;

    this.getVirtualDirectory=function () {
        var vDir = document.location.pathname.split('/');
        return '/' + vDir[1] + '/';
    }

    this.loadCSS = function (url) {
        var head = document.getElementsByTagName('head')[0];
        head.appendChild('<link rel="stylesheet" type="text/css" href="' + url + '">');
    }

    this.loadSCR = function (url) {
        var body = document.getElementsByTagName('body')[0];
        body.appendChild('<script type="text/javascript" src="' + url + '">');     
    } 
    /*
       dataToSend          Contains the JSON Object.
       submitType          1 == Normal Submit; 2 == Submit and Print.
       strMessagetoShow    Text that is displayed in the Please Wait Window.
    */

    this.ajaxPostDataJasonToServerPOST_Line     = function (strMethodToCall, dataToSend) {
        tempdata                                = null;

        if (request) request.abort();
        request =
            $.ajax
            ({
                url                             : strMethodToCall,
                type                            : 'POST',
                async                           : false, 
                dataType                        : 'json',
                data                            : JSON.stringify(dataToSend),
                contentType                     : 'application/json; charSet=UTF-8',
                success                         : function (data, textStatus, jqXHR) {
                    tempdata                    = data;
                },
                error                           : function (jqXHR, textStatus, errorThrown) {
                    tempdata                    = null;
                    /*
                     var error                   = strMethodToCall.split('/');
                     toastr.error("Error en el Metodo " + error[3], Basetitle);
                     console.log("AjaxError: Something really bad happened " + textStatus);
                     */
                    var error = $.parseJSON(jqXHR.responseText).Message;
                    toastr.error("Se genero el siguiente error: '" + error + "'", Basetitle);
                    console.log("AjaxError: Something really bad happened " + error);
                   
                },
                beforeSend                      : function (jqXHR, settings)
                {
                   
                },
                complete                        : function (jqXHR, textStatus)
                {
                  
                }
            }).done(function (data)
            {
                console.log("done : " + data.toString());
            }).fail(function (data)
            {
                console.log("fail : " + data.toString());
            });
        return tempdata;
    }
    this.ajaxPostDataJasonToServerPOST_Returm   = function (strMethodToCall, dataToSend, funtionResult) {
        tempdata = null;

        if (request) request.abort();

        $.ajax
        ({
            url                             : strMethodToCall,
            type                            : 'POST',
            async                           : true,
            dataType                        : 'json',
            data                            : JSON.stringify(dataToSend),
            contentType                     : 'application/json; charSet=UTF-8',
            beforeSend                      : function ()
            {
                   
                $.blockUI({
                    message: 'Cargando....',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
                   
            },
            complete                        : function ()
            {
            },
            success                         : function (data, textStatus, jqXHR)
            {
                tempdata                    = data;
                funtionResult(tempdata);
            },
            error                           : function (jqXHR, textStatus, errorThrown) {
                tempdata = null;
                try {
                    var errEstado           = jqXHR.status;
                    var errMensaje          = jqXHR.responseText == "" ? "" : jQuery.parseJSON(jqXHR.responseText).Message;
                    var errTipo             = jqXHR.responseText == "" ? "" : jQuery.parseJSON(jqXHR.responseText).ExceptionType;
                } catch (e) {
                    var x                   = e.message.toString();
                }
                $.unblockUI();
            },
            beforeSend                      : function (jqXHR, settings)
            {
                   
                $.blockUI({
                    message: 'Cargando....',
                    css: {
                        border: 'none',
                        padding: '15px',
                        backgroundColor: '#000',
                        '-webkit-border-radius': '10px',
                        '-moz-border-radius': '10px',
                        opacity: .5,
                        color: '#fff'
                    }
                });
                  
            },
            complete                        : function (jqXHR, textStatus) {}
        }).done(function (data)
        {
               
        }).fail(function (xhr, err)
        {
            $.unblockUI();
        });
    }
    
    this.ajaxPostDataJasonToServerGET_Line      = function (strMethodToCall, dataToSend) {
        debugger;
        tempdata    = null;

        if (request) request.abort();

        request =
            $.ajax
            ({
                url                             : strMethodToCall,
                type                            : 'GET',
                async                           : false,
                dataType                        : 'json',
                data                            : { parameter: JSON.stringify(dataToSend) },
                contentType                     : 'application/json; charSet=UTF-8',
                beforeSend                      : function ()
                {
                    /*
                    $.blockUI({
                        message: 'Cargando....',
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                    */
                },
                complete                        : function ()
                {
                    //  $.unblockUI();
                },
                success                         : function (data, textStatus, jqXHR)
                {
                    tempdata = data;
                },
                error                           : function (jqXHR, textStatus, errorThrown)
                {
                    tempdata = null;
                    try {
                        var errEstado           = jqXHR.status;
                        var errMensaje          = jqXHR.responseText == "" ? "" : jQuery.parseJSON(jqXHR.responseText).Message;
                        var errTipo             = jqXHR.responseText == "" ? "" : jQuery.parseJSON(jqXHR.responseText).ExceptionType;
                    } catch (e) {
                        var x = e.message.toString();
                    }
                },
                beforeSend                      : function (jqXHR, settings) { },
                complete                        : function (jqXHR, textStatus) {}
            }).done(function (data)
            {
                console.log("done : " + data.toString());
            }).fail(function (xhr, err)
            {
                var responseTitle           = $(xhr.responseText).filter('title').get(0);
            });
        return tempdata;
    }
    this.ajaxPostDataJasonToServerGET_Returm    = function (strMethodToCall, dataToSend, funtionResult) {
        debugger;
        tempdata    = null;

        if (request) request.abort();

        request =
            $.ajax
            ({
                url                             : strMethodToCall,
                type                            : 'GET',
                async                           : true,
                dataType                        : 'json',
                data                            : { parameter: JSON.stringify(dataToSend) },
                contentType                     : 'application/json; charSet=UTF-8',
                beforeSend                      : function ()
                {

                    $.blockUI({
                        message: 'Cargando....',
                        css: {
                            border: 'none',
                            padding: '15px',
                            backgroundColor: '#000',
                            '-webkit-border-radius': '10px',
                            '-moz-border-radius': '10px',
                            opacity: .5,
                            color: '#fff'
                        }
                    });
                },
                complete                        : function ()
                {
                    //  $.unblockUI();
                },
                success                         : function (data, textStatus, jqXHR)
                {
                    tempdata = data;
                    funtionResult(tempdata);
                },
                error                           : function (jqXHR, textStatus, errorThrown)
                {
                    tempdata = null;
                    try {
                        var errEstado           = jqXHR.status;
                        var errMensaje          = jqXHR.responseText == "" ? "" : jQuery.parseJSON(jqXHR.responseText).Message;
                        var errTipo             = jqXHR.responseText == "" ? "" : jQuery.parseJSON(jqXHR.responseText).ExceptionType;
                    } catch (e) {
                        var x = e.message.toString();
                    }
                },
                beforeSend                      : function (jqXHR, settings) { },
                complete                        : function (jqXHR, textStatus) {}
            }).done(function (data)
            {
                console.log("done : " + data.toString());
            }).fail(function (xhr, err)
            {
                var responseTitle           = $(xhr.responseText).filter('title').get(0);
            });
    }

    this.createPOPUP_LOADING = function () {

        var _div = '';
        _div = _div + ' <div id="loading" ';
        _div = _div + ' style="width: 100%;height: 100%;';
        _div = _div + ' top: 0px;left: 0px;position: fixed;opacity: 0.5;';
        _div = _div + ' background-color: #313131;z-index: 9999999;text-align: center;">';
        _div = _div + ' <img alt="...Cargando..." ';
        _div = _div + ' src="../Images/loading.gif"';
        _div = _div + ' style="position: absolute;';
        _div = _div + ' left: 50%; top: 50%; margin-top: -125px;';
        _div = _div + ' margin-left: -180px; vertical-align: middle;" />';
        _div = _div + ' </div>';
        _div = _div + '';

        if ($('#loading').length == 0 ) {
            $('body').append(_div);
        }
    }
    this.deletePOPUP_LOADING = function () {
        if ($('#loading').length) {
            $('#loading').remove();
        }
    }



}
