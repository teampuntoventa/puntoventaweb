﻿/*- info  - success  - error  - warning */
/* CREAR  REGIONES EN JAVASCRIPT CTRL + M + H */


var Basetitle = "Name Web";

var IdDefaultSede = 1;


var ColorHtml = [
                            { highlight: '#1ab394', color: '#a3e1d4' },     // verde
                            { highlight: '#0A30F3', color: '#B4C0FC' },     // azul
                            { highlight: '#EA1237', color: '#F9B7C0' },     // rojo 
                            { highlight: '#EADF5B', color: '#F6F1BA' },     // amarillo
                            { highlight: '#311913', color: '#E3BFB5' },     // negro

                            { highlight: '#6B1BB1', color: '#E7D2F8' },     // morado
                            { highlight: '#B06B1C', color: '#F8E7D2' },     // mostasa
                            { highlight: '#1D9EAF', color: '#D2F4F8' },     // 
                            { highlight: '#AE1E4B', color: '#F7D3DD' },     // 
                            { highlight: '#CD5C5C', color: '#F3D7D7' },     // 
                            { highlight: '#708090', color: '#E2E5E8' },     // 
                            { highlight: '#B0E0E6', color: '#D8F0F2' },    // 
                            { highlight: '#8B0000', color: '#FFCBCB' },     // 
                            { highlight: '#BC8F8F', color: '#ECDEDE' },     // 
                            { highlight: '#D2691E', color: '#F8E2D2' },     // 

                            { highlight: '#8BF9F8', color: '#B5FBFA' }      // celeste
];

var typeAlert = [
    "info",
    "success",
    "error",
    "warning"
];


//#region Opciones Selects Estaticos

var OptionsTipoConsultaVenta = [
                    { dataValue: '1', dataText: 'Venta' },
                    { dataValue: '2', dataText: 'Cotización' }
];

var OptionsPeriocidad = [
                    { dataValue: '1', dataText: 'Anual' },
                    { dataValue: '3', dataText: 'Mensual' }
];

var OptionsTypeDato = [
                    { dataValue: '1', dataText: 'int' },
                    { dataValue: '2', dataText: 'varchar' },
                    { dataValue: '3', dataText: 'decimal' },
                    { dataValue: '4', dataText: 'double' }
];

var OptionsTypeSexo = [
                            { dataValue: 'M', dataText: 'Masculino' },
                            { dataValue: 'F', dataText: 'Femenimo' }
];

var OptionsTypeCivilState = [
                            { dataValue: 'S', dataText: 'Soltero' },
                            { dataValue: 'C', dataText: 'Casado' }
];

var OptionsProvider = [
                            { dataValue: 1, dataText: 'Microsoft SQL Server' }
];

var OptionsTypeDocument = [
                            { dataValue: '1', dataText: 'DNI' },
                            { dataValue: '6', dataText: 'RUC' },
                            { dataValue: '4', dataText: 'Carnet de Extranjeria' },
                            { dataValue: '7', dataText: 'Pasaporte' },
                            { dataValue: '0', dataText: 'Otros Tipos' }
];

var OptionsTypeRegistro = [
                            { dataValue: 'C', dataText: 'Cliente' },
                            { dataValue: 'C', dataText: 'Paciente' },
                            { dataValue: 'C', dataText: 'Compañía de Seguros' },
                            { dataValue: 'C', dataText: 'Clìnica' },
                            { dataValue: 'S', dataText: 'Proveedor' }
];

var OptionsTypePersons = [
                            { dataValue: 'TPN', dataText: 'Natural' },
                            { dataValue: 'SND', dataText: 'Sujeto no domiciliado' },
                            { dataValue: 'TPJ', dataText: 'Juridica' }
];

var OptionsTypeConfirm = [
                            { dataValue: 'N', dataText: 'NO' },
                            { dataValue: 'Y', dataText: 'SI' }
];

var OptionsTypeConfirmEspanish = [
                            { dataValue: 'SI', dataText: 'SI' },
                            { dataValue: 'NO', dataText: 'NO' }

];

var OptionsConsentimientoInfo = [
                            { dataValue: 'SI', dataText: 'SI' },
                            { dataValue: '.....', dataText: 'NO' }
]

var OptionsTypeSeguro = [
                            { dataValue: 'EPS', dataText: 'EPS' },
                            { dataValue: 'SEG PART', dataText: 'Seguro Particular' },
                            { dataValue: 'SEG INTER', dataText: 'Seguro Internacional' }
];

var OptionsTypeDireccion = [
                            { dataValue: 'E', dataText: 'ENVIO' },
                            { dataValue: 'F', dataText: 'FATURACION' },
                            { dataValue: 'A', dataText: 'AMBAS' }
];

var OptionsTypeContact = [
                            { dataValue: 'VEN', dataText: 'Ventas' },
                            { dataValue: 'COB', dataText: 'Cobranzas' },
                            { dataValue: 'LOG', dataText: 'Logistica' },
                            { dataValue: 'TES', dataText: 'Tesoreria' }
];

var OptionsTypePago = [
                            { dataValue: '-1', dataText: 'Contado' },
                            { dataValue: '5', dataText: '15 dias' },
                            { dataValue: '2', dataText: '30 dias' },
                            { dataValue: '3', dataText: '45 dias' }
];

var OptionsTypeVia = [
                            { dataValue: 'Calle', dataText: 'Calle' },
                            { dataValue: 'Jirón', dataText: 'Jirón' },
                            { dataValue: 'Avenida', dataText: 'Avenida' },
                            { dataValue: 'Pasaje', dataText: 'Pasaje' }
];

var OptionsTypeViVienda = [
                            { dataValue: 'casa', dataText: 'casa' },
                            { dataValue: 'departamento', dataText: 'departamento' },
                            { dataValue: 'oficina', dataText: 'oficina' }
];

var OptionsAsignarLista = [
                            { dataValue: '2', dataText: 'DELIVERY' },
                            { dataValue: '3', dataText: 'COURIER' },
];

var OptionsTypePolize = [
                            { dataValue: '0', dataText: 'Ninguno' },
                            { dataValue: '1', dataText: 'Individual' },
                            { dataValue: '2', dataText: 'Corporativa' }
];

var optFarmac = [
                        { dataValue: '1', dataText: 'Farmacéutica 1' },
                        { dataValue: '2', dataText: 'Farmacéutica 2' }
];

var optEstadoRec = [
                        { dataValue: '1', dataText: 'Conforme' },
                        { dataValue: '2', dataText: 'No Conforme' }
];

var optTypeVehiculo = [
                        { dataValue: '1', dataText: 'Automóvil' },
                        { dataValue: '2', dataText: 'Autobús' },
                        { dataValue: '3', dataText: 'Cabezales' },
                        { dataValue: '4', dataText: 'Cuadrimoto' },
                        { dataValue: '5', dataText: 'Tricimoto' },
                        { dataValue: '6', dataText: 'Microbús' },
                        { dataValue: '7', dataText: 'Motocicletas' },
                        { dataValue: '8', dataText: 'Panel' },
                        { dataValue: '9', dataText: 'Pick Up' },
                        { dataValue: '10', dataText: 'Remolques' },
                        { dataValue: '11', dataText: 'Camiones' }
];

var optMotivoTranslado = [
                        { dataValue: '1', dataText: 'Venta' },
                        { dataValue: '2', dataText: 'Compra' },
                        { dataValue: '3', dataText: 'Consignación' },
                        { dataValue: '4', dataText: 'Venta de entrega a terceros' },
                        { dataValue: '5', dataText: 'Venta sujeta a confirmación por comprador' },
                        { dataValue: '6', dataText: 'Traslado entre establecido de una misma empresa' },
                        { dataValue: '7', dataText: 'Devolución' },
                        { dataValue: '8', dataText: 'Recojo de bienes' },
                        { dataValue: '9', dataText: ' Importación' },
                        { dataValue: '10', dataText: 'Exportación' },
                        { dataValue: '11', dataText: 'Traslado zona primaria' },
                        { dataValue: '12', dataText: 'Traslado por emisor itinerante' },
                        { dataValue: '13', dataText: 'Traslado de bienes para trasnformación' }
];

var OptionsTypeProm = [
                            { dataValue: '0', dataText: 'Por Articulo' },
                            { dataValue: '1', dataText: 'Por Grupo' },
                            { dataValue: '2', dataText: 'Por ' }
];

var OptionTypePromAcum = [
                            { dataValue: 'SI', dataText: 'SI' },
                            { dataValue: 'NO', dataText: 'No' }
];


var OptionsTypePromLimit = [
                            { dataValue: '0', dataText: 'Por Stock' },
                            { dataValue: '1', dataText: 'Por Limite de Clientes' },
                            { dataValue: '2', dataText: 'Por Fecha' }
];

//#endregion

//#region Funciones object($.fn)

$.fn.enterPress_FormatText = function (ide) {

    $(this).keypress(function (e) {
        //var regex = new RegExp(/^[a-zA-Z\s]+$/);      -- letras
        //var regex = new RegExp(/[0-9]+$/);            -- numero
        // var regex = new RegExp(/^[0-9A-Za-z]+$/);    // -- letras y numeros
        var regex = null;
        if (ide == "L")
            regex = new RegExp(/^[a-zA-Z\s\b @]+$/);
        else if (ide == "N")
            regex = new RegExp(/[0-9\b\t]+$/);
        else
            regex = new RegExp(/^[0-9A-Za-z@ \b]+$/);

        var str = String.fromCharCode(!e.charCode ? e.which : e.charCode).toUpperCase();
        if (regex.test(str)) {
            event.preventDefault();
            $(this).val($(this).val() + String.fromCharCode(event.which).toUpperCase());
        }
        else {
            $(this).focus();
            e.preventDefault();
            return false;
        }

    });
};

$.urlParam = function (name) {
    var results = new RegExp('[\\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (results != null)
        return results[1] || 0;
    else
        return results; //return results != null ? results[1] || 0 : results;
}

$.fn.iniDataTableObject = function (obj, divID, Btns, response, scroll) {
    try {
        var _Cntx = divID === undefined ? "#ibox-content" : divID;
        $(_Cntx).empty();
        var DataTable = this.selector;
        var html = $(this)[0].outerHTML;
        $(_Cntx).append(html);
        $(DataTable).empty();
        var tb = this;

        if ($(DataTable + " thead").length <= 0) {
            $(DataTable).prepend("<thead></thead>");
            $(DataTable + " > thead:first").append("<tr></tr>");
            obj.forEach(function (e) {
                if (e['Id'] != "")
                    $(DataTable + " > thead > tr:first").append("<th class='text-center' data= '" + $.trim(e['Id']) + "'" + (e.Table['Visible'] == true ? "" : " style='display:none'") + " >" + $.trim(e['Descripcion']) + "</th>");
            });
            if (Btns == true)
                $(DataTable + " > thead > tr:first").append("<th class='text-center' >Acción</th>");
        }

        $(DataTable).dataTable().fnDestroy();
        if (!(response === undefined)) {
            $(DataTable).DataTable({
                "scrollX": scroll === undefined ? false : true,
                dom: '<"html5buttons"B>lTfgitp',
                buttons: [
                    { extend: 'copy', title: DataTable },
                    { extend: 'csv', title: DataTable },
                    { extend: 'excel', title: DataTable },
                    { extend: 'pdf', title: DataTable },
                    {
                        extend: 'print',
                        customize: function (win) {
                            $(win.document.body).addClass('white-bg');
                            $(win.document.body).css('font-size', '10px');

                            $(win.document.body).find('table')
                                    .addClass('compact')
                                    .css('font-size', 'inherit');
                        }
                    }
                ],
                columns: GetColumns(obj).columns
            }).rows.add(response).draw();
        } else {

            $(DataTable).setDataTable();
        }
    } catch (e) {
        toastr.error("(sigloBase)iniDataTableObject: " + e.message.toString(), Basetitle);
    }
}

$.fn.iniValidateObjectForm = function (obj, Func) {
    try {
        $(this.selector).validate({
            rules: getRules(obj),
            highlight: function (element) {
                $(element).closest('.control-group').removeClass('success').addClass('error');
            },
            success: function (element) {
                $(element).closest('.control-group').removeClass('error').addClass('success');
            },
            submitHandler: function (form) {
                Func();
            }
        });

    } catch (e) {
        toastr.error(e.message.toString(), Basetitle);
    }
}





$.fn.CreateCheck = function () {
    this.iCheck({
        checkboxClass: 'icheckbox_square-green',
        radioClass: 'iradio_square-green',
    });
};

$.fn.CreateTouchSpinNumber = function () {
    this.TouchSpin({
        min: 0,
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white'
    });
};

$.fn.CreateTouchSpinDecimal = function (decimales) {
    this.TouchSpin({
        min: 0,
        max: 1000000000,
        decimals: decimales, // 2,
        buttondown_class: 'btn btn-white',
        buttonup_class: 'btn btn-white',
        // none | floor | round | ceil
        forcestepdivisibility: 'none',
    });
};

$.fn.CreateTouchSpin = function () {
    this.TouchSpin({
        // Minimum value.
        min: 0,
        // Maximum value.

        max: 100,
        // Applied when no explicit value is set on the input with the value attribute.

        // Empty string means that the value remains empty on initialization.
        initval: '',
        replacementval: '',

        // Number of decimal points.
        decimals: 0,

        // none | floor | round | ceil
        forcestepdivisibility: 'round',

        // Enables the traditional up/down buttons.
        verticalbuttons: false,

        // Class of the up button with vertical buttons mode enabled.
        verticalupclass: 'glyphicon glyphicon-chevron-up',

        // Class of the down button with vertical buttons mode enabled.
        verticaldownclass: 'glyphicon glyphicon-chevron-down',

        // Boost at every nth step.
        boostat: 5,

        // If enabled, the the spinner is continually becoming faster as holding the button.
        booster: true,

        // Maximum step when boosted.
        maxboostedstep: 10,

        // Text after the input.
        postfix: '%',

        // Text before the input.
        prefix: '$',

        // Extra class(es) for prefix.
        prefix_extraclass: '',

        //  Extra class(es) for postfix.
        postfix_extraclass: '',

        // Incremental/decremental step on up/down change.
        step: 1,

        // Refresh rate of the spinner in milliseconds.
        stepinterval: 100,

        // Time in milliseconds before the spinner starts to spin.
        stepintervaldelay: 500,

        //  Enables the mouse wheel to change the value of the input.
        mousewheel: true,

        //  Class(es) of down button.
        buttondown_class: 'btn btn-default',

        //  Class(es) of up button.
        buttonup_class: 'btn btn-default',

        // Text for down button
        buttondown_txt: '-',

        // Text for up button
        buttonup_txt: '+'
    });
};

//$.fn.CreateDatepicker = function () {
//    $(this).find(".input-group.date").datepicker({
//        //dateFormat: "yyyy-mm-dd",
//        //appendText: "(yyyy-mm-dd)",
//        //language: “de-DE”,
//        format: "dd/mm/yyyy",
//        language: "it-IT",
//        todayBtn: "linked",
//        keyboardNavigation: false,
//        forceParse: false,
//        calendarWeeks: true,
//        autoclose: true
//    });
//};
$.fn.CreateDatepicker = function () {
    $(this).parent(".input-group.date").datepicker({
        //dateFormat: "yyyy-mm-dd",
        //appendText: "(yyyy-mm-dd)",
        //language: “de-DE”,
        format: "dd/mm/yyyy",
        language: "it-IT",
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        calendarWeeks: true,
        autoclose: true
    });
};

$.fn.CreateDatepickerMonth = function () {
    $(this).parent(".input-group.date").datepicker({
        //dateFormat: "yyyy-mm-dd",
        //appendText: "(yyyy-mm-dd)",
        //language: “de-DE”,
        format: "mm",
        autoclose: true,
        language: "it-IT",
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        todayHighlight: true,
        minViewMode: 1
    });
};

$.fn.CreateDatepickerYear = function () {
    $(this).parent(".input-group.date").datepicker({
        //dateFormat: "yyyy-mm-dd",
        //appendText: "(yyyy-mm-dd)",
        //language: “de-DE”,
        format: "yyyy",
        autoclose: true,
        language: "it-IT",
        todayBtn: "linked",
        keyboardNavigation: false,
        forceParse: false,
        viewMode: "years",
        minViewMode: "years"
    });
};

$.fn.CreateSelected = function (array, val, text, etiqueta, index) {
    var obj = this.selector;
    try {
        $(obj).find('option').remove();
        if (index === undefined || index == false) {
            if (etiqueta === undefined)
                $(obj).append('<option value="" selected="selected"> -- Seleccione --</option>');
            else
                $(obj).append('<option value="0" selected="selected"> -- ' + etiqueta + ' --</option>');
        }
        /*
    else if (index == true)
        $(obj).append('<option value="0" disabled="disabled" selected="selected"> -- Seleccione --</option>');
        */

        $.each(array, function (i, item) {
            var objtemp = item;
            $(obj).append('<option value="' + item[val] + '">' + item[text] + '</option>');
        });
    } catch (e) {
        toastr.error(e.message.toString(), Basetitle);
    }

};

$.fn.CreateSelected2 = function (array, val, text, etiqueta, index) {
    var obj = this.selector;
    try {
        $(obj).find('option').remove();
        if (index === undefined || index == false) {
            if (etiqueta === undefined)
                $(obj).append('<option value="" selected="selected"> -- Seleccione --</option>');
            else
                $(obj).append('<option value="0" selected="selected"> -- ' + etiqueta + ' --</option>');
        }
        $.each(array, function (i, item) {
            var objtemp = item;
            $(obj).append('<option value="' + item[val] + '">' + item[text] + '</option>');
        });
        $(obj).select2();
    } catch (e) {
        toastr.error(e.message.toString(), Basetitle);
    }

};

$.fn.ClearSelected = function () {
    var obj = this.selector;
    try {
        if ($(obj).data('select2')) {
            $(obj).find('option').remove();
            $(obj).select2('val', 'All');
        }
        else {
            $(obj).find('option').remove();
        }
    } catch (e) {
        console.log(e.message.toString());
    }
};

$.extend({
    /*
        //var redirect                = path;
        //$.redirectPost(redirect, { parameters_one: 'example', parameters_thows: 'abc' });
    */
    redirectPost: function (location, args) {
        var form = '';
        $.each(args, function (key, value) {
            form += '<input type="hidden" name="' + key + '" value="' + value + '">';
        });
        $('<form action="' + location + '" method="POST">' + form + '</form>').appendTo('body').submit();
    }
});

$.dateTime = function (dateObject) {
    var d = new Date(dateObject);
    d.setDate(d.getDate() + 1);

    var day = d.getDate();
    var month = d.getMonth() + 1;
    var year = d.getFullYear();
    if (day < 10) {
        day = "0" + day;
    }
    if (month < 10) {
        month = "0" + month;
    }
    var date = day + "/" + month + "/" + year;

    return date;
};

$.dateTimeFormat = function (x, y) {
    try {
        var z = {
            M: x.getMonth() + 1,
            d: x.getDate(),
            h: x.getHours(),
            m: x.getMinutes(),
            s: x.getSeconds()
        };

        y = y.replace(/(M+|d+|h+|m+|s+)/g, function (v) {
            return ((v.length > 1 ? "0" : "") + eval('z.' + v.slice(-1))).slice(-2)
        });

        return y.replace(/(y+)/g, function (v) {
            return x.getFullYear().toString().slice(-v.length)
        });

    } catch (e) {
        //alert(e.message.toString());
    }
}

$.DatetimeDaysCalc = function (f, d) {
    var fecha = new Date(f);
    var dias = d + 1;
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
}

$.fn.serializeFormJSON = function () {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function () {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


$.fn.iniDataTableButton = function (obj, divID, Btns) {

    var _Cntx = divID === undefined ? "#ibox-content" : divID;
    $(_Cntx).empty();
    var DataTable = this.selector;
    var html = $(this)[0].outerHTML;
    $(_Cntx).append(html);
    $(DataTable).empty();

    if ($(DataTable + " thead").length <= 0) {
        $(DataTable).prepend("<thead></thead>");
        $(DataTable + " > thead:first").append("<tr></tr>");

        var _arrProps = Object.getOwnPropertyNames(obj);
        _arrProps.forEach(function (e, i) {
            var objtext = obj[e] == "" ? e : obj[e];
            if (i == 0)
                $(DataTable + " > thead > tr:first").append("<th data= '" + $.trim(e) + "' style='display:none'>" + $.trim(objtext) + "</th>");
            else
                $(DataTable + " > thead > tr:first").append("<th data= '" + $.trim(e) + "'>" + $.trim(objtext) + "</th>");
        });
        if (Btns == true)
            $(DataTable + " > thead > tr:first").append("<th>Acción</th>");
    }
};

$.fn.setcolumnsDataTable = function () {
    var dataTableHeaders = this.dataTable().find('thead th').filter("[data]").map(function () {
        return { data: $(this).attr("data") };
    }).get();
    return dataTableHeaders;
}

$.fn.iniDataTable = function (obj, divID) {

    var _Cntx = divID === undefined ? "#ibox-content" : divID;
    $(_Cntx).empty();
    var DataTable = this.selector;
    var html = $(this)[0].outerHTML;
    $(_Cntx).append(html);
    $(DataTable).empty();

    if ($(DataTable + " thead").length <= 0) {
        $(DataTable).prepend("<thead></thead>");
        $(DataTable + " > thead:first").append("<tr></tr>");

        var _arrProps = Object.getOwnPropertyNames(obj);
        _arrProps.forEach(function (e, i) {
            var objtext = obj[e] == "" ? e : obj[e];
            $(DataTable + " > thead > tr:first").append("<th data= '" + e + "' >" + objtext + "</th>");
        });
    }
};


$.fn.setDataTableResponse = function (response) {
    var dataTableHeaders = this.setcolumnsDataTable();
    this.dataTable().fnDestroy();
    this.DataTable({
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy' },
            { extend: 'csv' },
            { extend: 'excel', title: 'ExampleFile' },
            { extend: 'pdf', title: 'ExampleFile' },

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ],
        columns: dataTableHeaders
    }).rows.add(response).draw();
}

$.fn.setDataTableButtonResponse = function (col, response) {
    var tb = this;
    var colm = col.columns;
    try {
        tb.dataTable().fnDestroy();
        tb.DataTable({
            dom: '<"html5buttons"B>lTfgitp',
            buttons: [
                { extend: 'copy' },
                { extend: 'csv' },
                { extend: 'excel', title: 'ExampleFile' },
                { extend: 'pdf', title: 'ExampleFile' },

                {
                    extend: 'print',
                    customize: function (win) {
                        $(win.document.body).addClass('white-bg');
                        $(win.document.body).css('font-size', '10px');

                        $(win.document.body).find('table')
                                .addClass('compact')
                                .css('font-size', 'inherit');
                    }
                }
            ],
            columns: colm
        }).rows.add(response).draw();
        td.setDataTable();
    } catch (e) {
        alert("(sigloBase)setDataTableButtonResponse: " + e.message.toString());
    }
}

$.fn.setDataTable = function () {
    this.DataTable({

        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy' },
            { extend: 'csv' },
            { extend: 'excel', title: 'ExampleFile' },
            { extend: 'pdf', title: 'ExampleFile' },

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });
}

$.fn.setDataTableScroll = function () {
    this.DataTable({
        "scrollX": true,
        dom: '<"html5buttons"B>lTfgitp',
        buttons: [
            { extend: 'copy' },
            { extend: 'csv' },
            { extend: 'excel', title: 'ExampleFile' },
            { extend: 'pdf', title: 'ExampleFile' },

            {
                extend: 'print',
                customize: function (win) {
                    $(win.document.body).addClass('white-bg');
                    $(win.document.body).css('font-size', '10px');

                    $(win.document.body).find('table')
                            .addClass('compact')
                            .css('font-size', 'inherit');
                }
            }
        ]
    });
}
//#endregion

//#region Funciones Varios

function addLeadingZeros(sNum, len) {
    len -= sNum.length;
    while (len--) sNum = '0' + sNum;
    return sNum;
}

function runFromCharCode(key) {
    var output = eval("String.fromCharCode(" + key + ")");
    return output;
}

function runCharCodeAt(key) {
    var output = key.charCodeAt(0);
    return output;
}

function IsNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function capitalizeFirstLetter(string) {
    return string.charAt(0).toUpperCase() + string.slice(1);
}

function GetSessionValue(name) {
    var WebMethodpath = "../Login.aspx/GetSessionValue";
    var sendInfo = { Name: name };
    var response = MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d;
    return response;
}

function SetSessionValue(name, val) {
    var WebMethodpath = "../Login.aspx/SetSessionValue";
    var sendInfo = { Name: name, Value: val };
    var response = MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d;
}

function DateFormatPickerSet(fecha) {
    var newFecha = "";
    if (fecha) {
        var date = new Date(fecha);
        var year = date.getFullYear();
        var month = format_two_digits((1 + date.getMonth()).toString());
        var day = format_two_digits(date.getDate().toString());
        newFecha = day + '/' + month + '/' + year;
    }
    /*
      var date        = new Date(fecha);
        var year = date.getFullYear();
        var month = (1 + date.getMonth()).toString();
        var day = date.getDate().toString();
        month = month.length > 1 ? month : '0' + month;
        day = day.length > 1 ? day : '0' + day;
        newFecha = day + '/' + month + '/' + year;
    */
    return newFecha;
}

function DateFormatPickerGet(fecha) {
    var newFecha = "";
    if (fecha) {
        var dateParts = fecha.split("/");
        newFecha = new Date(dateParts[2],
                            dateParts[1] - 1,
                            dateParts[0],
                            new Date().getHours(),
                            new Date().getMinutes(),
                            new Date().getSeconds()
                            );
    }
    return newFecha;
}

function validCheck(e) {
    var keyCode = (e.which) ? e.which : e.keyCode;
    if ((keyCode >= 48 && keyCode <= 57) || (keyCode == 8))
        return true;
    else if (keyCode == 46) {
        var curVal = document.activeElement.value;
        if (curVal != null && curVal.trim().indexOf('.') == -1)
            return true;
        else
            return false;
    }
    else
        return false;
}

function time_format(d) {
    hours = format_two_digits(d.getHours());
    minutes = format_two_digits(d.getMinutes());
    seconds = format_two_digits(d.getSeconds());
    return hours + ":" + minutes + ":" + seconds;
}

function format_two_digits(n) {
    return n < 10 ? '0' + n : n;
}

function TodecimalFormat(num) {
    var cadena = ""; var aux;
    var cont = 1, m, k;
    if (num < 0) aux = 1; else aux = 0;
    num = num.toString();
    for (m = num.length - 1; m >= 0; m--) {
        cadena = num.charAt(m) + cadena;
        if (cont % 3 == 0 && m > aux) cadena = "." + cadena; else cadena = cadena;
        if (cont == 3) cont = 1; else cont++;
    }
    cadena = cadena.replace(/.,/, ",");
    return cadena;

}

function ArrayFill(valorIni, ValorFin) {
    var Array = [];
    for (var i = ValorFin ; i > valorIni ; i--) {
        var objects = new Object();
        objects.dataValue = i;
        objects.dataText = i;
        Array.push(objects);
    }
    return Array;
};

function AlertSwal(text, type) {
    swal({
        title: Basetitle,
        text: text,
        type: typeAlert[type],
        ButtonColor: "1AC943",
    });
}

function AlertSwalInput(text, type, fn_inputValue) {
    swal({
        title: Basetitle,
        text: text,
        type: typeAlert[type],
        showCancelButton: true,
        closeOnConfirm: false,
        animation: "slide-from-top",
        inputPlaceholder: "Write something",
    },
        function (inputValue) {
            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to write something!"); return false
            }
            var resultado = inputValue;
            fn_inputValue(resultado);
        }
    );
}

function AlertSwalConfirm(text, type, fn_Confirm) {
    swal({
        title: Basetitle,
        text: text,
        type: typeAlert[type],
        showCancelButton: true,
        confirmButtonColor: '#1AC943',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: "No",
        closeOnConfirm: false
    }, function () {
        $(".sa-info").css("display", "none");
        $(".sa-custom").css({ 'background-image': 'url(../Images/ajax-loader_3.gif)', 'width': '40px', 'height': '40px', 'display': 'block' });
        fn_Confirm();
    }
    );
};

function AlertSwalOptionConfirm(text, type, fn_Confirm) {
    swal({
        title: Basetitle,
        text: text,
        type: typeAlert[type],
        showCancelButton: true,
        confirmButtonColor: "1AC943",
        confirmButtonText: "Si",
        cancelButtonText: "No",
        closeOnConfirm: false,
        closeOnCancel: false
    }
    , function (isConfirm) { var resultado = isConfirm; fn_Confirm(resultado); });
}

function TableToJSON(tblObj) {
    var data = [];
    var $headers = $(tblObj).find("th");
    var $rows = $(tblObj).find("tbody tr").each(function (index) {
        $cells = $(this).find("td");
        data[index] = {};
        $cells.each(function (cellIndex) {
            data[index][$($headers[cellIndex]).html()] = $(this).html();
        });
    });
    return data;
}

function FillObject(obj, Entity) {
    try {
        obj.forEach(function (e, i) {
            Entity[e['Id']] = $(e['Input']).val();
        });

    } catch (e) {
        toastr.error(e.message.toString(), Basetitle);
    }
    return Entity;
}

function FillHtmlObject(obj, response) {
    try {
        obj.forEach(function (e, i) {
            $(e['Input']).val(response[e['Id']]);
        });

    } catch (e) {
        toastr.error(e.message.toString(), Basetitle);
    }
}

function GetColumns(obj) {
    var result = { columns: [] };
    try {
        obj.forEach(function (e, i) {

            if (e['Id'] != "" || e['Descripcion'] == "Buttons") {
                var item = {};
                item.data = e.Table['func'] == null ? e['Id'] : null;
                item.visible = e.Table['Visible'];
                if (!(e.Table['width'] == null))
                    item.width = e.Table['width'];
                if (!(e.Table['func'] == null))
                    item.mRender = function (data, type, full) { return e.Table.func(data, type, full); };


                result.columns.push(item);
            }
        });

    } catch (e) {
        alert(e.message.toString());
    }
    return result;
}

function ClearHtmlObject(obj) {
    try {
        obj.forEach(function (e, i) {
            $(e['Input']).val('');
        });
    } catch (e) {
        toastr.error(e.message.toString(), Basetitle);
    }
}

function IniHtmlObject(obj, Type) {
    try {
        obj.forEach(function (e, i) {
            var Input = e['Input'];
            var Type = e['Type'];
            var Source = e['Source'];
            var Valid = e['Valid'];
            switch (Type.toString().toUpperCase()) {
                case 'HIDDEN':
                    $(Input).val(0); break;
                case 'TEXT':
                    $(Input).val(''); break;
                case 'SELECT':
                    $(Input).CreateSelected(Source.data, Source.dataValue, Source.dataText, Source.etiqueta, Source.index); break;
                case 'SELECT2':
                    $(Input).CreateSelected2(Source.data, Source.dataValue, Source.dataText, Source.etiqueta, Source.index); break;
                case 'DATE':
                    $(Input).CreateDatepicker(); break;
                case 'MONTH':
                    $(Input).CreateDatepickerMonth(); break;
                case 'YEAR':
                    $(Input).CreateDatepickerYear(); break;
                case 'TIME':
                    $(Input).parent(".clockpicker").clockpicker(); break;
                case 'NUMBER':
                    $(Input).val(0); break;
                case 'DOUBLE':
                    $(Input).val(parseFloat(0)); break;
                case 'TEXTAREA':
                    $(Input).val(''); break;
                case 'DROPZONE':
                    $(Input).val(''); break;
            };

            $(Input).removeAttr('disabled');
            if (Type == 'V')
                $(Input).attr('disabled', 'disabled');
        });
    } catch (e) {
        toastr.error(e.message.toString(), Basetitle);
    }
}

function getRules(obj) {
    var rules = {};
    try {
        obj.forEach(function (e, i) {
            var _arrProps = Object.getOwnPropertyNames(e.Valid);
            if (_arrProps.length > 0) {
                var ruleObj = {};
                _arrProps.forEach(function (v, i) {
                    ruleObj[v] = e.Valid[v];
                });
                rules[e.Input.replace('#', '')] = ruleObj;
            }
        });

    } catch (e) {
        toastr.error(e.message.toString(), Basetitle);
    }
    return rules;
}


//#endregion


//#region List SAP
function ListPaices() {
    var WebMethodpath = "../Index.aspx/getListPaices";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListDepartamentos(valor) {
    var WebMethodpath = "../Index.aspx/getListDepartamentos";
    var sendInfo = { code: valor };
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListProvincias(valor) {
    var WebMethodpath = "../Index.aspx/getListProvincias";
    var sendInfo = { code: valor };
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListDistritos(valor) {
    var WebMethodpath = "../Index.aspx/getListDistritos";
    var sendInfo = { code: valor };
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListAseguradora() {
    var WebMethodpath = "../Index.aspx/getListAseguradora";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListMedico() {
    var WebMethodpath = "../Index.aspx/getListMedico";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListDiagnostico() {
    var WebMethodpath = "../Index.aspx/getLisDiagnostico";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListEspecialidad(parameter) {

    var WebMethodpath = "../Index.aspx/getLisEspecialidad";
    var sendInfo = { UFD1: parameter };
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function LisCondicionPago() {
    var WebMethodpath = "../Index.aspx/getLisCondicionPago";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListOCRD(val) {
    var WebMethodpath = "../Index.aspx/getListOCRD";
    var sendInfo = { p: val };
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

//#endregion

//#region getSelect SAP

function getSelectOCRD(object, val) {
    var ent = ListOCRD(val);
    $(object).CreateSelected2(ent, "CardCode", "CardName");
}

function getSelectAseguradora(object) {
    var ent = ListAseguradora();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectMedico(object) {
    var ent = ListMedico();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectPaices(object) {
    var ent = ListPaices();
    $(object).CreateSelected2(ent, "Code", "Name");
}

function getSelectDepartamentos(object, valor) {
    var ent = ListDepartamentos(valor);
    $(object).CreateSelected2(ent, "Code", "Name");
}

function getSelectProvincias(object, valor) {
    var ent = ListProvincias(valor);
    $(object).CreateSelected2(ent, "Code", "Name");
}

function getSelectDistritos(object, valor) {
    var ent = ListDistritos(valor);
    $(object).CreateSelected2(ent, "Code", "Name");
}

function getSelectDiagnostico(object) {
    var ent = ListDiagnostico();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectDiagnostico2(object) {
    var ent = ListDiagnostico();
    $(object).CreateSelected2(ent, "Code", "Name");
}

function getSelectCondicionPago(object) {
    var ent = LisCondicionPago();
    $(object).CreateSelected(ent, "GroupNum", "PymntGroup");
}

//#endregion

//#region List SQL
function ListMedico_SQL() {
    var WebMethodpath = "../Index.aspx/getListMedico_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListDiagnostico_SQL() {
    var WebMethodpath = "../Index.aspx/getLisDiagnostico_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListEntidadSalud_SQL() {
    var WebMethodpath = "../Index.aspx/getListEntidadSalud_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListDesapacho_SQL() {
    var WebMethodpath = "../Index.aspx/getListDesapacho_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListHorarioDesapacho_SQL() {
    var WebMethodpath = "../Index.aspx/getListHorarioDesapacho_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListDirecciones_SQL(code) {
    var WebMethodpath = "../Index.aspx/getListDirecciones_Code_SQL";
    var sendInfo = { carcode: code };
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListtCantidad_Dosis_SQL() {
    var WebMethodpath = "../Index.aspx/getListCantidad_Dosis_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListInterval_frecuencia_SQL() {
    var WebMethodpath = "../Index.aspx/getListInterval_frecuencia_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListVia_SQL() {
    var WebMethodpath = "../Index.aspx/getListVia_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListTipo_Pago_SQL() {
    var WebMethodpath = "../Index.aspx/getListTipo_Pago_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListTipo_Tarjeta_SQL() {
    var WebMethodpath = "../Index.aspx/getListTipo_Tarjeta_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListBanco_SQL() {
    var WebMethodpath = "../Index.aspx/getListBanco_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListDocumentsTable_SQL() {
    var WebMethodpath = "../Views/MantWebDocumentItems.aspx/ListarDocumentsTable_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListDocumentsType_SQL(p) {
    var WebMethodpath = "../Views/MantWebDocumentItems.aspx/ListarDocumentsType_SQL";
    var sendInfo = { p: p };
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListDocuments_SQL(object) {
    var WebMethodpath = "../Views/MantWebDocumentItems.aspx/ListarDocuments_SQL";
    var sendInfo = {};
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

function ListGeneric_SQL(name) {
    var WebMethodpath = "../Index.aspx/getListGeneric_SQL";
    var sendInfo = { sp: name };
    var response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
    return response;
}

//#endregion

//#region getSelect SQL
function getSelectDiagnostico2_SQL(object) {
    var ent = ListDiagnostico_SQL();
    $(object).CreateSelected2(ent, "Code", "Name");
}

function getSelectMedico_SQL(object) {
    var ent = ListMedico_SQL();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectEntidadSalud_SQL(object) {
    var ent = ListEntidadSalud_SQL();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectDesapacho_SQL(object) {
    var ent = ListDesapacho_SQL();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectHorarioDesapacho_SQL(object) {
    var ent = ListHorarioDesapacho_SQL();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectCantidad_Dosis2_SQL(object) {
    var ent = ListtCantidad_Dosis_SQL();
    $(object).CreateSelected2(ent, "Code", "Name");
}

function getSelectInterval_frecuencia2_SQL(object) {
    var ent = ListInterval_frecuencia_SQL();
    $(object).CreateSelected2(ent, "Code", "Name");
}

function getSelectVia2_SQL(object) {
    var ent = ListVia_SQL();
    $(object).CreateSelected2(ent, "Code", "Name");
}

function getSelectTipo_Pago_SQL(object) {
    var ent = ListTipo_Pago_SQL();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectTipo_Tarjeta_SQL(object) {
    var ent = ListTipo_Tarjeta_SQL();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectBanco_SQL(object) {
    var ent = ListBanco_SQL();
    $(object).CreateSelected(ent, "Code", "Name");
}

function getSelectDocumentsTable_SQL(object) {
    var ent = ListDocumentsTable_SQL();
    $(object).CreateSelected(ent, "DctTbId", "DctTbDescripcion", "Seleccione", false);
}

function getSelectDocumentsType_SQL(object, p) {
    var ent = ListDocumentsType_SQL(p);
    $(object).CreateSelected(ent, "DctTpId", "DctTpDescripcion", "Todos", false);
}

function getSelectDocuments_SQL(object, p) {
    var ent = ListDocuments_SQL(p);
    $(object).CreateSelected(ent, "DcmId", "DcmNombre");
}

function getSelectGeneric_SQL(object, name) {
    var ent = ListGeneric_SQL(name);
    $(object).CreateSelected(ent, "Code", "Name");
}

//#endregion

//#region Fill Objects Webs

function objFillMantWebPromociones() {
    try {
        var Obj = new Object();
        Obj.ItemCode = "#txtItemCode";
        Obj.ItemName = "#txtItemName";
        Obj.U_LH_PriAc = "#txtU_LH_PriAc";
        Obj.U_LH_SUBGDesc = "#txtU_LH_SUBGDesc";
        Obj.FirmName = "#txtFirmName";
        Obj.ItmGroupName = "#txtItmGroupName";
    } catch (e) {
        toastr.error(e.message.toString(), Basetitle);
    }
    return Obj;
}

//#endregion

