﻿var jsLogin = new function () {
    var path = "../Login.aspx";
    var response = null;
    var sendInfo = null;
    var WebMethodpath = "";

    this.Login = function (idSistema) {
        try {
            var _usuario = $("#txtusuario").val();
            var _password = $("#txtpassword").val();

            WebMethodpath = path.concat("/Logeo");
            sendInfo = { Usuario: _usuario, Password: _password, System: idSistema };
            response = MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d;

            if (parseInt(response) > 0) {
                toastr.warning('Datos correctos', Basetitle);
                setTimeout(function () {
                    toastr.options = {
                        closeButton: true,
                        progressBar: true,
                        showMethod: 'slideDown',
                        timeOut: 50
                    };
                    document.location.href = "Index.aspx";

                }, 1500);
            }
            else
                toastr.error("Datos incorrectos", Basetitle);

        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.getUlrParameterLogin = function (_usuario, idSistema) {
        try {
            WebMethodpath       = path.concat("/createSeccionLogin");
            sendInfo            = { Usuario: _usuario, System: idSistema };
            response            = MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d;
            if (parseInt(response) > 0) {
                document.location.href = "Index.aspx";
            }
            else {
                document.location.href = "../Login.aspx";
                toastr.error("error al crear Secciones para el Logeo", Basetitle);
            }

        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    }

    this.ClearSession = function () {
        try {
            try {
                WebMethodpath       = path.concat("/DeleteSeccionLogin");
                sendInfo            = {  };
                response            = MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d;
                if (parseInt(response) > 0) {
                    $("#hddusuarioLogin").val("0");
                    $("#hddsistemaLogin").val("0");
                    $("#hddroolLogin").val("0");
                    $("#hddnameusuarioLogin").val("");
                    document.location.href = "../Login.aspx";
                }
                else {
                    toastr.error("error al crear salir del sistema", Basetitle);
                }

            } catch (e) {
                toastr.error(e.message.toString(), Basetitle);
            }
        } catch (e) {
            toastr.error(e.message.toString(), Basetitle);
        }
    };

}