﻿$(function () {
    try {
        // alert("function");

        var idusuario = $("#hddusuarioLogin").val() == "" ? 0 : $("#hddusuarioLogin").val();
        var idsistema = $("#hddsistemaLogin").val() == "" ? 0 : $("#hddsistemaLogin").val();
        idusuario = 1; idsistema = 1;
        if (idsistema == 0 || idsistema == 0)
        { location.href = "../Login.aspx"; }
        else {
            //jsMaster.createMenuOptions(idusuario, idsistema);
        }
    } catch (e) {
        alert(e.message.toString());
    }
});

$(document).ready(function () {

    //alert("function ready");

    if (typeof (getCookie("activeLi")) != "undefined" && getCookie("activeLi").length > 0) {
        $('#side-menu li a').parent().addClass("active");
    }
    $("#side-menu li  a").click(function (a) {

        if (typeof (getCookie("activeLi")) != "undefined");
        removeActive();
        if ($(this).parent().hasClass("active") == false) {
            $(this).parent().addClass("active");
            $(this).parent().find("ul").addClass("in");

            var ClickTemp = typeof ($(this).parent().find("a").find(".nav-label").html()) == "undefined" ? "" : $(this).parent().find("a").find(".nav-label").html();
            if ($.trim(ClickTemp) != "") SetSessionValue("ClickTemp", ClickTemp);
        }
        else {
            $(this).parent().removeClass("active");
            $(this).parent().find("ul").removeClass("in");
        }
        setActiveCookie(this.getAttribute("href"));
    });
});

function removeActive() {
    $("#side-menu li").each(function (li) {
        $(this).removeClass("active");
        $(this).parent().find("ul").removeClass("in");
    });
}

function setActiveCookie(active) {
    var d = new Date();
    d.setTime(d.getTime() + (30 * 24 * 60 * 60 * 1000));
    var expires = "expires=" + d.toUTCString();
    document.cookie = "activeLi=" + active + "; " + expires;
}

function getCookie(cname) {
    var name = cname + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1);
        if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
    }
    return "";
}

var jsMaster = new function () {

    var path = "../MasterSecurity.aspx";

    this.createMenuOptions = function (idusuario, idsistema) {

        var menu = $("#side-menu");
        if (menu.find('li').length == 0) {
            if (menu.find('li').hasClass("nav-header") == false) {

                var objDatosUsuario = getPeopleId(idusuario);

                menu.append($("<li>").addClass("nav-header")
                        .append($("<div>").addClass("dropdown profile-element")
                             .append($("<span>").append("<img alt='image' class='img-rounded' src='../Images/user_default.jpg' />"))
                             .append($("<a>").addClass("dropdown-toggle")
                                             .attr("href", "#")
                                             .attr("data-toggle", "dropdown")
                                             .append($("<span>").addClass("clear")
                                                .append($("<span>").addClass("block m-t-xs")
                                                    .append("<strong id='idNameUsuario' class='font-bold'>" + objDatosUsuario.PeoName + " " + objDatosUsuario.PeoLastnamep + "</strong>")
                                                     )
                                                    )
                             )
                        )
                       .append($("<div>").addClass("logo-element").html("GLE+"))
                    );


                var objMenu = getMenuStrip(idusuario, idsistema);
                var getParents = Enumerable.From(objMenu).Where(function (x) { return x.MenIdchildren == 0 })
                                                                 .OrderBy(function (x) { return x.MenOrder })
                                                                 .ToArray();

                var idrol = $("#hddroolLogin").val() == "" ? 0 : $("#hddroolLogin").val();
                var objsubMenu = getSubMenuStrip(idusuario, idsistema, idrol);
                if (objsubMenu != null) {
                    menu.find("li").find("div").find("a").find(".clear").append($("<span>").addClass("text-muted text-xs block").html(objsubMenu.RolName).append("<b class='caret'>"));
                    menu.find("li").find("div").append($("<ul>").attr("id", "ulSubMenu").addClass("dropdown-menu animated fadeInRight m-t-xs"));
                    menu.find("li").find("div").find("#ulSubMenu").append("<li><a href='javascript:jsLogin.ClearSession();'>Logout</a></li>");
                }

                getParents.forEach(function (Parents) {
                    var getChilds = Enumerable.From(objMenu).Where(function (x) { return x.MenIdchildren == Parents.MenId })
                                                                 .OrderBy(function (x) { return x.MenOrder })
                                                                 .ToArray();

                    var getSessionClick = $("#sessionClick").val() == "" ? "" : $("#sessionClick").val();
                    var cssLi = "";
                    var cssUl = "";
                    if (Parents.MenName == $.trim(getSessionClick)) { cssLi = "active"; cssUl = "in"; }

                    menu.append($("<li>").addClass(cssLi)
                           .append($("<a>").attr("href", "#")
                               .append("<i class='fa fa-th-large'>")
                               .append($("<span>").addClass("nav-label").attr("data-i18n", "nav").html(Parents.MenName))
                               .append("<span class='fa arrow'></span>")
                                   )
                                .append($("<ul>").attr("id", Parents.MenId).addClass("nav nav-second-level collapse " + cssUl))
                               );

                    getChilds.forEach(function (Childs) {
                        menu.find("#" + Parents.MenId + "").append($("<li>").addClass("").append($("<a>").attr("href", Childs.Menoptionform).html(Childs.MenName)));
                    });
                });
                $("#side-menu").trigger("create");
            }
        }
    }

    function getPeopleId(idusu) {
        var response = null;
        var WebMethodpath = path.concat("/getPeopleId");
        try {
            var sendInfo = { idusuario: idusu };
            response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
        } catch (e) {

        }
        return response;
    }

    function getMenuStrip(idusu, system) {
        var response = null;
        var WebMethodpath = path.concat("/getMuenUsuarioSystem");
        try {
            var sendInfo = { idusuario: idusu, idsystem: system };
            response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
        } catch (e) {

        }
        return response;
    }

    function getSubMenuStrip(idusu, system, idrol) {
        var response = null;
        var WebMethodpath = path.concat("/getRoleSystem");
        try {
            var sendInfo = { idusuario: idusu, idsystem: system, roolId: idrol };
            response = JSON.parse(MAIN.ajaxPostDataJasonToServerPOST_Line(WebMethodpath, sendInfo).d);
        } catch (e) {

        }
        return response;
    }

};