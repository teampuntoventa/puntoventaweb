﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCPV.Web.Models
{
    public static class ResourceSap
    {
        public const string Company = "PE_SBO_LIFEHOPE_D1";
        public const string Key = "eDTL1q27/E8X3rLElDFhAsZoYaDXS8M/ox96p6n4L/Q3t19IFtV55NV2pw2Gq11u";
    }
    public static class Constantes
    {
        public const string pathDigitalizacion = "/Files/Digitalizacion/";
        public const string modeInsert = "I";
        public const string modeEdith = "E";
        public const string sepradador = "_";
    }
    public static class Configuration
    {
        public const string EXT_TXT = ".txt";
        public const string RANDOM_VALUE = "?r=";
        public const string EXT_PDF = ".pdf";
        public const string ANTEFIJO_DOC = "D-";
        public const string PATH_SCANNER = @"E:\";
        public const string TYPE_PATH_SCANNER = @"SCANNER\";


    }
    public static class ResourceBaseController
    {
        public const string fileFlag = "flag.txt";
        public const string processFlag = "process.txt";
        public const string fileTemp = "temporal.pdf"; // Este es el archivo que se crea temporalmente mientras se hace el merge de los archivos pdf.
        public const string Lote = "LOTE";
    }
    public static class ResourceIOHelper
    {
        public const string slash = "/";
        public const string backSlash = @"\";
        public const string previous = @"\..\";
        public const string minus = "-";
        public const string extPDF = ".pdf";
        public const string upLevelFolder = "../";
    }
}