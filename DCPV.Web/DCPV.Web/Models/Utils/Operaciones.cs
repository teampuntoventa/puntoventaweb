﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCPV.Web.Models.Utils
{
    public enum EnumTipoOperaciones
    {
        Aritmetica,
        Logica,
        Concatenado

    }
    public class TipoOperaciones
    {
        public EnumTipoOperaciones ope { get; set; }
        public string dataValue { get; set; }
        public string dataText { get; set; }

        public TipoOperaciones(EnumTipoOperaciones pOpe, string pCod, string pTxt)
        {
            this.ope = pOpe;
            this.dataValue = pCod;
            this.dataText = pTxt;
        }
    }
    public class Operaciones : List<TipoOperaciones>
    {
        public const string cnsConcat = "[&]";
        public Operaciones()
        {
            /*
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Aritmetica, "+", "[+]"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Aritmetica, "-", "[-]"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Aritmetica, "*", "[*]"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Aritmetica, "/", "[/]"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Aritmetica, "=", "[=]"));

            this.Add(new TipoOperaciones(EnumTipoOperaciones.Logica, "Inner Join", "[Union]"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Logica, "if", "[si o si ]"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Logica, "case", "[caso]"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Logica, "AND", "[Y]"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Logica, "OR", "[O]"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Logica, "LIKE", "[Buscar]"));

            this.Add(new TipoOperaciones(EnumTipoOperaciones.Concatenado, "comillaSimple", "[']"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Concatenado, "comillaDoble", "['']"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Concatenado, "Concatenar", "[Concat]"));
            */

            this.Add(new TipoOperaciones(EnumTipoOperaciones.Aritmetica, "[+]", "SUMAR"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Aritmetica, "[-]", "RESTAR"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Aritmetica, "[*]", "MULTIPLICAR"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Aritmetica, "[/]", "DIVIDIR"));

            this.Add(new TipoOperaciones(EnumTipoOperaciones.Logica, "[IF]", "IF"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Logica, "[AND]", "AND"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Logica, "[OR]", "OR"));
            this.Add(new TipoOperaciones(EnumTipoOperaciones.Concatenado, cnsConcat, "CONCATENAR"));
        }
    }

    public class Operacion
    {
        public string dataValue { get; set; }
        public string dataText { get; set; }

        public List<Operacion> ListarOperaciones()
        {
            List<Operacion> lista = new List<Operacion>();

            Enum.GetValues(typeof(EnumTipoOperaciones)).Cast<EnumTipoOperaciones>().ToList().ForEach(obj =>
            {
                Operacion Operacion = new Operacion {   dataValue = obj.GetHashCode().ToString(),
                                                        dataText = obj.ToString()
                                                    };
                lista.Add(Operacion);
            });


            return lista;
        }

        public List<TipoOperaciones> ListarTipoOperaciones(int id)
        {
            List<TipoOperaciones> lista = new List<TipoOperaciones>();
            lista = new Operaciones().Where(x => x.ope.GetHashCode() == id).ToList();
            return lista;
        }
    }

}