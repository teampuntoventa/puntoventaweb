﻿using Ionic.Zip;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using HtmlAgilityPack;
using System.Data;

namespace DCPV.Web.Models.Utils
{
    public static class Util
    {
        private static string mst_error = string.Empty;

        public static string SaveFileBinary(string Path, string FileName, byte[] pData)
        {
            bool booRslt = false;
            string PathFilename = Path + FileName;
            BinaryWriter Writer = null;
            try
            {
                Writer = new BinaryWriter(File.OpenWrite(PathFilename));
                Writer.Write(pData);
                Writer.Flush();
                Writer.Close();
                booRslt = true;
            }
            catch (System.Exception ex)
            {
                mst_error = ex.Message.ToString();
                booRslt = false;
            }

            if (!booRslt) PathFilename = string.Empty;
            return PathFilename;
        }

        public static byte[] BinaryFile(string filename)
        {
            FileStream fs = new FileStream(filename, FileMode.Open, FileAccess.Read);
            byte[] ImageData = new byte[fs.Length];
            fs.Read(ImageData, 0, System.Convert.ToInt32(fs.Length));
            fs.Close();
            return ImageData;
        }

        public static bool ComprimirFolder(string PathFile, string PathZip, string nameFile)
        {
            string pathFileZip = HttpContext.Current.Server.MapPath(PathZip + nameFile + ".zip");
            try
            {
                using (ZipFile zip = new ZipFile())
                {
                    zip.AddFile(PathFile, "");
                    zip.Save(pathFileZip);
                }
                File.Delete(PathFile);
                return true;
            }
            catch (Exception ex)
            {
                mst_error = ex.Message.ToString();
                return false;

            }

        }

        public static string getTextFromFile(string pPathFile)
        {
            string sQry = string.Empty;
            try
            {
                sQry = System.IO.File.ReadAllText(pPathFile);
            }
            catch (Exception ex)
            {
                sQry = string.Empty;
                throw ex;
            }
            return sQry;
        }

        public static string removeDiacritics(this string text)
        {
            if (text == null) return null;
            var chars =
                from c in text.Normalize(NormalizationForm.FormD).ToCharArray()
                let uc = CharUnicodeInfo.GetUnicodeCategory(c)
                where uc != UnicodeCategory.NonSpacingMark
                select c;
            var cleanStr = new string(chars.ToArray()).Normalize(NormalizationForm.FormC);
            cleanStr = cleanStr.ToUpper();
            cleanStr = cleanStr.Trim();
            cleanStr = cleanStr.Replace(".", "");
            cleanStr = cleanStr.Replace("-", "");
            cleanStr = cleanStr.Replace("_", "");
            cleanStr = cleanStr.Replace(" ", string.Empty);

            return cleanStr;

        }

        public static int getTotalCheckLot(string routeFileCheck, int lineSearch)
        {
            int totalChecks = 0;
            try
            {
                if (System.IO.File.Exists(routeFileCheck))
                {
                    string[] lines = System.IO.File.ReadAllLines(routeFileCheck);
                    totalChecks = int.Parse(lines[lineSearch]);
                }
                else
                {
                    System.IO.File.Create(routeFileCheck).Close();
                    IOHelper.writeStringFolder(routeFileCheck, "0", 0);
                }
            }
            catch (Exception)
            {//Se atrapa la excepción en caso no poder el valor y por lo tanto el número de checks sera 0
            }
            return totalChecks;
        }

        public static string getPdfToDigitize(string[] pdfFiles)
        {
            string result = string.Empty;
            foreach (string strPdf in pdfFiles)
            {
                string strWithoutExt = strPdf.ToLower().Replace(Configuration.EXT_PDF, string.Empty);
                int number;
                bool isNumeric = int.TryParse(strWithoutExt, out number);
                if (isNumeric)
                {
                    result = number.ToString();
                    break;
                }
            }
            return result;
        }

        public static void compressFile(string filePath, string newFile, bool deleted)
        {
            //newFile = newFile.Replace(ResourceIOHelper.slash, ResourceIOHelper.minus);

            using (PdfReader reader = new PdfReader((string)filePath))
            {
                using (PdfStamper stamper = new PdfStamper(reader, new FileStream(newFile, FileMode.Create), PdfWriter.VERSION_1_5))
                {
                    stamper.Writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_5);
                    stamper.Writer.CompressionLevel = PdfStream.BEST_SPEED;

                    stamper.FormFlattening = true;

                    int total = reader.NumberOfPages + 1;
                    for (int i = 1; i < total; i++)
                    {
                        reader.SetPageContent(1, reader.GetPageContent(1), PdfStream.BEST_SPEED);
                    }
                    stamper.SetFullCompression();
                    stamper.Close();
                }
                reader.Close();
            }

            System.IO.File.SetAttributes(filePath, FileAttributes.Normal);
            if (deleted == true) System.IO.File.Delete(filePath);
        }

        public static bool CombineMultiplePDFs(string[] fileNames, string rootPdf)
        {
            Boolean result = false;
            int pageOffset = 0;
            int position = 0;
            Document document = null;
            PdfCopy writer = null;

            while (position < fileNames.Length)
            {
                using (PdfReader reader = new PdfReader(fileNames[position]))
                {
                    reader.ConsolidateNamedDestinations();
                    int numberPages = reader.NumberOfPages;
                    pageOffset += numberPages;

                    if (position == 0)
                    {
                        document = new Document(reader.GetPageSizeWithRotation(1));
                        FileStream tempFileStream = new FileStream(rootPdf, FileMode.Create);
                        writer = new PdfCopy(document, tempFileStream);
                        document.Open();
                        tempFileStream.Flush();
                    }
                    for (int i = 0; i < numberPages;)
                    {
                        ++i;
                        if (writer != null)
                        {
                            PdfImportedPage page = writer.GetImportedPage(reader, i);
                            writer.AddPage(page);
                        }
                    }
                    PRAcroForm form = reader.AcroForm;
                    if (form != null && writer != null)
                    {
                        writer.Close();
                    }
                    position++;
                }
            }
            if (document != null)
            {
                document.Close();
            }
            System.IO.File.Delete(fileNames[0]);
            System.IO.File.Delete(fileNames[1]);
            System.IO.File.Move(rootPdf, fileNames[0]);
            result = true;

            return result;
        }

        public static int SplitAndSave(string inputPath)
        {
            int numberOfPages = 0;
            FileInfo file = new FileInfo(inputPath);
            string name = file.Name.Substring(0, file.Name.LastIndexOf("."));
            string fileOutput = file.FullName.Substring(0, file.FullName.LastIndexOf("."));

            /*
            string      miRuta          = file.FullName.Substring(0, file.FullName.LastIndexOf(@"\") + 1);
            string      miArchivo       = file.FullName.Substring(file.FullName.LastIndexOf(@"\") + 1);
            */
            try
            {
                using (PdfReader reader = new PdfReader(inputPath))
                {
                    for (int pagenumber = 1; pagenumber <= reader.NumberOfPages; pagenumber++)
                    {
                        string filename = pagenumber.ToString() + ".pdf";
                        Document document = new Document();
                        FileStream _fileStream = new FileStream(fileOutput + filename, FileMode.Create);
                        PdfCopy copy = new PdfCopy(document, _fileStream);
                        document.Open();
                        copy.AddPage(copy.GetImportedPage(reader, pagenumber));
                        document.Close();
                    }
                    numberOfPages = reader.NumberOfPages;
                }
            }
            catch
            {
                //se cae porque el archivo pdf esta dañado por una mala tipificacion.
            }

            try
            {
                file.Delete();
            }
            catch (Exception)
            {
                return 0;
            }

            return numberOfPages;

        }

        public static string TipoCambioListarSunat(string mes, string anio, string día)
        {
            try
            {

                var sUrl = "http://www.sunat.gob.pe/cl-at-ittipcam/tcS01Alias?mes=" + mes + "&anho=" + anio;

                Encoding objEncoding = Encoding.GetEncoding("ISO-8859-1");
                CookieCollection objCookies = new CookieCollection();

                HttpWebRequest getRequest = (HttpWebRequest)WebRequest.Create(sUrl);
                getRequest.Credentials = CredentialCache.DefaultNetworkCredentials;
                getRequest.ProtocolVersion = HttpVersion.Version11;
                getRequest.UserAgent = ".NET Framework 4.0";
                getRequest.Method = "GET";

                getRequest.CookieContainer = new CookieContainer();
                getRequest.CookieContainer.Add(objCookies);


                string sGetResponse = string.Empty;

                using (HttpWebResponse getResponse = (HttpWebResponse)getRequest.GetResponse())
                {
                    objCookies = getResponse.Cookies;

                    using (StreamReader srGetResponse = new StreamReader(getResponse.GetResponseStream(), objEncoding))
                    {
                        sGetResponse = srGetResponse.ReadToEnd();
                    }
                }
                //Obtenemos Informacion
                HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                document.LoadHtml(sGetResponse);

                HtmlNodeCollection NodesTr = document.DocumentNode.SelectNodes("//table[@class='class=\"form-table\"']//tr");
                if (NodesTr != null)
                {
                    var dt = new DataTable();
                    dt.Columns.Add("dia", typeof(int));
                    dt.Columns.Add("compra", typeof(string));
                    dt.Columns.Add("venta", typeof(string));

                    int iNumFila = 0;
                    foreach (HtmlNode Node in NodesTr)
                    {
                        if (iNumFila > 0)
                        {
                            int iNumColumna = 0;
                            DataRow dr = dt.NewRow();
                            foreach (HtmlNode subNode in Node.Elements("td"))
                            {

                                if (iNumColumna == 0) dr = dt.NewRow();

                                string sValue = subNode.InnerHtml.ToString().Trim();
                                sValue = System.Text.RegularExpressions.Regex.Replace(sValue, "<.*?>", " ");
                                dr[iNumColumna] = sValue;

                                iNumColumna++;

                                if (iNumColumna == 3)
                                {
                                    dt.Rows.Add(dr);
                                    iNumColumna = 0;
                                }
                            }
                        }
                        iNumFila++;
                    }
                    var rowFind = dt.AsEnumerable().ToList().Find(item => item.Field<int>("dia").Equals(int.Parse(día)));
                    if (rowFind != null)
                    {
                        string venta = rowFind.Field<string>("venta");
                        return venta;
                    }

                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return "0.00";
        }

        public static List<string> GetProveedorPost(string nro)
        {
            List<string> dt = new List<string>() ;

            try
            {
                var sUrl = "http://www.razonsocialperu.com/empresa/detalle/" + nro;
                Encoding objEncoding = Encoding.GetEncoding("ISO-8859-1");
                CookieCollection objCookies = new CookieCollection();

                HttpWebRequest getRequest = (HttpWebRequest)WebRequest.Create(sUrl);
                getRequest.Credentials = CredentialCache.DefaultNetworkCredentials;
                getRequest.ProtocolVersion = HttpVersion.Version11;
                getRequest.UserAgent = ".NET Framework 4.0";
                getRequest.Method = "GET";

                getRequest.CookieContainer = new CookieContainer();
                getRequest.CookieContainer.Add(objCookies);


                string sGetResponse = string.Empty;

                using (HttpWebResponse getResponse = (HttpWebResponse)getRequest.GetResponse())
                {
                    objCookies = getResponse.Cookies;

                    using (StreamReader srGetResponse = new StreamReader(getResponse.GetResponseStream(), objEncoding))
                    {
                        sGetResponse = srGetResponse.ReadToEnd();
                    }
                }
              
                HtmlAgilityPack.HtmlDocument document = new HtmlAgilityPack.HtmlDocument();
                document.LoadHtml(sGetResponse);
                HtmlNodeCollection NodesTr = document.DocumentNode.SelectNodes("//ul[@class='iconlist-color clearfix']//li");
                if (NodesTr != null)
                {
                    foreach (HtmlNode Node in NodesTr)
                    {
                        string sValue           = Node.InnerText.ToString().Trim();
                        sValue                  = System.Text.RegularExpressions.Regex.Replace(sValue, "<.*?>", " ");
                        string valueReplace     = sValue.Substring(sValue.LastIndexOf(":")+1);

                        dt.Add(valueReplace.Trim());
                    }
                }
            }
            catch (Exception)
            {
                dt = new List<string>();
              //  throw new Exception(ex.Message);
            }
            return dt;
        }
    }

    public class NumericComparer : IComparer
    {
        public int Compare(object x, object y)
        {
            if ((x is string) && (y is string))
            {
                return StringLogicalComparer.Compare((string)x, (string)y);
            }
            return -1;
        }
    }

    public class StringLogicalComparer
    {
        public static int Compare(string s1, string s2)
        {
            if ((s1 == null) && (s2 == null)) return 0;
            else if (s1 == null) return -1;
            else if (s2 == null) return 1;

            if ((s1.Equals(string.Empty) && (s2.Equals(string.Empty)))) return 0;
            else if (s1.Equals(string.Empty)) return -1;
            else if (s2.Equals(string.Empty)) return -1;
            bool sp1 = Char.IsLetterOrDigit(s1, 0);
            bool sp2 = Char.IsLetterOrDigit(s2, 0);
            if (sp1 && !sp2) return 1;
            if (!sp1 && sp2) return -1;

            int i1 = 0, i2 = 0;
            int r = 0;
            while (true)
            {
                bool c1 = Char.IsDigit(s1, i1);
                bool c2 = Char.IsDigit(s2, i2);
                if (!c1 && !c2)
                {
                    bool letter1 = Char.IsLetter(s1, i1);
                    bool letter2 = Char.IsLetter(s2, i2);
                    if ((letter1 && letter2) || (!letter1 && !letter2))
                    {
                        if (letter1 && letter2)
                        {
                            r = Char.ToLower(s1[i1]).CompareTo(Char.ToLower(s2[i2]));
                        }
                        else
                        {
                            r = s1[i1].CompareTo(s2[i2]);
                        }
                        if (r != 0) return r;
                    }
                    else if (!letter1 && letter2) return -1;
                    else if (letter1 && !letter2) return 1;
                }
                else if (c1 && c2)
                {
                    r = CompareNum(s1, ref i1, s2, ref i2);
                    if (r != 0) return r;
                }
                else if (c1)
                {
                    return -1;
                }
                else if (c2)
                {
                    return 1;
                }
                i1++;
                i2++;
                if ((i1 >= s1.Length) && (i2 >= s2.Length))
                {
                    return 0;
                }
                else if (i1 >= s1.Length)
                {
                    return -1;
                }
                else if (i2 >= s2.Length)
                {
                    return -1;
                }
            }
        }

        private static int CompareNum(string s1, ref int i1, string s2, ref int i2)
        {
            int nzStart1 = i1, nzStart2 = i2;
            int end1 = i1, end2 = i2;

            ScanNumEnd(s1, i1, ref end1, ref nzStart1);
            ScanNumEnd(s2, i2, ref end2, ref nzStart2);
            int start1 = i1; i1 = end1 - 1;
            int start2 = i2; i2 = end2 - 1;

            int nzLength1 = end1 - nzStart1;
            int nzLength2 = end2 - nzStart2;

            if (nzLength1 < nzLength2) return -1;
            else if (nzLength1 > nzLength2) return 1;

            for (int j1 = nzStart1, j2 = nzStart2; j1 <= i1; j1++, j2++)
            {
                int r = s1[j1].CompareTo(s2[j2]);
                if (r != 0) return r;
            }

            int length1 = end1 - start1;
            int length2 = end2 - start2;
            if (length1 == length2) return 0;
            if (length1 > length2) return -1;
            return 1;
        }

        private static void ScanNumEnd(string s, int start, ref int end, ref int nzStart)
        {
            nzStart = start;
            end = start;
            bool countZeros = true;
            while (Char.IsDigit(s, end))
            {
                if (countZeros && s[end].Equals('0'))
                {
                    nzStart++;
                }
                else countZeros = false;
                end++;
                if (end >= s.Length) break;
            }
        }
    }
}