﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCPV.Web.Models.Utils
{
    public static class IOHelper
    {
        public static void writeStringFolder(string pathFile, string text, int position)
        {
            string[] lines = System.IO.File.ReadAllLines(pathFile);
            if (lines == null || lines.Length == 0 || lines.Equals(string.Empty))
            {
                lines = new string[3];
                lines[0] = text;
                lines[1] = text;
                lines[2] = string.Empty;
            }
            else
            {
                lines[position] = text;
            }
            System.IO.File.WriteAllLines(pathFile, lines);
        }
    }
}