﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DCPV.Web.Models.Utils
{
    public class ErrorLog
    {
        public static void RegistraLogErrors(string msterror_one, string msterror_two , string msterror_Four = "")
        {
            subir:
            string xRuta                                = System.Web.HttpContext.Current.Server.MapPath("~");
            string xArchivo                             = @"notepad.log";
            string msterror_three                       = DateTime.Now.ToShortDateString();
            string Path                                 = string.Concat( xRuta.ToString() , xArchivo.ToString());

            if (!System.IO.File.Exists(Path))
            {
                string pathString                       = xRuta.ToString();
                System.IO.Directory.CreateDirectory(pathString);
                pathString                              = pathString + xArchivo;
                System.IO.StreamWriter archivo          = System.IO.File.AppendText(pathString);
                archivo.Close();
                goto subir;
            }

            System.IO.File.SetAttributes(Path, System.IO.FileAttributes.Normal);
            System.IO.StreamWriter error                = System.IO.File.AppendText(Path);
            error.WriteLine("---------------------------------------------------------------------------------");
            error.WriteLine("error one : " + msterror_one);
            error.WriteLine("error two : " + msterror_two);
            error.WriteLine("error three : " + msterror_three);
            if(!string.IsNullOrEmpty(msterror_Four)) error.WriteLine("error Line : " + msterror_Four);

            error.WriteLine("---------------------------------------------------------------------------------");
            error.Close();
            error.Dispose();
        }
    }
}