﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloPeriod
    {
        CResult add(Period p);
        CResult upd(Period p);
        CResult del(Period p);
        IList<Period> lis(Period p);
        Period sel(Period p);
    }
}
