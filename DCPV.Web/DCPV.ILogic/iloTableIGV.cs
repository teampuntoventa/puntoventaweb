﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloTableIGV
    {
        CResult add(TableIGV p);
        CResult upd(TableIGV p);
        CResult del(TableIGV p);
        IList<TableIGV> lis(TableIGV p);
        TableIGV sel(TableIGV p);
    }
}
