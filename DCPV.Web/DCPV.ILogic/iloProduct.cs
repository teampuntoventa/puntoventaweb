﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloProduct
    {
        CResult add(Product p);
        CResult upd(Product p);
        CResult del(Product p);
        IList<Product> lis(Product p);
        Product sel(Product p);
    }
}
