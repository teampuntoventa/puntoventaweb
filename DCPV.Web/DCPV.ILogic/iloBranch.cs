﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloBranch
    {
        CResult add(Branch p);
        CResult upd(Branch p);
        CResult del(Branch p);
        IList<Branch> lis(Branch p);
        Branch sel(Branch p);
    }
}
