﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloBusinessPartner
    {
        CResult add(BusinessPartner p);
        CResult upd(BusinessPartner p);
        CResult del(BusinessPartner p);
        IList<BusinessPartner> lis(BusinessPartner p);
        BusinessPartner sel(BusinessPartner p);
    }
}
