﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloMovement
    {
        CResult add(Movement p);
        CResult upd(Movement p);
        CResult del(Movement p);
        IList<Movement> lis(Movement p);
        Movement sel(Movement p);
    }
}
