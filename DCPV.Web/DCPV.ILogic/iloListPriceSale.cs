﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloListPriceSale
    {
        CResult add(ListPriceSale p);
        CResult upd(ListPriceSale p);
        CResult del(ListPriceSale p);
        IList<ListPriceSale> lis(ListPriceSale p);
        ListPriceSale sel(ListPriceSale p);
    }
}
