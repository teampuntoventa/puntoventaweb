﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloDocumentObject
    {
        CResult add(DocumentObject p);
        CResult upd(DocumentObject p);
        CResult del(DocumentObject p);
        IList<DocumentObject> lis(DocumentObject p);
        DocumentObject sel(DocumentObject p);
    }
}
