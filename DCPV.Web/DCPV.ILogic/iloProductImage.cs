﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloProductImage
    {
        CResult add(ProductImage p);
        CResult upd(ProductImage p);
        CResult del(ProductImage p);
        IList<ProductImage> lis(ProductImage p);
        ProductImage sel(ProductImage p);
    }
}
