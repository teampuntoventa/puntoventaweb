﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloConfig
    {
        CResult add(Config p);
        CResult upd(Config p);
        CResult del(Config p);
        IList<Config> lis(Config p);
        Config sel(Config p);
    }
}
