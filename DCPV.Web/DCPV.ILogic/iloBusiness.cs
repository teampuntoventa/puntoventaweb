﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloBusiness
    {
        CResult add(Business p);
        CResult upd(Business p);
        CResult del(Business p);
        IList<Business> lis(Business p);
        Business sel(Business p);
    }
}
