﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloDocumentType
    {
        CResult add(DocumentType p);
        CResult upd(DocumentType p);
        CResult del(DocumentType p);
        IList<DocumentType> lis(DocumentType p);
        DocumentType sel(DocumentType p);
    }
}
