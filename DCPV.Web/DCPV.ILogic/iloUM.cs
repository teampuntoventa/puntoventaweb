﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloUM
    {
        CResult add(UM p);
        CResult upd(UM p);
        CResult del(UM p);
        IList<UM> lis(UM p);
        UM sel(UM p);
    }
}
