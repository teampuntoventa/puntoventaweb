﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloProductType
    {
        CResult add(ProductType p);
        CResult upd(ProductType p);
        CResult del(ProductType p);
        IList<ProductType> lis(ProductType p);
        ProductType sel(ProductType p);
    }
}
