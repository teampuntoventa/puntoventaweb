﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloMovementDetail
    {
        CResult add(MovementDetail p);
        CResult upd(MovementDetail p);
        CResult del(MovementDetail p);
        IList<MovementDetail> lis(MovementDetail p);
        MovementDetail sel(MovementDetail p);
    }
}
