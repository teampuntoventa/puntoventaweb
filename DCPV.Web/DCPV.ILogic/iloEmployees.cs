﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloEmployees
    {
        CResult add(Employees p);
        CResult upd(Employees p);
        CResult del(Employees p);
        IList<Employees> lis(Employees p);
        Employees sel(Employees p);
    }
}
