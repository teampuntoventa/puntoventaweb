﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloMovementType
    {
        CResult add(MovementType p);
        CResult upd(MovementType p);
        CResult del(MovementType p);
        IList<MovementType> lis(MovementType p);
        MovementType sel(MovementType p);
    }
}
