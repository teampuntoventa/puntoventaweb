﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloSubProductLine
    {
        CResult add(SubProductLine p);
        CResult upd(SubProductLine p);
        CResult del(SubProductLine p);
        IList<SubProductLine> lis(SubProductLine p);
        SubProductLine sel(SubProductLine p);
    }
}
