﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloProductLine
    {
        CResult add(ProductLine p);
        CResult upd(ProductLine p);
        CResult del(ProductLine p);
        IList<ProductLine> lis(ProductLine p);
        ProductLine sel(ProductLine p);
    }
}
