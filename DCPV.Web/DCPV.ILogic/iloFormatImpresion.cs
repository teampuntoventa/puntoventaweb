﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloFormatImpresion
    {
        CResult add(FormatImpresion p);
        CResult upd(FormatImpresion p);
        CResult del(FormatImpresion p);
        IList<FormatImpresion> lis(FormatImpresion p);
        FormatImpresion sel(FormatImpresion p);
    }
}
