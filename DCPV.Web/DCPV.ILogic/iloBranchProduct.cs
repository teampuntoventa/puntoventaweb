﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloBranchProduct
    {
        CResult add(BranchProduct p);
        CResult upd(BranchProduct p);
        CResult del(BranchProduct p);
        IList<BranchProduct> lis(BranchProduct p);
        BranchProduct sel(BranchProduct p);
    }
}
