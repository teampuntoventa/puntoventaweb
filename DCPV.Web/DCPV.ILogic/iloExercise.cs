﻿using DCPV.Entity;
using System.Collections.Generic;

namespace DCPV.ILogic
{
    public interface iloExercise
    {
        CResult add(Exercise p);
        CResult upd(Exercise p);
        CResult del(Exercise p);
        IList<Exercise> lis(Exercise p);
        Exercise sel(Exercise p);
    }
}
