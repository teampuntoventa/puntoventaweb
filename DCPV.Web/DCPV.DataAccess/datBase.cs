﻿using System;

namespace DCPV.DataAccess
{
    public class datBase : IDisposable
    {
        public static entDataBase database = null;

        public datBase(entDataBase pDb = null)
        {
            try
            {
                database = pDb;
                bool booCnx = false;

                if (database != null)
                {
                    Dispose();
                    booCnx = datCnx.Iniciar(database.SrvProvider,
                                            database.SrvHostIP,
                                            database.DbaName,
                                            database.SrvUser,
                                            database.SrvPassword,
                                            database.SrvPort.ToString(),//add lennyn
                                            database.PathFile

                                            );
                }
                else
                {
                    booCnx = datCnx.Iniciar();
                }

                if (!booCnx)
                {
                    Dispose();
                    throw new Exception("Error al Incicar conexión a la Base de Datos. " +
                                        Environment.NewLine + "[datBase]");
                }

            }
            catch (Exception ex) { throw (ex); }
        }

        ~datBase()
        {
            try { Dispose(); }
            catch (Exception ex) { throw ex; }
        }

        public void Dispose()
        {
            try
            {
                datCnx.Finalizar();
                GC.SuppressFinalize(this);
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
