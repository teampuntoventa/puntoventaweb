﻿
using System.Runtime.Serialization;

namespace DCPV.DataAccess
{
    [DataContract]
    public class entServer
    {
        #region Variables
        private int srvId;
        private int srvProvider;
        private string srvHostName;
        private string srvHostIP;
        private int srvPort;
        private string srvUser;
        private string srvPassword;
        private bool srvActive;
        private string proName;
        private string pathFile;
        #endregion

        #region Propiedades
        [DataMember(Name = "srvId", Order = 1)]
        public int SrvId
        {
            get { return this.srvId; }
            set { this.srvId = value; }
        }
        [DataMember(Name = "srvProvider", Order = 2)]
        public int SrvProvider
        {
            get { return this.srvProvider; }
            set { this.srvProvider = value; }
        }
        [DataMember(Name = "srvHostName", Order = 3)]
        public string SrvHostName
        {
            get { return this.srvHostName; }
            set { this.srvHostName = value; }
        }
        [DataMember(Name = "srvHostIP", Order = 4)]
        public string SrvHostIP
        {
            get { return this.srvHostIP; }
            set { this.srvHostIP = value; }
        }
        [DataMember(Name = "srvPort", Order = 5)]
        public int SrvPort
        {
            get { return this.srvPort; }
            set { this.srvPort = value; }
        }
        [DataMember(Name = "srvUser", Order = 6)]
        public string SrvUser
        {
            get { return this.srvUser; }
            set { this.srvUser = value; }
        }
        [DataMember(Name = "srvPassword", Order = 7)]
        public string SrvPassword
        {
            get { return this.srvPassword; }
            set { this.srvPassword = value; }
        }
        [DataMember(Name = "srvActive", Order = 8)]
        public bool SrvActive
        {
            get { return this.srvActive; }
            set { this.srvActive = value; }
        }

        [DataMember(Name = "proName", Order = 9)]
        public string ProName
        {
            get { return this.proName; }
            set { this.proName = value; }
        }

        [DataMember(Name = "pathFile", Order = 10)]
        public string PathFile
        {
            get { return this.pathFile; }
            set { this.pathFile = value; }
        }
        #endregion

        #region Constructor
        public entServer()
        {
            this.srvId = 0;
            this.srvProvider = 0;
            this.srvHostName = string.Empty;
            this.srvHostIP = string.Empty;
            this.srvPort = 0;
            this.srvUser = string.Empty;
            this.srvPassword = string.Empty;
            this.srvActive = true;
            this.proName = string.Empty;
            this.pathFile = string.Empty;
        }
        #endregion
    }
}
