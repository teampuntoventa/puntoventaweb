﻿


namespace DCPV.DataAccess
{
    public class entDataBase : entServer
    {
        #region Variables
        //private int dbaId;
        //private int proId;
        //private string dbaHostName;
        //private string dbaHostIP;
        //private int dbaPort;
        //private string dbaUser;
        //private string dbaPassword;
        private string dbaName;
        //private bool dbaActive;

        //private string proName;
        #endregion

        #region Propiedades
        //[DataMember(Name = "dbaId", Order = 1)]
        //public int DbaId
        //{
        //    get { return this.dbaId; }
        //    set { this.dbaId = value; }
        //}
        //[DataMember(Name = "proId", Order = 2)]
        //public int ProId
        //{
        //    get { return this.proId; }
        //    set { this.proId = value; }
        //}
        //[DataMember(Name = "dbaHostName", Order = 3)]
        //public string DbaHostName
        //{
        //    get { return this.dbaHostName; }
        //    set { this.dbaHostName = value; }
        //}
        //[DataMember(Name = "dbaHostIP", Order = 4)]
        //public string DbaHostIP
        //{
        //    get { return this.dbaHostIP; }
        //    set { this.dbaHostIP = value; }
        //}
        //[DataMember(Name = "dbaPort", Order = 5)]
        //public int DbaPort
        //{
        //    get { return this.dbaPort; }
        //    set { this.dbaPort = value; }
        //}
        //[DataMember(Name = "dbaUser", Order = 6)]
        //public string DbaUser
        //{
        //    get { return this.dbaUser; }
        //    set { this.dbaUser = value; }
        //}
        //[DataMember(Name = "dbaPassword", Order = 7)]
        //public string DbaPassword
        //{
        //    get { return this.dbaPassword; }
        //    set { this.dbaPassword = value; }
        //}
        //[DataMember(Name = "dbaName", Order = 8)]
        public string DbaName
        {
            get { return this.dbaName; }
            set { this.dbaName = value; }
        }
        //[DataMember(Name = "dbaActive", Order = 9)]
        //public bool DbaActive
        //{
        //    get { return this.dbaActive; }
        //    set { this.dbaActive = value; }
        //}

        //[DataMember(Name = "proName", Order = 10)]
        //public string ProName
        //{
        //    get { return this.proName; }
        //    set { this.proName = value; }
        //}
        #endregion

        #region Constructor
        public entDataBase()
        {
            this.dbaName = string.Empty;
        }
        public entDataBase(entServer pSrv)
        {
            this.SrvId = pSrv.SrvId;
            this.SrvProvider = pSrv.SrvProvider;
            this.SrvHostName = pSrv.SrvHostName;
            this.SrvHostIP = pSrv.SrvHostIP;
            this.SrvPort = pSrv.SrvPort;
            this.SrvUser = pSrv.SrvUser;
            this.SrvPassword = pSrv.SrvPassword;
            this.PathFile = pSrv.PathFile;//add lennyn
        }
        #endregion
    }
}
