﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datListPriceSale : datBase
    {
        public int add(ListPriceSale p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSListPriceSale", p.BsnId, p.PrdId, p.LstPrcSlNum, p.LstPrcSlVnt, p.objType, p.LstPrcSlId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(ListPriceSale p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDListPriceSale", p.LstPrcSlId, p.BsnId, p.PrdId, p.LstPrcSlNum, p.LstPrcSlVnt, p.objType) > 0 ? p.LstPrcSlId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(ListPriceSale p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELListPriceSale", p.LstPrcSlId) > 0 ? p.LstPrcSlId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<ListPriceSale> lis(ListPriceSale p)
        {
            IList<ListPriceSale> oList = new List<ListPriceSale>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISListPriceSale"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorListPriceSale(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<ListPriceSale>();
                throw ex;
            }
            return oList;
        }
        public ListPriceSale sel(ListPriceSale p)
        {
            ListPriceSale oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSListPriceSale", p.LstPrcSlId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorListPriceSale(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new ListPriceSale();
                throw ex;
            }
            return oEnt;
        }
        public ListPriceSale ctorListPriceSale(SqlDataReader sqlreader)
        {
            ListPriceSale oEnt = new ListPriceSale();
            int indice;

            if (sqlreader.TryGetOrdinal("LstPrcSlId", out indice))
                oEnt.LstPrcSlId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.LstPrcSlId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("PrdId", out indice))
                oEnt.PrdId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdId = 0;
            if (sqlreader.TryGetOrdinal("LstPrcSlNum", out indice))
                oEnt.LstPrcSlNum = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.LstPrcSlNum = 0;
            if (sqlreader.TryGetOrdinal("LstPrcSlVnt", out indice))
                oEnt.LstPrcSlVnt = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.LstPrcSlVnt = 0;
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
