﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datMovement : datBase
    {
        public int add(Movement p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSMovement", p.BsnId, p.ExrId, p.PerdId, p.BsnPrtId, p.MvmDocnum, p.MvnDocStatus, p.DcmObjId, p.MvnDocType, p.MvnDocCur, p.MvnDocRate, p.MvnComments, p.MvnJrnlDiario, p.EplId, p.MvnDocDate, p.MvnTipoDoc, p.MvnSerieDoc, p.MvnCorrelativo, p.MvnBaseTipoDoc, p.MvnBaseSerieDoc, p.MvnBaseCorrelativo, p.MvnSubTotal, p.MvnIGV, p.MvnOtros, p.MvnTotal, p.objType, p.MvnId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(Movement p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDMovement", p.MvnId, p.BsnId, p.ExrId, p.PerdId, p.BsnPrtId, p.MvmDocnum, p.MvnDocStatus, p.DcmObjId, p.MvnDocType, p.MvnDocCur, p.MvnDocRate, p.MvnComments, p.MvnJrnlDiario, p.EplId, p.MvnDocDate, p.MvnTipoDoc, p.MvnSerieDoc, p.MvnCorrelativo, p.MvnBaseTipoDoc, p.MvnBaseSerieDoc, p.MvnBaseCorrelativo, p.MvnSubTotal, p.MvnIGV, p.MvnOtros, p.MvnTotal, p.objType) > 0 ? p.MvnId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(Movement p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELMovement", p.MvnId) > 0 ? p.MvnId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<Movement> lis(Movement p)
        {
            IList<Movement> oList = new List<Movement>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISMovement"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorMovement(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<Movement>();
                throw ex;
            }
            return oList;
        }
        public Movement sel(Movement p)
        {
            Movement oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSMovement", p.MvnId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorMovement(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new Movement();
                throw ex;
            }
            return oEnt;
        }
        public Movement ctorMovement(SqlDataReader sqlreader)
        {
            Movement oEnt = new Movement();
            int indice;

            if (sqlreader.TryGetOrdinal("MvnId", out indice))
                oEnt.MvnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.MvnId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("ExrId", out indice))
                oEnt.ExrId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.ExrId = 0;
            if (sqlreader.TryGetOrdinal("PerdId", out indice))
                oEnt.PerdId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PerdId = 0;
            if (sqlreader.TryGetOrdinal("BsnPrtId", out indice))
                oEnt.BsnPrtId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnPrtId = 0;
            if (sqlreader.TryGetOrdinal("MvmDocnum", out indice))
                oEnt.MvmDocnum = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvmDocnum = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnDocStatus", out indice))
                oEnt.MvnDocStatus = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnDocStatus = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("DcmObjId", out indice))
                oEnt.DcmObjId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.DcmObjId = 0;
            if (sqlreader.TryGetOrdinal("MvnDocType", out indice))
                oEnt.MvnDocType = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnDocType = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnDocCur", out indice))
                oEnt.MvnDocCur = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnDocCur = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnDocRate", out indice))
                oEnt.MvnDocRate = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnDocRate = 0;
            if (sqlreader.TryGetOrdinal("MvnComments", out indice))
                oEnt.MvnComments = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnComments = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnJrnlDiario", out indice))
                oEnt.MvnJrnlDiario = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnJrnlDiario = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("EplId", out indice))
                oEnt.EplId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.EplId = 0;
            if (sqlreader.TryGetOrdinal("MvnDocDate", out indice))
                oEnt.MvnDocDate = sqlreader.IsDBNull(indice) ? Convert.ToDateTime("01/01/1900") : sqlreader.GetDateTime(indice);
            else
                oEnt.MvnDocDate = Convert.ToDateTime("01/01/1900");
            if (sqlreader.TryGetOrdinal("MvnTipoDoc", out indice))
                oEnt.MvnTipoDoc = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnTipoDoc = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnSerieDoc", out indice))
                oEnt.MvnSerieDoc = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnSerieDoc = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnCorrelativo", out indice))
                oEnt.MvnCorrelativo = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnCorrelativo = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnBaseTipoDoc", out indice))
                oEnt.MvnBaseTipoDoc = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnBaseTipoDoc = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnBaseSerieDoc", out indice))
                oEnt.MvnBaseSerieDoc = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnBaseSerieDoc = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnBaseCorrelativo", out indice))
                oEnt.MvnBaseCorrelativo = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnBaseCorrelativo = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnSubTotal", out indice))
                oEnt.MvnSubTotal = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnSubTotal = 0;
            if (sqlreader.TryGetOrdinal("MvnIGV", out indice))
                oEnt.MvnIGV = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnIGV = 0;
            if (sqlreader.TryGetOrdinal("MvnOtros", out indice))
                oEnt.MvnOtros = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnOtros = 0;
            if (sqlreader.TryGetOrdinal("MvnTotal", out indice))
                oEnt.MvnTotal = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnTotal = 0;
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }
}
