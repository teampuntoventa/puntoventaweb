﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datEmployees : datBase
    {
        public int add(Employees p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSEmployees", p.BsnId, p.EplDescripcion, p.objType, p.EplId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(Employees p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDEmployees", p.EplId, p.BsnId, p.EplDescripcion, p.objType) > 0 ? p.EplId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(Employees p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELEmployees", p.EplId) > 0 ? p.EplId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<Employees> lis(Employees p)
        {
            IList<Employees> oList = new List<Employees>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISEmployees"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorEmployees(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<Employees>();
                throw ex;
            }
            return oList;
        }
        public Employees sel(Employees p)
        {
            Employees oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSEmployees", p.EplId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorEmployees(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new Employees();
                throw ex;
            }
            return oEnt;
        }
        public Employees ctorEmployees(SqlDataReader sqlreader)
        {
            Employees oEnt = new Employees();
            int indice;

            if (sqlreader.TryGetOrdinal("EplId", out indice))
                oEnt.EplId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.EplId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("EplDescripcion", out indice))
                oEnt.EplDescripcion = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.EplDescripcion = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
