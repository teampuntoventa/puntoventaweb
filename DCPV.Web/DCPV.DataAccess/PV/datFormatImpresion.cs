﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datFormatImpresion : datBase
    {
        public int add(FormatImpresion p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSFormatImpresion", p.FrmImpDescripction, p.FrmImpCampoLibre1, p.FrmImpCampoLibre2, p.FrmImpCampoLibre3, p.FrmImpCampoLibre4, p.objType, p.FrmImpId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(FormatImpresion p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDFormatImpresion", p.FrmImpId, p.FrmImpDescripction, p.FrmImpCampoLibre1, p.FrmImpCampoLibre2, p.FrmImpCampoLibre3, p.FrmImpCampoLibre4, p.objType) > 0 ? p.FrmImpId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(FormatImpresion p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELFormatImpresion", p.FrmImpId) > 0 ? p.FrmImpId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<FormatImpresion> lis(FormatImpresion p)
        {
            IList<FormatImpresion> oList = new List<FormatImpresion>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISFormatImpresion"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorFormatImpresion(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<FormatImpresion>();
                throw ex;
            }
            return oList;
        }
        public FormatImpresion sel(FormatImpresion p)
        {
            FormatImpresion oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSFormatImpresion", p.FrmImpId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorFormatImpresion(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new FormatImpresion();
                throw ex;
            }
            return oEnt;
        }
        public FormatImpresion ctorFormatImpresion(SqlDataReader sqlreader)
        {
            FormatImpresion oEnt = new FormatImpresion();
            int indice;

            if (sqlreader.TryGetOrdinal("FrmImpId", out indice))
                oEnt.FrmImpId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.FrmImpId = 0;
            if (sqlreader.TryGetOrdinal("FrmImpDescripction", out indice))
                oEnt.FrmImpDescripction = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.FrmImpDescripction = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("FrmImpCampoLibre1", out indice))
                oEnt.FrmImpCampoLibre1 = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.FrmImpCampoLibre1 = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("FrmImpCampoLibre2", out indice))
                oEnt.FrmImpCampoLibre2 = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.FrmImpCampoLibre2 = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("FrmImpCampoLibre3", out indice))
                oEnt.FrmImpCampoLibre3 = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.FrmImpCampoLibre3 = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("FrmImpCampoLibre4", out indice))
                oEnt.FrmImpCampoLibre4 = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.FrmImpCampoLibre4 = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
