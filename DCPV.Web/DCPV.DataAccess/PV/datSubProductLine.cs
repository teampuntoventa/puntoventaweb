﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datSubProductLine : datBase
    {
        public int add(SubProductLine p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSSubProductLine", p.BsnId, p.PrdTpid, p.PrdLnId, p.SbPrdLnDescription, p.objType, p.SbPrdLnId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(SubProductLine p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDSubProductLine", p.SbPrdLnId, p.BsnId, p.PrdTpid, p.PrdLnId, p.SbPrdLnDescription, p.objType) > 0 ? p.SbPrdLnId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(SubProductLine p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELSubProductLine", p.SbPrdLnId) > 0 ? p.SbPrdLnId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<SubProductLine> lis(SubProductLine p)
        {
            IList<SubProductLine> oList = new List<SubProductLine>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISSubProductLine"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorSubProductLine(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<SubProductLine>();
                throw ex;
            }
            return oList;
        }
        public SubProductLine sel(SubProductLine p)
        {
            SubProductLine oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSSubProductLine", p.SbPrdLnId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorSubProductLine(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new SubProductLine();
                throw ex;
            }
            return oEnt;
        }
        public SubProductLine ctorSubProductLine(SqlDataReader sqlreader)
        {
            SubProductLine oEnt = new SubProductLine();
            int indice;

            if (sqlreader.TryGetOrdinal("SbPrdLnId", out indice))
                oEnt.SbPrdLnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.SbPrdLnId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("PrdTpid", out indice))
                oEnt.PrdTpid = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdTpid = 0;
            if (sqlreader.TryGetOrdinal("PrdLnId", out indice))
                oEnt.PrdLnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdLnId = 0;
            if (sqlreader.TryGetOrdinal("SbPrdLnDescription", out indice))
                oEnt.SbPrdLnDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.SbPrdLnDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
