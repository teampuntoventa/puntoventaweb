﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datProduct : datBase
    {
        public int add(Product p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSProduct", p.BsnId, p.PrdTpid, p.PrdLnId, p.SbPrdLnId, p.UmId, p.CodeBar, p.PrdDescription, p.PrdImage, p.PrdCst, p.PrdVnt, p.PrdObser, p.objType, p.PrdId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(Product p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDProduct", p.PrdId, p.BsnId, p.PrdTpid, p.PrdLnId, p.SbPrdLnId, p.UmId, p.CodeBar, p.PrdDescription, p.PrdImage, p.PrdCst, p.PrdVnt, p.PrdObser, p.objType) > 0 ? p.PrdId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(Product p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELProduct", p.PrdId) > 0 ? p.PrdId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<Product> lis(Product p)
        {
            IList<Product> oList = new List<Product>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISProduct"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorProduct(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<Product>();
                throw ex;
            }
            return oList;
        }
        public Product sel(Product p)
        {
            Product oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSProduct", p.PrdId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorProduct(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new Product();
                throw ex;
            }
            return oEnt;
        }
        public Product ctorProduct(SqlDataReader sqlreader)
        {
            Product oEnt = new Product();
            int indice;

            if (sqlreader.TryGetOrdinal("PrdId", out indice))
                oEnt.PrdId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("PrdTpid", out indice))
                oEnt.PrdTpid = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdTpid = 0;
            if (sqlreader.TryGetOrdinal("PrdLnId", out indice))
                oEnt.PrdLnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdLnId = 0;
            if (sqlreader.TryGetOrdinal("SbPrdLnId", out indice))
                oEnt.SbPrdLnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.SbPrdLnId = 0;
            if (sqlreader.TryGetOrdinal("UmId", out indice))
                oEnt.UmId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.UmId = 0;
            if (sqlreader.TryGetOrdinal("CodeBar", out indice))
                oEnt.CodeBar = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.CodeBar = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("PrdDescription", out indice))
                oEnt.PrdDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.PrdDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("PrdImage", out indice))
                oEnt.PrdImage = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.PrdImage = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("PrdCst", out indice))
                oEnt.PrdCst = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.PrdCst = 0;
            if (sqlreader.TryGetOrdinal("PrdVnt", out indice))
                oEnt.PrdVnt = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.PrdVnt = 0;
            if (sqlreader.TryGetOrdinal("PrdObser", out indice))
                oEnt.PrdObser = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.PrdObser = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
