﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datBranch : datBase
    {
        public int add(Branch p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSBranch", p.BsnId, p.BrnDescription, p.BrnType, p.BrnRefId, p.BrnVen, p.BrnCom, p.BrnIng, p.BrnSal, p.BrnTrns, p.objType, p.BrnId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int upd(Branch p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDBranch", p.BrnId, p.BsnId, p.BrnDescription, p.BrnType, p.BrnRefId, p.BrnVen, p.BrnCom, p.BrnIng, p.BrnSal, p.BrnTrns, p.objType) > 0 ? p.BrnId : 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int del(Branch p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELBranch", p.BrnId) > 0 ? p.BrnId : 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IList<Branch> lis(Branch p)
        {
            IList<Branch> oList = new List<Branch>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISBranch"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorBranch(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oList;
        }
        public Branch sel(Branch p)
        {
            Branch oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSBranch", p.BrnId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorBranch(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return oEnt;
        }
        public Branch ctorBranch(SqlDataReader sqlreader)
        {
            Branch oEnt = new Branch();
            int indice;

            if (sqlreader.TryGetOrdinal("BrnId", out indice))
                oEnt.BrnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("BrnDescription", out indice))
                oEnt.BrnDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BrnDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BrnType", out indice))
                oEnt.BrnType = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BrnType = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BrnRefId", out indice))
                oEnt.BrnRefId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnRefId = 0;
            if (sqlreader.TryGetOrdinal("BrnVen", out indice))
                oEnt.BrnVen = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnVen = 0;
            if (sqlreader.TryGetOrdinal("BrnCom", out indice))
                oEnt.BrnCom = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnCom = 0;
            if (sqlreader.TryGetOrdinal("BrnIng", out indice))
                oEnt.BrnIng = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnIng = 0;
            if (sqlreader.TryGetOrdinal("BrnSal", out indice))
                oEnt.BrnSal = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnSal = 0;
            if (sqlreader.TryGetOrdinal("BrnTrns", out indice))
                oEnt.BrnTrns = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnTrns = 0;
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
