﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datMovementDetail : datBase
    {
        public int add(MovementDetail p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSMovementDetail", p.MvnId, p.BrnId, p.PrdId, p.PrdDesc, p.MvnDtlStatus, p.DcmObjBaseId, p.MvnBaseId, p.MvnDtlCur, p.MvnDtlRate, p.MvnDtlAcctCode, p.MvnDtlTaxCode, p.MvnDtlCostingCode, p.MvnDtlQty, p.MvnDtlVnt, p.MvnDtlDsct, p.MvnDtlMnt, p.objType, p.MvnDtlId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(MovementDetail p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDMovementDetail", p.MvnDtlId, p.MvnId, p.BrnId, p.PrdId, p.PrdDesc, p.MvnDtlStatus, p.DcmObjBaseId, p.MvnBaseId, p.MvnDtlCur, p.MvnDtlRate, p.MvnDtlAcctCode, p.MvnDtlTaxCode, p.MvnDtlCostingCode, p.MvnDtlQty, p.MvnDtlVnt, p.MvnDtlDsct, p.MvnDtlMnt, p.objType) > 0 ? p.MvnDtlId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(MovementDetail p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELMovementDetail", p.MvnDtlId) > 0 ? p.MvnDtlId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<MovementDetail> lis(MovementDetail p)
        {
            IList<MovementDetail> oList = new List<MovementDetail>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISMovementDetail"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorMovementDetail(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<MovementDetail>();
                throw ex;
            }
            return oList;
        }
        public MovementDetail sel(MovementDetail p)
        {
            MovementDetail oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSMovementDetail", p.MvnDtlId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorMovementDetail(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new MovementDetail();
                throw ex;
            }
            return oEnt;
        }
        public MovementDetail ctorMovementDetail(SqlDataReader sqlreader)
        {
            MovementDetail oEnt = new MovementDetail();
            int indice;

            if (sqlreader.TryGetOrdinal("MvnDtlId", out indice))
                oEnt.MvnDtlId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.MvnDtlId = 0;
            if (sqlreader.TryGetOrdinal("MvnId", out indice))
                oEnt.MvnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.MvnId = 0;
            if (sqlreader.TryGetOrdinal("BrnId", out indice))
                oEnt.BrnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnId = 0;
            if (sqlreader.TryGetOrdinal("PrdId", out indice))
                oEnt.PrdId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdId = 0;
            if (sqlreader.TryGetOrdinal("PrdDesc", out indice))
                oEnt.PrdDesc = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.PrdDesc = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnDtlStatus", out indice))
                oEnt.MvnDtlStatus = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnDtlStatus = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("DcmObjBaseId", out indice))
                oEnt.DcmObjBaseId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.DcmObjBaseId = 0;
            if (sqlreader.TryGetOrdinal("MvnBaseId", out indice))
                oEnt.MvnBaseId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.MvnBaseId = 0;
            if (sqlreader.TryGetOrdinal("MvnDtlCur", out indice))
                oEnt.MvnDtlCur = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnDtlCur = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnDtlRate", out indice))
                oEnt.MvnDtlRate = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnDtlRate = 0;
            if (sqlreader.TryGetOrdinal("MvnDtlAcctCode", out indice))
                oEnt.MvnDtlAcctCode = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnDtlAcctCode = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnDtlTaxCode", out indice))
                oEnt.MvnDtlTaxCode = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnDtlTaxCode = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnDtlCostingCode", out indice))
                oEnt.MvnDtlCostingCode = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnDtlCostingCode = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnDtlQty", out indice))
                oEnt.MvnDtlQty = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnDtlQty = 0;
            if (sqlreader.TryGetOrdinal("MvnDtlVnt", out indice))
                oEnt.MvnDtlVnt = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnDtlVnt = 0;
            if (sqlreader.TryGetOrdinal("MvnDtlDsct", out indice))
                oEnt.MvnDtlDsct = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnDtlDsct = 0;
            if (sqlreader.TryGetOrdinal("MvnDtlMnt", out indice))
                oEnt.MvnDtlMnt = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MvnDtlMnt = 0;
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
