﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datPeriod : datBase
    {
        public int add(Period p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSPeriod", p.BsnId, p.ExrId, p.PerdFecIni, p.PerdFecFin, p.PerdDescription, p.PerdClosed, p.objType, p.PerdId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(Period p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDPeriod", p.PerdId, p.BsnId, p.ExrId, p.PerdFecIni, p.PerdFecFin, p.PerdDescription, p.PerdClosed, p.objType) > 0 ? p.PerdId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(Period p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELPeriod", p.PerdId) > 0 ? p.PerdId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<Period> lis(Period p)
        {
            IList<Period> oList = new List<Period>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISPeriod"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorPeriod(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<Period>();
                throw ex;
            }
            return oList;
        }
        public Period sel(Period p)
        {
            Period oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSPeriod", p.PerdId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorPeriod(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new Period();
                throw ex;
            }
            return oEnt;
        }
        public Period ctorPeriod(SqlDataReader sqlreader)
        {
            Period oEnt = new Period();
            int indice;

            if (sqlreader.TryGetOrdinal("PerdId", out indice))
                oEnt.PerdId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PerdId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("ExrId", out indice))
                oEnt.ExrId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.ExrId = 0;
            if (sqlreader.TryGetOrdinal("PerdFecIni", out indice))
                oEnt.PerdFecIni = sqlreader.IsDBNull(indice) ? Convert.ToDateTime("01/01/1900") : sqlreader.GetDateTime(indice);
            else
                oEnt.PerdFecIni = Convert.ToDateTime("01/01/1900");
            if (sqlreader.TryGetOrdinal("PerdFecFin", out indice))
                oEnt.PerdFecFin = sqlreader.IsDBNull(indice) ? Convert.ToDateTime("01/01/1900") : sqlreader.GetDateTime(indice);
            else
                oEnt.PerdFecFin = Convert.ToDateTime("01/01/1900");
            if (sqlreader.TryGetOrdinal("PerdDescription", out indice))
                oEnt.PerdDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.PerdDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("PerdClosed", out indice))
                oEnt.PerdClosed = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PerdClosed = 0;
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
