﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datBranchProduct : datBase
    {
        public int add(BranchProduct p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSBranchProduct", p.BsnId, p.PrdId, p.BrnId, p.BrnPrdQty, p.BrnPrdQtyMin, p.BrnPrdClosed, p.objType, p.BrnPrdId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int upd(BranchProduct p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDBranchProduct", p.BrnPrdId, p.BsnId, p.PrdId, p.BrnId, p.BrnPrdQty, p.BrnPrdQtyMin, p.BrnPrdClosed, p.objType) > 0 ? p.BrnPrdId : 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int del(BranchProduct p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELBranchProduct", p.BrnPrdId) > 0 ? p.BrnPrdId : 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public IList<BranchProduct> lis(BranchProduct p)
        {
            IList<BranchProduct> oList = new List<BranchProduct>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISBranchProduct"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorBranchProduct(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<BranchProduct>();
                throw ex;
            }
            return oList;
        }
        public BranchProduct sel(BranchProduct p)
        {
            BranchProduct oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSBranchProduct", p.BrnPrdId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorBranchProduct(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new BranchProduct();
                throw ex;
            }
            return oEnt;
        }
        public BranchProduct ctorBranchProduct(SqlDataReader sqlreader)
        {
            BranchProduct oEnt = new BranchProduct();
            int indice;

            if (sqlreader.TryGetOrdinal("BrnPrdId", out indice))
                oEnt.BrnPrdId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnPrdId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("PrdId", out indice))
                oEnt.PrdId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdId = 0;
            if (sqlreader.TryGetOrdinal("BrnId", out indice))
                oEnt.BrnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnId = 0;
            if (sqlreader.TryGetOrdinal("BrnPrdQty", out indice))
                oEnt.BrnPrdQty = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.BrnPrdQty = 0;
            if (sqlreader.TryGetOrdinal("BrnPrdQtyMin", out indice))
                oEnt.BrnPrdQtyMin = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.BrnPrdQtyMin = 0;
            if (sqlreader.TryGetOrdinal("BrnPrdClosed", out indice))
                oEnt.BrnPrdClosed = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BrnPrdClosed = 0;
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
