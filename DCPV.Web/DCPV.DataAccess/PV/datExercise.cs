﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datExercise : datBase
    {
        public int add(Exercise p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSExercise", p.BsnId, p.ExrDescription, p.ExrYear, p.ExrClosed, p.objType, p.ExrId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(Exercise p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDExercise", p.ExrId, p.BsnId, p.ExrDescription, p.ExrYear, p.ExrClosed, p.objType) > 0 ? p.ExrId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(Exercise p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELExercise", p.ExrId) > 0 ? p.ExrId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<Exercise> lis(Exercise p)
        {
            IList<Exercise> oList = new List<Exercise>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISExercise"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorExercise(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<Exercise>();
                throw ex;
            }
            return oList;
        }
        public Exercise sel(Exercise p)
        {
            Exercise oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSExercise", p.ExrId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorExercise(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new Exercise();
                throw ex;
            }
            return oEnt;
        }
        public Exercise ctorExercise(SqlDataReader sqlreader)
        {
            Exercise oEnt = new Exercise();
            int indice;

            if (sqlreader.TryGetOrdinal("ExrId", out indice))
                oEnt.ExrId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.ExrId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("ExrDescription", out indice))
                oEnt.ExrDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.ExrDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("ExrYear", out indice))
                oEnt.ExrYear = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.ExrYear = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("ExrClosed", out indice))
                oEnt.ExrClosed = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.ExrClosed = 0;
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
