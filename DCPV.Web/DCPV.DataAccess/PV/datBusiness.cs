﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datBusiness : datBase
    {
        public int add(Business p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSBusiness", p.BsnRUC, p.BsnReason, p.BsnAddress, p.BsnDescription, p.BsnLogo, p.BsnNameContact, p.BsnEmail, p.BsnTel, p.BsnInfAdd, p.objType, p.BsnId);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int upd(Business p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDBusiness", p.BsnId, p.BsnRUC, p.BsnReason, p.BsnAddress, p.BsnDescription, p.BsnLogo, p.BsnNameContact, p.BsnEmail, p.BsnTel, p.BsnInfAdd, p.objType) > 0 ? p.BsnId : 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        public int del(Business p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELBusiness", p.BsnId) > 0 ? p.BsnId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<Business> lis(Business p)
        {
            IList<Business> oList = new List<Business>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISBusiness"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorBusiness(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<Business>();
                throw ex;
            }
            return oList;
        }
        public Business sel(Business p)
        {
            Business oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSBusiness", p.BsnId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorBusiness(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new Business();
                throw ex;
            }
            return oEnt;
        }
        public Business ctorBusiness(SqlDataReader sqlreader)
        {
            Business oEnt = new Business();
            int indice;

            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("BsnRUC", out indice))
                oEnt.BsnRUC = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnRUC = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnReason", out indice))
                oEnt.BsnReason = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnReason = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnAddress", out indice))
                oEnt.BsnAddress = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnAddress = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnDescription", out indice))
                oEnt.BsnDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnLogo", out indice))
                oEnt.BsnLogo = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnLogo = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnNameContact", out indice))
                oEnt.BsnNameContact = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnNameContact = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnEmail", out indice))
                oEnt.BsnEmail = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnEmail = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnTel", out indice))
                oEnt.BsnTel = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnTel = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnInfAdd", out indice))
                oEnt.BsnInfAdd = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnInfAdd = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
