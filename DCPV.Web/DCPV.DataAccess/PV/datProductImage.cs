﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datProductImage : datBase
    {
        public int add(ProductImage p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSProductImage", p.PrdId, p.BsnId, p.PrdImgRuta, p.objType, p.PrdImgId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(ProductImage p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDProductImage", p.PrdImgId, p.PrdId, p.BsnId, p.PrdImgRuta, p.objType) > 0 ? p.PrdImgId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(ProductImage p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELProductImage", p.PrdImgId) > 0 ? p.PrdImgId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<ProductImage> lis(ProductImage p)
        {
            IList<ProductImage> oList = new List<ProductImage>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISProductImage"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorProductImage(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<ProductImage>();
                throw ex;
            }
            return oList;
        }
        public ProductImage sel(ProductImage p)
        {
            ProductImage oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSProductImage", p.PrdImgId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorProductImage(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new ProductImage();
                throw ex;
            }
            return oEnt;
        }
        public ProductImage ctorProductImage(SqlDataReader sqlreader)
        {
            ProductImage oEnt = new ProductImage();
            int indice;

            if (sqlreader.TryGetOrdinal("PrdImgId", out indice))
                oEnt.PrdImgId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdImgId = 0;
            if (sqlreader.TryGetOrdinal("PrdId", out indice))
                oEnt.PrdId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("PrdImgRuta", out indice))
                oEnt.PrdImgRuta = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.PrdImgRuta = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
