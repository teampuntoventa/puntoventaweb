﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datBusinessPartner : datBase
    {
        public int add(BusinessPartner p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSBusinessPartner", p.BsnId, p.DcmTpId, p.BsnPrtType, p.BsnPrtNumDoc, p.BsnPrtReason, p.BsnPrtContact, p.BsnPrtAlias, p.BsnPrtTlf1, p.BsnPrtTlf2, p.BsnPrtEmail, p.BsnPrtDirección, p.BsnPrtMapLat, p.BsnPrtMapLng, p.BsnPrtObs, p.objType, p.BsnPrtId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(BusinessPartner p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDBusinessPartner", p.BsnPrtId, p.BsnId, p.DcmTpId, p.BsnPrtType, p.BsnPrtNumDoc, p.BsnPrtReason, p.BsnPrtContact, p.BsnPrtAlias, p.BsnPrtTlf1, p.BsnPrtTlf2, p.BsnPrtEmail, p.BsnPrtDirección, p.BsnPrtMapLat, p.BsnPrtMapLng, p.BsnPrtObs, p.objType) > 0 ? p.BsnPrtId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(BusinessPartner p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELBusinessPartner", p.BsnPrtId) > 0 ? p.BsnPrtId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<BusinessPartner> lis(BusinessPartner p)
        {
            IList<BusinessPartner> oList = new List<BusinessPartner>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISBusinessPartner"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorBusinessPartner(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<BusinessPartner>();
                throw ex;
            }
            return oList;
        }
        public BusinessPartner sel(BusinessPartner p)
        {
            BusinessPartner oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSBusinessPartner", p.BsnPrtId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorBusinessPartner(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new BusinessPartner();
                throw ex;
            }
            return oEnt;
        }
        public BusinessPartner ctorBusinessPartner(SqlDataReader sqlreader)
        {
            BusinessPartner oEnt = new BusinessPartner();
            int indice;

            if (sqlreader.TryGetOrdinal("BsnPrtId", out indice))
                oEnt.BsnPrtId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnPrtId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("DcmTpId", out indice))
                oEnt.DcmTpId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.DcmTpId = 0;
            if (sqlreader.TryGetOrdinal("BsnPrtType", out indice))
                oEnt.BsnPrtType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnPrtType = 0;
            if (sqlreader.TryGetOrdinal("BsnPrtNumDoc", out indice))
                oEnt.BsnPrtNumDoc = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtNumDoc = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtReason", out indice))
                oEnt.BsnPrtReason = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtReason = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtContact", out indice))
                oEnt.BsnPrtContact = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtContact = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtAlias", out indice))
                oEnt.BsnPrtAlias = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtAlias = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtTlf1", out indice))
                oEnt.BsnPrtTlf1 = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtTlf1 = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtTlf2", out indice))
                oEnt.BsnPrtTlf2 = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtTlf2 = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtEmail", out indice))
                oEnt.BsnPrtEmail = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtEmail = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtDirección", out indice))
                oEnt.BsnPrtDirección = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtDirección = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtMapLat", out indice))
                oEnt.BsnPrtMapLat = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtMapLat = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtMapLng", out indice))
                oEnt.BsnPrtMapLng = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtMapLng = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnPrtObs", out indice))
                oEnt.BsnPrtObs = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.BsnPrtObs = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
