﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datDocumentType : datBase
    {
        public int add(DocumentType p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSDocumentType", p.BsnId, p.DcmTpDescription, p.objType, p.DcmTpId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(DocumentType p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDDocumentType", p.DcmTpId, p.BsnId, p.DcmTpDescription, p.objType) > 0 ? p.DcmTpId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(DocumentType p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELDocumentType", p.DcmTpId) > 0 ? p.DcmTpId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<DocumentType> lis(DocumentType p)
        {
            IList<DocumentType> oList = new List<DocumentType>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISDocumentType"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorDocumentType(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<DocumentType>();
                throw ex;
            }
            return oList;
        }
        public DocumentType sel(DocumentType p)
        {
            DocumentType oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSDocumentType", p.DcmTpId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorDocumentType(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new DocumentType();
                throw ex;
            }
            return oEnt;
        }
        public DocumentType ctorDocumentType(SqlDataReader sqlreader)
        {
            DocumentType oEnt = new DocumentType();
            int indice;

            if (sqlreader.TryGetOrdinal("DcmTpId", out indice))
                oEnt.DcmTpId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.DcmTpId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("DcmTpDescription", out indice))
                oEnt.DcmTpDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.DcmTpDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
