﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datProductType : datBase
    {
        public int add(ProductType p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSProductType", p.BsnId, p.PrdTpDescription, p.objType, p.PrdTpId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(ProductType p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDProductType", p.PrdTpId, p.BsnId, p.PrdTpDescription, p.objType) > 0 ? p.PrdTpId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(ProductType p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELProductType", p.PrdTpId) > 0 ? p.PrdTpId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<ProductType> lis(ProductType p)
        {
            IList<ProductType> oList = new List<ProductType>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISProductType"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorProductType(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<ProductType>();
                throw ex;
            }
            return oList;
        }
        public ProductType sel(ProductType p)
        {
            ProductType oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSProductType", p.PrdTpId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorProductType(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new ProductType();
                throw ex;
            }
            return oEnt;
        }
        public ProductType ctorProductType(SqlDataReader sqlreader)
        {
            ProductType oEnt = new ProductType();
            int indice;

            if (sqlreader.TryGetOrdinal("PrdTpId", out indice))
                oEnt.PrdTpId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdTpId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("PrdTpDescription", out indice))
                oEnt.PrdTpDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.PrdTpDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
