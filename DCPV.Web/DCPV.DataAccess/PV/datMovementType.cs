﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datMovementType : datBase
    {
        public int add(MovementType p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSMovementType", p.BsnId, p.ExrId, p.MvnTpDesc, p.MvnTpType, p.objType, p.MvnTpId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(MovementType p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDMovementType", p.MvnTpId, p.BsnId, p.ExrId, p.MvnTpDesc, p.MvnTpType, p.objType) > 0 ? p.MvnTpId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(MovementType p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELMovementType", p.MvnTpId) > 0 ? p.MvnTpId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<MovementType> lis(MovementType p)
        {
            IList<MovementType> oList = new List<MovementType>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISMovementType"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorMovementType(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<MovementType>();
                throw ex;
            }
            return oList;
        }
        public MovementType sel(MovementType p)
        {
            MovementType oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSMovementType", p.MvnTpId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorMovementType(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new MovementType();
                throw ex;
            }
            return oEnt;
        }
        public MovementType ctorMovementType(SqlDataReader sqlreader)
        {
            MovementType oEnt = new MovementType();
            int indice;

            if (sqlreader.TryGetOrdinal("MvnTpId", out indice))
                oEnt.MvnTpId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.MvnTpId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("ExrId", out indice))
                oEnt.ExrId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.ExrId = 0;
            if (sqlreader.TryGetOrdinal("MvnTpDesc", out indice))
                oEnt.MvnTpDesc = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnTpDesc = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MvnTpType", out indice))
                oEnt.MvnTpType = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.MvnTpType = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
