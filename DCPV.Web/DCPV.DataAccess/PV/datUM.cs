﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datUM : datBase
    {
        public int add(UM p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSUM", p.BsnId, p.UmDescription, p.objType, p.UmId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(UM p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDUM", p.UmId, p.BsnId, p.UmDescription, p.objType) > 0 ? p.UmId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(UM p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELUM", p.UmId) > 0 ? p.UmId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<UM> lis(UM p)
        {
            IList<UM> oList = new List<UM>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISUM"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorUM(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<UM>();
                throw ex;
            }
            return oList;
        }
        public UM sel(UM p)
        {
            UM oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSUM", p.UmId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorUM(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new UM();
                throw ex;
            }
            return oEnt;
        }
        public UM ctorUM(SqlDataReader sqlreader)
        {
            UM oEnt = new UM();
            int indice;

            if (sqlreader.TryGetOrdinal("UmId", out indice))
                oEnt.UmId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.UmId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("UmDescription", out indice))
                oEnt.UmDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.UmDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
