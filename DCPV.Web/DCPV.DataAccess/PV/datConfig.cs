﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datConfig : datBase
    {
        public int add(Config p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSConfig", p.CnfIdApp, p.CnfKey, p.BsnId, p.FrmImpId, p.CnfLauncherCam, p.CnfInfCustomer, p.CnfMon, p.objType, p.CnfId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(Config p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDConfig", p.CnfId, p.CnfIdApp, p.CnfKey, p.BsnId, p.FrmImpId, p.CnfLauncherCam, p.CnfInfCustomer, p.CnfMon, p.objType) > 0 ? p.CnfId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(Config p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELConfig", p.CnfId) > 0 ? p.CnfId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<Config> lis(Config p)
        {
            IList<Config> oList = new List<Config>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISConfig"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorConfig(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<Config>();
                throw ex;
            }
            return oList;
        }
        public Config sel(Config p)
        {
            Config oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSConfig", p.CnfId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorConfig(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new Config();
                throw ex;
            }
            return oEnt;
        }
        public Config ctorConfig(SqlDataReader sqlreader)
        {
            Config oEnt = new Config();
            int indice;

            if (sqlreader.TryGetOrdinal("CnfId", out indice))
                oEnt.CnfId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.CnfId = 0;
            if (sqlreader.TryGetOrdinal("CnfIdApp", out indice))
                oEnt.CnfIdApp = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.CnfIdApp = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("CnfKey", out indice))
                oEnt.CnfKey = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.CnfKey = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("FrmImpId", out indice))
                oEnt.FrmImpId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.FrmImpId = 0;
            if (sqlreader.TryGetOrdinal("CnfLauncherCam", out indice))
                oEnt.CnfLauncherCam = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.CnfLauncherCam = 0;
            if (sqlreader.TryGetOrdinal("CnfInfCustomer", out indice))
                oEnt.CnfInfCustomer = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.CnfInfCustomer = 0;
            if (sqlreader.TryGetOrdinal("CnfMon", out indice))
                oEnt.CnfMon = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.CnfMon = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
