﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datTableIGV : datBase
    {
        public int add(TableIGV p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSTableIGV", p.Name, p.Description, p.Rate, p.ValidForAR, p.ValidForAP, p.SalesTax, p.PurchTax, p.MinAmount, p.MaxAmount, p.objType, p.Code);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(TableIGV p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDTableIGV", p.Code, p.Name, p.Description, p.Rate, p.ValidForAR, p.ValidForAP, p.SalesTax, p.PurchTax, p.MinAmount, p.MaxAmount, p.objType) > 0 ? 1 : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(TableIGV p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELTableIGV", p.Code) > 0 ? 1 : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<TableIGV> lis(TableIGV p)
        {
            IList<TableIGV> oList = new List<TableIGV>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISTableIGV"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorTableIGV(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<TableIGV>();
                throw ex;
            }
            return oList;
        }
        public TableIGV sel(TableIGV p)
        {
            TableIGV oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSTableIGV", p.Code))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorTableIGV(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new TableIGV();
                throw ex;
            }
            return oEnt;
        }
        public TableIGV ctorTableIGV(SqlDataReader sqlreader)
        {
            TableIGV oEnt = new TableIGV();
            int indice;

            if (sqlreader.TryGetOrdinal("Code", out indice))
                oEnt.Code = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.Code = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("Name", out indice))
                oEnt.Name = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.Name = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("Description", out indice))
                oEnt.Description = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.Description = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("Rate", out indice))
                oEnt.Rate = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.Rate = 0;
            if (sqlreader.TryGetOrdinal("ValidForAR", out indice))
                oEnt.ValidForAR = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.ValidForAR = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("ValidForAP", out indice))
                oEnt.ValidForAP = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.ValidForAP = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("SalesTax", out indice))
                oEnt.SalesTax = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.SalesTax = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("PurchTax", out indice))
                oEnt.PurchTax = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.PurchTax = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("MinAmount", out indice))
                oEnt.MinAmount = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MinAmount = 0;
            if (sqlreader.TryGetOrdinal("MaxAmount", out indice))
                oEnt.MaxAmount = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetDecimal(indice);
            else
                oEnt.MaxAmount = 0;
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
