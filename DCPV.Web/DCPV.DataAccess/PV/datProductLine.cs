﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datProductLine : datBase
    {
        public int add(ProductLine p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSProductLine", p.BsnId, p.PrdTpid, p.PrdLnDescription, p.objType, p.PrdLnId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(ProductLine p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDProductLine", p.PrdLnId, p.BsnId, p.PrdTpid, p.PrdLnDescription, p.objType) > 0 ? p.PrdLnId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(ProductLine p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELProductLine", p.PrdLnId) > 0 ? p.PrdLnId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<ProductLine> lis(ProductLine p)
        {
            IList<ProductLine> oList = new List<ProductLine>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISProductLine"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorProductLine(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<ProductLine>();
                throw ex;
            }
            return oList;
        }
        public ProductLine sel(ProductLine p)
        {
            ProductLine oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSProductLine", p.PrdLnId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorProductLine(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new ProductLine();
                throw ex;
            }
            return oEnt;
        }
        public ProductLine ctorProductLine(SqlDataReader sqlreader)
        {
            ProductLine oEnt = new ProductLine();
            int indice;

            if (sqlreader.TryGetOrdinal("PrdLnId", out indice))
                oEnt.PrdLnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdLnId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("PrdTpid", out indice))
                oEnt.PrdTpid = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.PrdTpid = 0;
            if (sqlreader.TryGetOrdinal("PrdLnDescription", out indice))
                oEnt.PrdLnDescription = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.PrdLnDescription = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
