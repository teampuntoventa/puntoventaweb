﻿using DCPV.Entity;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace DCPV.DataAccess.PV
{
    public class datDocumentObject : datBase
    {
        public int add(DocumentObject p)
        {
            try
            {
                return (int)datCnx._GDatos.TraerValorOutput("USP_INSDocumentObject", p.BsnId, p.MvnTpId, p.DcmObjDesc, p.objType, p.DcmObjId);
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int upd(DocumentObject p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_UPDDocumentObject", p.DcmObjId, p.BsnId, p.MvnTpId, p.DcmObjDesc, p.objType) > 0 ? p.DcmObjId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public int del(DocumentObject p)
        {
            try
            {
                return (int)datCnx._GDatos.Ejecutar("USP_DELDocumentObject", p.DcmObjId) > 0 ? p.DcmObjId : 0;
            }
            catch (Exception ex)
            {
                
                throw ex;
            }
        }
        public IList<DocumentObject> lis(DocumentObject p)
        {
            IList<DocumentObject> oList = new List<DocumentObject>();
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_LISDocumentObject"))
                {
                    while (dr.Read())
                    {
                        oList.Add(ctorDocumentObject(dr));
                    }
                }
            }
            catch (Exception ex)
            {
                oList = new List<DocumentObject>();
                throw ex;
            }
            return oList;
        }
        public DocumentObject sel(DocumentObject p)
        {
            DocumentObject oEnt = null;
            try
            {
                using (SqlDataReader dr = (SqlDataReader)datCnx._GDatos.TraerDataReader("USP_BUSDocumentObject", p.DcmObjId))
                {
                    while (dr.Read())
                    {
                        oEnt = ctorDocumentObject(dr);
                    }
                }
            }
            catch (Exception ex)
            {
                oEnt = new DocumentObject();
                throw ex;
            }
            return oEnt;
        }
        public DocumentObject ctorDocumentObject(SqlDataReader sqlreader)
        {
            DocumentObject oEnt = new DocumentObject();
            int indice;

            if (sqlreader.TryGetOrdinal("DcmObjId", out indice))
                oEnt.DcmObjId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.DcmObjId = 0;
            if (sqlreader.TryGetOrdinal("BsnId", out indice))
                oEnt.BsnId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.BsnId = 0;
            if (sqlreader.TryGetOrdinal("MvnTpId", out indice))
                oEnt.MvnTpId = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.MvnTpId = 0;
            if (sqlreader.TryGetOrdinal("DcmObjDesc", out indice))
                oEnt.DcmObjDesc = sqlreader.IsDBNull(indice) ? Convert.ToString("") : Convert.ToString(sqlreader.GetString(indice));
            else
                oEnt.DcmObjDesc = Convert.ToString("");
            if (sqlreader.TryGetOrdinal("objType", out indice))
                oEnt.objType = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.objType = 0;
            if (sqlreader.TryGetOrdinal("Active", out indice))
                oEnt.Active = sqlreader.IsDBNull(indice) ? 0 : sqlreader.GetInt32(indice);
            else
                oEnt.Active = 0;

            return oEnt;
        }
    }

}
