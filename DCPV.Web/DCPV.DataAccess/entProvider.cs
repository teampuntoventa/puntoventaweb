﻿
using System.Collections.Generic;
using System.Runtime.Serialization;

namespace DCPV.DataAccess
{
    [DataContract]
    public class entProvider
    {
        #region Variables
        private int proId;
        private string proName;
        #endregion

        #region Propiedades
        [DataMember(Name = "proId", Order = 1)]
        public int ProId
        {
            get { return this.proId; }
            set { this.proId = value; }
        }
        [DataMember(Name = "proName", Order = 2)]
        public string ProName
        {
            get { return this.proName; }
            set { this.proName = value; }
        }
        #endregion

        #region Constructores
        public entProvider()
        {
            this.proId = 0;
            this.proName = string.Empty;
        }
        public entProvider(int pId, string pName)
        {
            this.proId = pId;
            this.proName = pName;
        }
        #endregion
    }

    public class ListEntProvider
    {
        public static IList<entProvider> toList()
        {
            IList<entProvider> oList = new List<entProvider>();
            oList.Add(new entProvider((int)DataProviders.MsSQL, DataProviders.MsSQL.ToString()));
            //oList.Add(new entProvider((int)Utils.DataProviders.MySQL, Utils.DataProviders.MySQL.ToString()));
            //oList.Add(new entProvider((int)Utils.DataProviders.Access, Utils.DataProviders.Access.ToString()));
            //oList.Add(new entProvider((int)Utils.DataProviders.Excel, Utils.DataProviders.Excel.ToString()));
            //oList.Add(new entProvider((int)Utils.DataProviders.TextFile, Utils.DataProviders.TextFile.ToString()));

            return oList;
        }
    }

    public enum DataProviders
    {
        MsSQL = 1,
        MySQL = 2,
        Access = 3,
        Excel = 4,
        TextFile = 5
    }
}
