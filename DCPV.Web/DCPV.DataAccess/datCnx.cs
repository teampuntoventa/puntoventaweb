﻿using System;
using libCon;

namespace DCPV.DataAccess
{
    public static class datCnx
    {
        #region Variables de la Conexión
        public static GDatos _GDatos;
        // Identificador de la Conexión
        private static string dbId = "";
        // HostName
        private static string dbH = string.Empty;
        // Database Name
        private static string dbD = string.Empty;
        // Database User
        private static string dbU = string.Empty;
        // Database Password
        private static string dbP = string.Empty;
        // Database Port
        private static string dbO = string.Empty;
        // Flag que indica si se cargó los parámetros
        private static bool setValues = false;
        #endregion

        #region Lectura y Escritura de Parámetros de Conexión
        /// <summary>
        /// Crear o modificar archivo y parámetros de conexión
        /// </summary>
        public static bool setValuesPrmsToCfg(string pH, string pO, string pD, string pU, string pP)
        {
            try
            {
                return libUtlsRM.utlConfig.setCfgPrms(dbId,
                                                        ListEntProvider.toList()[0].ProName + pD,
                                                        pH, pO, pD, pU, pP);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }
        /// <summary>
        /// Obtiene el valor de los parámetros de configuración a partir del archivo de configuración
        /// </summary>
        /// <returns></returns>
        public static bool getValuesPrmsFromCfg()
        {
            try
            {
                // si no estan cargados
                if (!setValues)
                {
                    //traermos desde el cfgprm.txt
                    var lista = libUtlsRM.utlConfig.getCfgToList(dbId);
                    dbH = lista[libUtlsRM.utlConfig.prmH];
                    dbO = lista[libUtlsRM.utlConfig.prmO];
                    dbD = lista[libUtlsRM.utlConfig.prmD];
                    dbU = lista[libUtlsRM.utlConfig.prmU];
                    dbP = lista[libUtlsRM.utlConfig.prmP];
                    setValues = true;
                }
            }
            catch (Exception)
            {
                setValues = false;
                throw;
            }
            return setValues;
        }
        #endregion

        #region Manejo de la Conexión
        /// <summary>
        /// Iniciar Sesion SQL
        /// </summary>
        /// <returns></returns>
        public static bool Iniciar()
        {
            bool booRslt = true;
            try
            {
                dbD = "DCPuntoVenta"; // dbManagementPleWrk"; "dbManagementPleWrk_II";
                dbH = "SICAMP12-DES004";
                dbU = "sa";
                dbP = "abcDEF123";

                /* ASIGNAR VALORES */
                setValuesPrmsToCfg(dbH, string.Empty, dbD, dbU, dbP);

                // Cargamos los parámetros de cnx
                if (!getValuesPrmsFromCfg())
                {
                    booRslt = false;
                    throw new Exception("Error al obtener parámetros de conexión al servidor de datos." +
                                        Environment.NewLine + "cnx.Iniciar()");
                }
                // Si se cargaron
                if (setValues)
                {
                    _GDatos = new msSqlSvr(dbH, dbD, dbU, dbP);
                    booRslt = _GDatos.Autenticar();
                    if (!booRslt)
                    {
                        throw new Exception("Error al iniciar Sesión de Conexión con la Base de Datos." +
                                            Environment.NewLine + " Verificar parámetros de conexión." +
                                            Environment.NewLine + " Verificar conexión 1con el servidor." +
                                            Environment.NewLine + " Verificar conexión de red." +
                                            Environment.NewLine + " cnx.Iniciar()");
                    }
                }
            }
            catch (Exception ex)
            {
                booRslt = false;
                throw ex;
            }
            return booRslt;
        }


        public static bool Iniciar(int pProv = 0, string pH = "", string pD = "", string pU = "", string pP = "", string pO = "", string pF = "")
        {
            bool booRslt = true;
            try
            {
                // Cargamos los parámetros de cnx
                if (pProv == ListEntProvider.toList()[0].ProId) // MsSQL
                {
                    _GDatos = new msSqlSvr(pH, pD, pU, pP);
                    booRslt = _GDatos.Autenticar();
                }
                else if (pProv == ListEntProvider.toList()[1].ProId) // MySQL
                {
                    _GDatos = new mySqlSvr(pH, pO, pD, pU, pP);
                    booRslt = _GDatos.Autenticar();
                }
                else if (pProv == ListEntProvider.toList()[2].ProId) // Access
                {
                    // crear la coneccion con access
                    _GDatos = new mdSqlSvr(pF, pD, pU, pP);
                    booRslt = _GDatos.Autenticar();
                }

                if (!booRslt)
                {
                    throw new Exception("Error al iniciar Sesión de Conexión con la Base de Datos." +
                                        Environment.NewLine + " Verificar parámetros de conexión." +
                                        Environment.NewLine + " Verificar conexión 1con el servidor." +
                                        Environment.NewLine + " Verificar conexión de red." +
                                        Environment.NewLine + " cnx.Iniciar()");
                }
            }
            catch (Exception ex)
            {
                booRslt = false;
                throw ex;
            }
            return booRslt;
        }


        /// <summary>
        /// Finaliza Sesion SQL
        /// </summary>
        public static void Finalizar()
        {
            try
            {
                if (_GDatos != null) _GDatos.CerrarConexion();
            }
            catch (Exception)
            {
                throw;
            }
        }
        /// <summary>
        /// Begin Trans
        /// </summary>
        public static void IniciarTransaccion()
        {
            _GDatos.IniciarTransaccion();
        }
        /// <summary>
        /// Commit Trans
        /// </summary>
        public static void TerminarTransaccion()
        {
            _GDatos.TerminarTransaccion();
        }
        /// <summary>
        /// RollBack Trans
        /// </summary>
        public static void AbortarTransaccion()
        {
            _GDatos.AbortarTransaccion();
        }
        #endregion
    }
}
