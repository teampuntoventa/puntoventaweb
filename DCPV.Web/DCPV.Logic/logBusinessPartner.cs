﻿using DCPV.DataAccess.PV;
using DCPV.Entity;
using DCPV.ILogic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DCPV.Logic
{
    public class logBusinessPartner : iloBusinessPartner
    {
        CResult iloBusinessPartner.add(BusinessPartner p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datBusinessPartner ida = new datBusinessPartner())
                {
                    objResult.DocEntry = ida.add(p).ToString();
                }
                objResult.Result = int.Parse(objResult.DocEntry) > 0 ? 0 : -1;
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloBusinessPartner.upd(BusinessPartner p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datBusinessPartner ida = new datBusinessPartner())
                {
                    objResult.Result = ida.upd(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloBusinessPartner.del(BusinessPartner p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datBusinessPartner ida = new datBusinessPartner())
                {
                    objResult.Result = ida.del(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        IList<BusinessPartner> iloBusinessPartner.lis(BusinessPartner p)
        {
            try
            {
                using (datBusinessPartner ida = new datBusinessPartner())
                {
                    return ida.lis(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new List<BusinessPartner>();
            }
        }

        BusinessPartner iloBusinessPartner.sel(BusinessPartner p)
        {
            try
            {
                using (datBusinessPartner ida = new datBusinessPartner())
                {
                    return ida.sel(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new BusinessPartner();
            }
        }

    }


}
