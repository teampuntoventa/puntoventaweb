﻿using DCPV.DataAccess.PV;
using DCPV.Entity;
using DCPV.ILogic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DCPV.Logic
{
    public class logPeriod : iloPeriod
    {
        CResult iloPeriod.add(Period p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datPeriod ida = new datPeriod())
                {
                    objResult.DocEntry = ida.add(p).ToString();
                }
                objResult.Result = int.Parse(objResult.DocEntry) > 0 ? 0 : -1;
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloPeriod.upd(Period p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datPeriod ida = new datPeriod())
                {
                    objResult.Result = ida.upd(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloPeriod.del(Period p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datPeriod ida = new datPeriod())
                {
                    objResult.Result = ida.del(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        IList<Period> iloPeriod.lis(Period p)
        {
            try
            {
                using (datPeriod ida = new datPeriod())
                {
                    return ida.lis(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new List<Period>();
            }
        }

        Period iloPeriod.sel(Period p)
        {
            try
            {
                using (datPeriod ida = new datPeriod())
                {
                    return ida.sel(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new Period();
            }
        }

    }


}
