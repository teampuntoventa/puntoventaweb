﻿using DCPV.DataAccess.PV;
using DCPV.Entity;
using DCPV.ILogic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DCPV.Logic
{
    public class logProductType : iloProductType
    {
        CResult iloProductType.add(ProductType p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datProductType ida = new datProductType())
                {
                    objResult.DocEntry = ida.add(p).ToString();
                }
                objResult.Result = int.Parse(objResult.DocEntry) > 0 ? 0 : -1;
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloProductType.upd(ProductType p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datProductType ida = new datProductType())
                {
                    objResult.Result = ida.upd(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloProductType.del(ProductType p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datProductType ida = new datProductType())
                {
                    objResult.Result = ida.del(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        IList<ProductType> iloProductType.lis(ProductType p)
        {
            try
            {
                using (datProductType ida = new datProductType())
                {
                    return ida.lis(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new List<ProductType>();
            }
        }

        ProductType iloProductType.sel(ProductType p)
        {
            try
            {
                using (datProductType ida = new datProductType())
                {
                    return ida.sel(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new ProductType();
            }
        }

    }


}
