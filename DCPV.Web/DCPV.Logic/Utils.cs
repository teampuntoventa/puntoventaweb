﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DCPV.Logic
{
    public class Utils
    {
        public static void RegistraLogErrors(string Method, string Error, string Obj)
        {
        subir:
            string xRuta = "";
            string xArchivo = @"notepad.log";
            string Fecha = DateTime.Now.ToShortDateString();
            string Path = string.Concat(xRuta.ToString(), xArchivo.ToString());

            if (!System.IO.File.Exists(Path))
            {
                string pathString = xRuta.ToString();
                System.IO.Directory.CreateDirectory(pathString);
                pathString = pathString + xArchivo;
                System.IO.StreamWriter archivo = System.IO.File.AppendText(pathString);
                archivo.Close();
                goto subir;
            }

            System.IO.File.SetAttributes(Path, System.IO.FileAttributes.Normal);
            System.IO.StreamWriter error = System.IO.File.AppendText(Path);
            error.WriteLine("---------------------------------------------------------------------------------");
            error.WriteLine("Fecha : " + Fecha);
            error.WriteLine("Metodo : " + Method);
            error.WriteLine("Error: " + Error);
            error.WriteLine("Object : " + Obj);
            error.WriteLine("---------------------------------------------------------------------------------");
            error.Close();
            error.Dispose();
        }
    }
}
