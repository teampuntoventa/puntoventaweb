﻿using DCPV.DataAccess.PV;
using DCPV.Entity;
using DCPV.ILogic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DCPV.Logic
{
    public class logMovementDetail : iloMovementDetail
    {
        CResult iloMovementDetail.add(MovementDetail p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datMovementDetail ida = new datMovementDetail())
                {
                    objResult.DocEntry = ida.add(p).ToString();
                }
                objResult.Result = int.Parse(objResult.DocEntry) > 0 ? 0 : -1;
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloMovementDetail.upd(MovementDetail p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datMovementDetail ida = new datMovementDetail())
                {
                    objResult.Result = ida.upd(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloMovementDetail.del(MovementDetail p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datMovementDetail ida = new datMovementDetail())
                {
                    objResult.Result = ida.del(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        IList<MovementDetail> iloMovementDetail.lis(MovementDetail p)
        {
            try
            {
                using (datMovementDetail ida = new datMovementDetail())
                {
                    return ida.lis(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new List<MovementDetail>();
            }
        }

        MovementDetail iloMovementDetail.sel(MovementDetail p)
        {
            try
            {
                using (datMovementDetail ida = new datMovementDetail())
                {
                    return ida.sel(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new MovementDetail();
            }
        }

    }


}
