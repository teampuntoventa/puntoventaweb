﻿using DCPV.DataAccess.PV;
using DCPV.Entity;
using DCPV.ILogic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DCPV.Logic
{
    public class logExercise : iloExercise
    {
        CResult iloExercise.add(Exercise p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datExercise ida = new datExercise())
                {
                    objResult.DocEntry = ida.add(p).ToString();
                }
                objResult.Result = int.Parse(objResult.DocEntry) > 0 ? 0 : -1;
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloExercise.upd(Exercise p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datExercise ida = new datExercise())
                {
                    objResult.Result = ida.upd(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloExercise.del(Exercise p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datExercise ida = new datExercise())
                {
                    objResult.Result = ida.del(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        IList<Exercise> iloExercise.lis(Exercise p)
        {
            try
            {
                using (datExercise ida = new datExercise())
                {
                    return ida.lis(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new List<Exercise>();
            }
        }

        Exercise iloExercise.sel(Exercise p)
        {
            try
            {
                using (datExercise ida = new datExercise())
                {
                    return ida.sel(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new Exercise();
            }
        }

    }


}
