﻿using DCPV.DataAccess.PV;
using DCPV.Entity;
using DCPV.ILogic;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;

namespace DCPV.Logic
{
    public class logUM : iloUM
    {
        CResult iloUM.add(UM p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datUM ida = new datUM())
                {
                    objResult.DocEntry = ida.add(p).ToString();
                }
                objResult.Result = int.Parse(objResult.DocEntry) > 0 ? 0 : -1;
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloUM.upd(UM p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datUM ida = new datUM())
                {
                    objResult.Result = ida.upd(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        CResult iloUM.del(UM p)
        {
            CResult objResult = new CResult();
            try
            {
                using (datUM ida = new datUM())
                {
                    objResult.Result = ida.del(p) > 0 ? 0 : -1;
                }
            }
            catch (Exception ex)
            {
                objResult.Result = -1;
                objResult.DocEntry = "";
                objResult.Error = ex.Message.ToString();
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
            }
            return objResult;
        }

        IList<UM> iloUM.lis(UM p)
        {
            try
            {
                using (datUM ida = new datUM())
                {
                    return ida.lis(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new List<UM>();
            }
        }

        UM iloUM.sel(UM p)
        {
            try
            {
                using (datUM ida = new datUM())
                {
                    return ida.sel(p);
                }
            }
            catch (Exception ex)
            {
                Utils.RegistraLogErrors(System.Reflection.MethodBase.GetCurrentMethod().Name, ex.Message.ToString(), JsonConvert.SerializeObject(p));
                return new UM();
            }
        }

    }


}
